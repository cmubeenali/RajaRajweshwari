<?php $this->load->view('Header/header.php'); ?>
    <div class="container-fluid col-md-8">
		<div class="card ">
			<div class="card-body">
				<h4 class="card-title">Users</h4>

				<div class="tab-container">
					<ul class="nav nav-tabs" role="tablist" id="myTab">
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#addnew" role="tab" id="navNew">Add New</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#list" role="tab" id="navList">List</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active fade show" id="list" name="list" role="tabpanel">
                            <br>
							<div class="table-responsive table-hover" id="tblData" name="tblData">
								<table id="data-table" class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Username</th>
											<th>Email</th>
											<th>User Type</th>
											<th>Action</th>
									</tr>
									</thead>
									<tbody>
									<?php
										$i = 1;
										foreach ($userData as $key) {?>
									<tr>
										<th scope="row"><?=$i;?></th>
										<td><?=$key->userName;?></td>
										<td><?=$key->email;?></td>
										<td><?=$key->userType;?></td>
										<td><i class="mdi mdi-pencil" style="cursor:pointer;" onclick="editUser(<?=$key->userId;?>)"> </i>/<i class="mdi mdi-delete" style="cursor:pointer;" onclick="deleteUser(<?=$key->userId;?>)">  </i></td>
									</tr>
									<?php $i++;}?>

								</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="addnew" role="tabpanel">
							<div class="card-body">
								<input type="hidden" id="hidUserId" value="" name="hidUserId">
								<div class="row">
									<div class="col-md-6">
										<label class="control-label">Username</label>
										<div class="form-group">
											<input type="text" id="username" name="username" class="form-control" value="<?php echo $userInfo['username'] ?>" >
											<i class="form-group__bar"></i>
										</div>
									</div>
									<div class="col-md-6">
										<label class="control-label">Email</label>
										<div class="form-group">
											<input type="text" id="email" name="email" class="form-control" value="<?php echo $userInfo['email'] ?>">
											<i class="form-group__bar"></i>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label class="control-label">User Type</label>
										<div class="form-group">
											<select id="userType" name="userType" class="form-control">
												<option <?php if ($userInfo['userType'] == "User") {?> selected <?php }?> >User</option>
												<option <?php if ($userInfo['userType'] == "Admin") {?> selected <?php }?> >Admin</option>
											</select>
											<i class="form-group__bar"></i>
										</div>
									</div>
									<div class="col-md-6">
										<label class="control-label">Password</label>
										<div class="form-group">
											<input type="password" id="password" name="password" class="form-control" value="<?php echo $userInfo['password'] ?>">
											<i class="form-group__bar"></i>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 pull-right">
										<div class="btn-demo form-group">
											<button class="btn btn-primary pull-right" id="btnSave">Save</button>
											<button class="btn btn-light pull-right" id="btnClear">Clear</button>
										</div>
									</div>
								</div>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('Header/footer.php'); ?>

<script>
    $(document).ready(function () {
    	$('#data-table').DataTable();
    });	

    function clear() {
    	$('#username').val('');
    	$('#email').val('');
    	document.getElementById('userType').getElementsByTagName('option')[0].selected = 'selected'		
    	$('#password').val('');
		$('#username').focus();
		$('#hidUserId').val('');
		$('#btnSave').html("Save");
    }

    function switchTabNew() {
    	$('#myTab a:first').tab('show');
	}
	$('#btnClear').click(function(){
		clear();
	});   

    $('#btnSave').click(function () {
    	if ($('#username').val() == "") {    		
    		toastr["warning"]("Username is required!");
    		$('#username').focus();
    	} else if ($('#password').val() == "") {    		    	
			toastr["warning"]("Password is required!");
    		$('#password').focus();
    	} else {
    		var userId = 0
    		if ($('#hidUserId').val() == "")
    			userId = <?php echo $userInfo['userId'] ?>;
    		else
    			userId = $('#hidUserId').val();
    		var msg = "";
    		if (userId == 0)
    			msg = "Saved successfully";
    		else if (userId > 0)
    			msg = "Updated successfully";
    		$.ajax({
    			url: '<?php echo site_url('General/Users')?>',
    			datatype: 'json',
    			data: {
    				userId: userId,
    				username: $('#username').val(),
    				email: $('#email').val(),
    				userType: $('#userType option:selected').val(),
    				password: $('#password').val()
    			},
    			method: 'post',
    			success: function (resp) {
    				swal({
    					title: 'Users',
    					text: msg,
    					type: 'success',
    					showConfirmButton: false,
    				})
    				setTimeout(function () {
    					location.reload();
    				}, 800);
    			}
    		});
    	}
    });

    function editUser(userId) {
    	if (userId > 0) {
    		$.ajax({
    			url: '<?php echo site_url('General/userViewById') ?>',
    			datatype: 'json',
    			data: {
    				userId: userId
    			},
    			method: 'post',
    			success: function (resp) {
    				switchTabNew();
    				var user = $.parseJSON(resp);
    				$('#username').val(user['userName']);
					$('#email').val(user['email']);
					if(user['userType']=='User')
						document.getElementById('userType').getElementsByTagName('option')[0].selected = 'selected'
					else if(user['userType']=='Admin')
						document.getElementById('userType').getElementsByTagName('option')[1].selected = 'selected'					
    				$('#password').val(user['password']);
    				$('#btnSave').html('Update');
    				$('#hidUserId').val(user['userId']);
    			}
    		});
    	}
    }

    async function deleteUser(userId) {
    	if (userId > 0) {			

    		swal({
    			title: 'Are you sure?',
    			text: 'This user will be deleted!',
    			type: 'warning',
    			showCancelButton: true,
				confirmButtonColor: 'rgb(221, 51, 51)',
    			confirmButtonClass: 'btn-danger',
    			confirmButtonText: 'Yes, delete it!',
    			cancelButtonClass: 'swal2-cancel swal2-styled'    			
    		}).then((result)=>{
				if(result.value){
					$.ajax({
						url: '<?php echo site_url('General/userDeleteById') ?>',
						datatype: 'json',
						data: {
							userId: userId
						},
						method: 'post',
						success: function (resp) {
							swal({
								title: 'Users',
								text: resp,
								type: 'success',
								showConfirmButton: false,
							})
							setTimeout(function () {
								location.reload();
							}, 800);
						}
					});
				}
			});
    	}
	}

</script>

</body>
</html>