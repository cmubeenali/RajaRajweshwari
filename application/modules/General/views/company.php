<?php $this->load->view('Header/header.php'); ?>
    <div class="container-fluid col-md-8">
    <div class="content__inner">            
        <h3 style="margin-bottom: 1rem;">Company</h1>                    
            <div class="card">
            <div class="card-body">
            <h4 class="card-title">General Info</h4>                                                                        
                <div class="row">
                    <div class="col-md-6">                                
                    <div class="form-group">
                        <input type="text" id="companyName" class="form-control" placeholder="Company Name" value="<?php if(isset($info[0]->companyName)){ echo $info[0]->companyName; } ?>"/>
                    </div>
                    </div>
                    <div class="col-md-6">                                
                    <div class="form-group">
                        <input type="text" id="city" name="city" class="form-control" placeholder="City" value="<?php if(isset($info[0]->city)){ echo $info[0]->city; } ?>"/>
                    </div>
                    </div>
                </div>
                <div class="clearfix mb-2"></div>
                <div class="row">
                    <div class="col-md-12">                                                                
                    <div class="form-group">
                        <textarea class="form-control" id="address" name="address" rows="5" placeholder="Address"><?php if(isset($info[0]->address)){echo $info[0]->address; } ?></textarea>
                    </div>
                    </div>                              
                </div>
                <div class="row">
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" id="gstIn" name="gstIn" placeholder="GSTIN/UIN" value="<?php if(isset($info[0]->gstIn)){ echo $info[0]->gstIn; } ?>"/>
                    </div>
                </div>
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php if(isset($info[0]->phone)){ echo $info[0]->phone; } ?>"/>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email : sample@mail.com" value="<?php if(isset($info[0]->email)){ echo $info[0]->email; } ?>"/>
                    </div>
                </div>
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" id="pincode" name="pincode" placeholder="PIN/ZIP" value="<?php if(isset($info[0]->pincode)){ echo $info[0]->pincode; } ?>"/>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" id="state" name="state" placeholder="State" value="<?php if(isset($info[0]->state)){ echo $info[0]->state; } ?>"/>
                    </div>
                </div>                            
                </div>
                <h4 class="card-title">Other Info</h4>
                <div class="row">
                <div class=col-md-4>                                
                    <h3 class="card-body__title" style="font-size:1rem; color:rgba(255,255,255,.85);">Print Type</h3>                                
                    <div class="form-group">                                    
                        <select id="printType" name="printType" class="select2" style="width: 100%;">
                            <option <?php if($info[0]->printType=="") { ?> selected <?php } ?> >--Select--</option>
                            <option <?php if($info[0]->printType=="A4") { ?> selected <?php } ?> >A4</option>
                            <option <?php if($info[0]->printType=="Dot") { ?> selected <?php } ?> >Dot</option>
                        </select>
                    </div>
                </div>
                <div class=col-md-4 style="display: none;">
                    <label class="control-label">Tax Bill</label>
                    <br>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="taxBillYes" name="taxBill" value="Yes" <?php if($info[0]->taxBill==1) { ?> checked <?php } ?>>
                            <label class="custom-control-label" for="taxBillYes">Yes</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="taxBillNo" name="taxBill" value="No" <?php if($info[0]->taxBill==0) { ?> checked <?php } ?>>
                            <label class="custom-control-label" for="taxBillNo">No</label>
                        </div>
                    </div>
                </div>     
                <div class=col-md-4 style="display: none;">
                    <label class="control-label">Previous Balance</label>
                    <br>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="isPreviousBalanceYes" name="isPreviousBalance" value="Yes" <?php if($info[0]->isPreviousBalance==1) { ?> checked <?php } ?>>
                            <label class="custom-control-label" for="isPreviousBalanceYes">Yes</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="isPreviousBalanceNo" name="isPreviousBalance" value="No" <?php if($info[0]->isPreviousBalance==0) { ?> checked <?php } ?>>
                            <label class="custom-control-label" for="isPreviousBalanceNo">No</label>
                        </div>
                    </div>
                </div>                   
                </div>                          
                <div class="clearfix mb-2"></div>
                <div class="row">
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" id="bankName" name="bankName" class="form-control" placeholder="Bank Name" value="<?php if(isset($info[0]->bankName)){ echo $info[0]->bankName; } ?>"/>
                    </div>
                </div>
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" id="accountNo" name="accountNo" class="form-control" placeholder="A/C No" value="<?php if(isset($info[0]->accountNo)){ echo $info[0]->accountNo; } ?>"/>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class=col-md-6>                                                                
                    <div class="form-group">
                        <input type="text" id="branchAndIfsCode" name="branchAndIfsCode" class="form-control" placeholder="Branch & IFSC" value="<?php if(isset($info[0]->branchAndIfsCode)){ echo $info[0]->branchAndIfsCode; } ?>"/>
                    </div>
                </div>                            
                </div>
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group" style="float:right;">
                        <input type="button" class="btn btn-primary" id="btnSave" value="Update"/>
                    </div>
                </div>
                </div>                                                     
            </div>
            </div>
            </div>

    </div>
    </div>
<?php $this->load->view('Header/footer.php'); ?>

    <script type="text/javascript">      

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        $('#btnSave').click(function(){                                
            if($('#companyName').val()==""){                                    
                toastr["warning"]("Company name is required.!");
                $('#companyName').focus();
            }
            else {   
                var companyName=$('#companyName').val();
                var city=$('#city').val();
                var address=$('#address').val();
                var gstIn=$('#gstIn').val();
                var phone=$('#phone').val();
                var email=$('#email').val();
                var pincode=$('#pincode').val();
                var state=$('#state').val();
                var printType=$('#printType option:selected').val();
                var taxBill=$("input[name='taxBill']:checked").val();
                var isPreviousBalance=$("input[name='isPreviousBalance']:checked").val();
                var bankName=$('#bankName').val();
                var accountNo=$('#accountNo').val();
                var branchAndIfsCode=$('#branchAndIfsCode').val();                    
                $.ajax({
                    url:'<?= site_url('General/Company') ?>',                        
                    datatype:'json',
                    data: {companyName: companyName, city: city, address: address, gstIn:gstIn,
                        phone: phone, email:email, pincode:pincode, state: state,
                        printType: printType, taxBill: taxBill, isPreviousBalance: isPreviousBalance,
                            bankName: bankName, accountNo: accountNo, branchAndIfsCode: branchAndIfsCode },
                    method:'post',
                    success:function(resp){
                        if(resp.length>0){
                            swal({
                                title: 'Company',
                                text: resp,
                                type: 'success',                                
                                confirmButtonClass: 'swal2-confirm swal2-styled'                                
                            })
                        }
                    }
                });                    
            }
        });            
    </script>
</body>

</html>
