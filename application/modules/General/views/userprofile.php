<?php $this->load->view('Header/header.php'); ?>
<div class="container-fluid col-md-8">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">User Profile</h4>
            <div class="card-body">
                <form action="<?= site_url('General/Profile') ?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Username</label>
                            <div class="form-group">
                                <input type="text" id="userName" name="userName" class="form-control"
                                       value="<?php echo $userName ?>" required>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Email</label>
                            <div class="form-group">
                                <input type="text" id="email" name="email" class="form-control"
                                       value="<?php echo $email ?>">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">User Type</label>
                            <div class="form-group">
                                <select id="userType" name="userType" class="form-control">
                                    <option <?php if ($userType == "User") { ?> selected <?php } ?> >User</option>
                                    <option <?php if ($userType == "Admin") { ?> selected <?php } ?> >Admin</option>
                                </select>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Password</label>
                            <div class="form-group">
                                <input type="password" id="password" name="password" class="form-control"
                                       value="<?php echo $password ?>" required>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <div class="btn-demo form-group">
                                <button class="btn btn-default pull-right" id="btnSave">Update Changes</button>
                            </div>
                        </div>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('Header/footer.php'); ?>

<script>

</script>

</body>
</html>