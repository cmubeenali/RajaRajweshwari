<?php

class Mdl_purchaseInvoice extends CI_Model
{

    function getProducts()
    {
        $query = $this->db->get('product_tbl');
        $data = $query->result();
        return $data;
    }

    function getProductById()
    {
        $id = $this->input->post('procode');
        $data = $this->db->query("select GROUP_CONCAT(t1.unitConversionId) as unitConversionIds,t4.productId,t4.productGroupId,t4.unitId,t4.productName,t4.productCode,t4.taxType,t4.taxPercent,
      t4.purchaseRate,t4.wholeSalesRate,t4.wholeSalesRate,t4.retailRate,t4.consumerRate,t4.mrp,t4.mrp,t4.openingStock,t4.entryDate,
      t4.purTaxPercent,t4.reorderLevel,t4.hsnOrSacCode,t4.image,t5.unitName as productUnitName,
      GROUP_CONCAT(t2.unitName) as mainUnitNames,
      GROUP_CONCAT(t1.conversionUnitId) as conversionUnitIds, GROUP_CONCAT(t3.unitName) as conversionUnitName from unitconversion_tbl t1
      inner join unit_tbl t2 on t2.unitId=t1.unitId
      inner join unit_tbl t3 on t3.unitId=t1.conversionUnitId
      inner join product_tbl t4 on t4.unitId=t1.unitId
      inner join unit_tbl t5 on t5.unitId=t4.unitId
      where t4.productId=$id");
        echo json_encode($data->result());
    }

    function voucherNumber($vendorType)
    {
        //voucher no
        $financialYearId = $this->financialYearId();
        $sql="";
        if($vendorType=="Vendor")
        {
            $sql="select IFNULL(MAX(VoucherNo),0) +1 as voucherNo
            from purchasemaster_tbl
            where taxForm='true' and vendorType='Vendor' and financialYearId={$financialYearId}";
        }
        else if($vendorType=="Customer"){
            $sql="select IFNULL(MAX(VoucherNo),0) +1 as voucherNo
            from purchasemaster_tbl
            where vendorType='Customer' and financialYearId={$financialYearId}";
        }
        return $this->db->query($sql)->row_array()['voucherNo'];
    }

    function financialYearId()
    {
        $this->db->where('closed=0');
        $row = $this->db->get('financialyear_tbl')->row_array();
        return $row['financialYearId'];
    }

    function inserNewPurchaseInvoice($dataMaster)
    {
//Master Insert
        $this->db->insert('purchasemaster_tbl', $dataMaster);
        return $masterId = $this->db->insert_id();
    }

    function inserNewPurchaseInvoiceDetails($dataDetails)
    {
        $this->db->insert('purchaseldetails_tbl', $dataDetails);
    }

    function updatePurchaseInvoice($dataMaster, $masterId)
    {
        $this->db->where('purchaseMasterId', $masterId);
        $this->db->update('purchasemaster_tbl', $dataMaster);
    }

    function updatePurchaseInvoiceDetails($dataDetails, $hiddenPurchaseID)
    {
        $this->db->where('purchaseDetailsId', $hiddenPurchaseID);
        $this->db->update('purchaseldetails_tbl', $dataDetails);

    }

    function removeDeleteRows($id)
    {
        $this->db->where('purchaseDetailsId', $id);
        $this->db->delete('purchaseldetails_tbl');
    }

    function transactionAdd($purchaseMasterId)
    {
        $this->Mdl_Transactions->transactionDelete($purchaseMasterId, "Purchase Invoice");
        $masterInfo = $this->purchaseInvoiceViewByIdMaster($purchaseMasterId);
        $transInfo = array(
            'VoucherId' => $purchaseMasterId,
            'VoucherType' => $_SESSION['isWithoutTax'] == "false" ? 'Purchase Invoice' : "Purchase Without",
            'VoucherDate' => $masterInfo['entryDate'],
            'VoucherNo' => ($masterInfo['prefix'] . "/" . $masterInfo['voucherNo']),
            'LedgerId' => $masterInfo['ledgerHead'],
            'Dr' => 0,
            'Cr' => $masterInfo['totalAmount']
        );
        $this->Mdl_Transactions->transactionSave($transInfo);

        $transInfo['LedgerId'] = $masterInfo['purchaseHead'];
        $transInfo['Dr'] = $masterInfo['totalAmount'];
        $transInfo['Cr'] = 0;
        $this->Mdl_Transactions->transactionSave($transInfo);
    }

    function getAllPurchseInvoices()
    {
        $qry_extra = "";
        if ($_SESSION['isWithoutTax'] == "true")
            $qry_extra = "where taxForm='true'";
        else
            $qry_extra = "where taxForm='false'";
        $qry = $this->db->query("select t1.purchaseMasterId, concat(t1.prefix,\"/\",convert (t1.voucherNo,char)) as voucherNo,
                              DATE_FORMAT(t1.entryDate,'%d/%m/%Y')as voucherDate,
                              t1.ledgerHead,
                              t1.purchaseHead,
                              t1.amount,
                              t1.discount,
                              t1.taxAmount,
                              t1.totalAmount,
                              lt.ledgerName,
                              t1.vendorType
                            from purchasemaster_tbl t1
                            inner join ledger_tbl lt on t1.ledgerHead = lt.ledgerId                              
                            {$qry_extra}                            
                            order by t1.purchaseMasterId desc");
        return $qry->result();
    }

    function purchaseMasterAndDetailsDeleteByMasterId($masterId)
    {
        $this->db->where('purchaseMasterId', $masterId);
        $this->db->delete('purchaseldetails_tbl');
        $this->db->where('purchaseMasterId', $masterId);
        $this->db->delete('purchasemaster_tbl');

    }

    function purchaseInvoiceViewByIdMaster($invoiceId)
    {

        $this->db->where('purchaseMasterId', $invoiceId);
        return $qry = $this->db->get('purchasemaster_tbl')->row_array();
    }

    function purchaseInvoiceDetailsViewByIdMaster($invoiceId)
    {
        $query = "select t1.purchaseDetailsId,
  t1.purchaseMasterId,t1.productId,t1.unitId,t1.unitConversionId,t1.unitConversionRate,t1.taxType,t1.taxPercent,t1.qty,t1.rate,t1.netAmount,
  t1.taxAmount,t1.totalAmount,t1.hsnOrSacCode,t1.grossWeight,t1.stoneWeight,t1.stoneRate,t1.nettWeight,t1.wastage,t1.makingCharge,
  pt.retailRate
from purchaseldetails_tbl t1
inner join product_tbl pt on t1.productId = pt.productId
where t1.purchaseMasterId=$invoiceId";
        return $this->db->query($query)->result();

    }
}

?>