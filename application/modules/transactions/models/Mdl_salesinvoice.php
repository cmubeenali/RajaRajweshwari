<?php

class Mdl_salesinvoice extends CI_Model
{
    function productViewById($productId)
    {
        $query = $this->db->query("select GROUP_CONCAT(t1.unitConversionId) as unitConversionIds,t4.productId,t4.productGroupId,t4.unitId,t4.productName,t4.productCode,t4.taxType,t4.taxPercent,
      t4.purchaseRate,t4.wholeSalesRate,t4.wholeSalesRate,t4.retailRate,t4.consumerRate,t4.mrp,t4.mrp,t4.openingStock,t4.entryDate,
      t4.purTaxPercent,t4.reorderLevel,t4.hsnOrSacCode,t4.image,t5.unitName as productUnitName,
      GROUP_CONCAT(t2.unitName) as mainUnitNames,
      GROUP_CONCAT(t1.conversionUnitId) as conversionUnitIds, GROUP_CONCAT(t3.unitName) as conversionUnitName from unitconversion_tbl t1
      inner join unit_tbl t2 on t2.unitId=t1.unitId
      inner join unit_tbl t3 on t3.unitId=t1.conversionUnitId
      inner join product_tbl t4 on t4.unitId=t1.unitId
      inner join unit_tbl t5 on t5.unitId=t4.unitId
      where t4.productId=$productId");
        return $query->result();
    }

    function SalesInvoiceMasterAdd($invoiceInfo)
    {
        $this->db->where('closed=0');
        $row = $this->db->get('financialyear_tbl')->row_array();
        $financialYearId = $row['financialYearId'];

        $this->db->select('IFNULL(MAX(voucherNo),0) +1 as voucherNo');
        $this->db->from('salesmaster_tbl');
        $this->db->where('financialYearId', $financialYearId);
        $this->db->where('salesType', $invoiceInfo['salesType']);
        $row = $this->db->get()->row_array();
        $voucherNo = $row['voucherNo'];

        $invoiceInfo['financialYearId'] = $financialYearId;
        $invoiceInfo['voucherNo'] = $voucherNo;
        $this->db->insert('salesmaster_tbl', $invoiceInfo);
        return $this->db->insert_id();
    }

    function SalesInvoiceDetailsAdd($detailsInfo)
    {
        $this->db->where('productId', $detailsInfo['productId']);
        $row = $this->db->get('product_tbl')->row_array();
        $purchaseRate = $row['purchaseRate'];
        $detailsInfo['purchaseRate'] = $purchaseRate;
        $this->db->insert('salesdetails_tbl', $detailsInfo);
    }

    function SalesInvoiceMasterViewById($masterId)
    {
        $this->db->where('salesMasterId', $masterId);
        $row = $this->db->get('salesmaster_tbl')->row_array();
        return $row;
    }

    function SalesInvoiceViewForTable($salesType)
    {
        $query = $this->db->query('select t1.salesMasterId, concat(t1.prefix,\'/\',convert (t1.voucherNo,char)) as voucherNo,
                              DATE_FORMAT(t1.entryDate,"%d/%m/%Y")as voucherDate,
                              t1.ledgerHead,
                              t1.salesHead,
                              t1.amount,
                              t1.discount,
                              t1.taxAmount,
                              t1.totalAmount,
                              t1.salesType,
                              lt.ledgerName
                            from salesmaster_tbl t1
                            inner join ledger_tbl lt on t1.ledgerHead = lt.ledgerId
                            where t1.salesType="'.$salesType.'"
                            order by t1.salesMasterId desc');
        return $query->result();
    }

    function SalesDetailsViewByMasterId($masterId)
    {
        $query = $this->db->query('select t1.salesDetailsId,t1.salesMasterId,t1.productId,t1.unitId,t1.taxPercent,
            t1.qty,t1.rate,t1.netAmount,t1.taxAmount,t1.totalAmount,t1.hsnOrSacCode,t1.purchaseRate,t1.vaPercent,
            t1.vaAmount,t1.discount, pt.productCode,pt.productName,ReturnUnitName(t1.unitId,ifnull(t1.unitConversionId,0))as unitName,u.unitId as conversionUnit,t1.unitConversionId,pt.retailRate from salesdetails_tbl t1
             inner join product_tbl pt on t1.productId = pt.productId 
             inner join unit_tbl u on pt.unitId = u.unitId 
             where t1.salesMasterId=' . $masterId);
        return $query->result_array();
    }

    function SalesInvoiceMasterEdit($masterId, $masterInfo)
    {
        $this->db->where('salesMasterId', $masterId);
        $this->db->update('salesmaster_tbl', $masterInfo);
    }

    function SalesDetailsEdit($detailsId, $detailsInfo)
    {
        $this->db->where('salesDetailsId', $detailsId);
        $this->db->update('salesdetails_tbl', $detailsInfo);
    }

    function SalesMasterDeleteById($masterId)
    {
        $this->db->where('salesMasterId', $masterId);
        $this->db->delete('salesmaster_tbl');
    }

    function SalesDetailsDeleteByMasterId($masterId)
    {
        $this->db->where('salesMasterId', $masterId);
        $this->db->delete('salesdetails_tbl');
    }

    function SalesDetailsDeleteById($detailsId)
    {
        $this->db->where('salesDetailsId', $detailsId);
        $this->db->delete('salesdetails_tbl');
    }
}

?>