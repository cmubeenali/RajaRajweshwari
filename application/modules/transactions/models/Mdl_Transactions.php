<?php

class Mdl_Transactions extends CI_Model
{


    function fetchcontacts()
    {
        $data = $this->db->get('contacts_tbl');
        return $data->result();
    }

    function fetchLedgerDetails()
    {
        $data = $this->db->get('ledger_tbl');
        return $data->result();
    }

    function addNewReceiptVoucher()
    {
        $prefix = "RV";

        $userId = $_SESSION['userId'];

        if (!$this->input->post('hiddenreceiptvoucherid') == "") {

            $data = array(

                'voucherDate' => $this->input->post('datepicker'),
                'ledgerFirst' => $this->input->post('bankorcash'),
                'ledgerSecond' => $this->input->post('account'),
                'total' => $this->input->post('amount'),
                'description' => $this->input->post('desc'));
            $this->db->where('receiptID', $this->input->post('hiddenreceiptvoucherid'));
            $this->db->update('receiptvoucher_tbl', $data);

            $this->db->where('receiptID', $this->input->post('hiddenreceiptvoucherid'));
            $info = $this->db->get('receiptvoucher_tbl')->row_array();

            $this->transactionDelete($this->input->post('hiddenreceiptvoucherid'), "Receipt Voucher");
            $this->receiptTransactionSave($this->input->post('hiddenreceiptvoucherid'), $info);
        } else {
            $financialYearId = 0;
            $this->db->where('closed=0');
            $row = $this->db->get('financialyear_tbl')->row_array();
            $financialYearId = $row['financialYearId'];

            $this->db->select('IFNULL(MAX(VoucherNo),0) +1 as VoucherNo');
            $this->db->from('receiptvoucher_tbl');
            $this->db->where('financialYearId', $financialYearId);
            $oldvoucher = $this->db->get()->result();

            $VoucherNo = $oldvoucher[0]->VoucherNo + 1;
            $data = array('prefix' => $prefix,
                'VoucherNo' => $VoucherNo,
                'voucherDate' => date('Y-m-d'),
                'ledgerFirst' => $this->input->post('bankorcash'),
                'ledgerSecond' => $this->input->post('account'),
                'total' => $this->input->post('amount'),
                'description' => $this->input->post('desc'),
                'userId' => $_SESSION['userId'],
                'financialYearId' => $financialYearId);
            $this->db->insert('receiptvoucher_tbl', $data);
            $receiptID = $this->db->insert_id();
            $this->receiptTransactionSave($receiptID, $data);
        }

    }

    function receiptTransactionSave($receiptID, $data)
    {
        $transInfo = array('voucherId' => $receiptID,
            'voucherType' => 'Receipt Voucher',
            'voucherDate' => $data['voucherDate'],
            'voucherNo' => $data['prefix'] . "/" . $data['VoucherNo'],
            'ledgerId' => $data['ledgerSecond'],
            'Cr' => $data['total'],
            'Dr' => 0);

        $this->db->insert('transaction_tbl', $transInfo);

        $transInfo['ledgerId'] = $data['ledgerFirst'];
        $transInfo['Cr'] = 0;
        $transInfo['Dr'] = $data['total'];

        $this->db->insert('transaction_tbl', $transInfo);
    }


    function receiptvouchertblfetch()
    {
        $sql="select t1.receiptID,
              t1.prefix,
              t1.entryDate,
              t1.voucherDate,
              t1.VoucherNo,
              t1.description,
              t1.ledgerFirst,
              t1.ledgerSecond,
              t1.total,
              t1.userId,
              t1.financialYearId,
              lt.ledgerName as 'ledgerNameFirst',
              lt2.ledgerName as 'ledgerNameSecond'
              from receiptvoucher_tbl t1
              inner join ledger_tbl lt on t1.ledgerFirst = lt.ledgerId
              inner join ledger_tbl lt2 on t1.ledgerSecond = lt2.ledgerId
              order by t1.receiptID desc ";

        return $this->db->query($sql)->result();
    }

    function delReceiptVoucher()
    {
        $this->transactionDelete($this->input->post('id'), "Receipt Voucher");
        $id = $this->input->post('id');
        $this->db->where('receiptID', $id);
        $this->db->delete('receiptvoucher_tbl');
    }

    function fetchVoucherWithId()
    {
        $id = $this->input->post('id');
        $this->db->where('receiptID', $id);
        $qry = $this->db->get('receiptvoucher_tbl');
        $data = $qry->result();
        echo json_encode($data);

    }

    function transactionDelete($voucherId, $voucherType)
    {
        $this->db->where('voucherType', $voucherType);
        $this->db->where('voucherId', $voucherId);
        $this->db->delete('transaction_tbl');
    }

    function transactionSave($transInfo)
    {
        $this->db->insert('transaction_tbl', $transInfo);
    }
}