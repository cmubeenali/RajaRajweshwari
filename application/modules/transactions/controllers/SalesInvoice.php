<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalesInvoice extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Dashboard/Mdl_dashboard');
        $this->load->model('common/Mdl_Common');
        $this->load->model('Mdl_salesinvoice');
        $this->load->model('Mdl_Transactions');
        $this->load->model('masters/Mdl_Masters');
        $this->load->model('Mdl_salesorder');

    }

    function test()
    {
        $img['text']="BENJITH";
        $img['dudu']="Abhisja";
        echo (json_encode($img));

    }

    function index()
    {
        $_SESSION['pageTitle']="Sales Invoice";
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $info['cashOrParty'] = $this->Mdl_Common->CashOrPartySelectBill();
            $info['salesAccount'] = $this->Mdl_Common->SalesSelectFill();
            $info['productCode'] = $this->Mdl_Common->ProductCodeSelectFill();
            $info['productName'] = $this->Mdl_Common->ProductNameSelectFill();
            $info['unit'] = $this->Mdl_Common->UnitSelectFill();
            $info['tableData'] = $this->Mdl_salesinvoice->SalesInvoiceViewForTable('Sales');
            $info['salesOrder'] = $this->Mdl_salesorder->SalesInvoiceViewForTable('Sales');
            $this->load->view('salesinvoice', $info);
        }
    }

    function invoice()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceID = $this->input->post('invoiceID');
            $msg = "";
            //estimate Bill
            $cashOrParty = $this->input->post('cashOrParty');
            $isCashOrPartyIsOld = $this->input->post('isCashOrPartyIsOld');
            if ($isCashOrPartyIsOld != 'true') {

                $dataLedger = array(
                    'ledgerName' => $cashOrParty,
                    'accountId' => "22",//22=Customer,21=Supplier
                    'tableName' => 'contacts_tbl');

                $data = array(
                    'contactName' => $cashOrParty,
                    'type' => 'Customer'
                );
                $ledgerInfo = $this->Mdl_Masters->addnewContact($dataLedger, $data);
                $cashOrParty=$ledgerInfo['ledgerId'];
            }
            $gstIn=$this->input->post('gstIn');
            $invoiceInfo = array('prefix' => 'INV',
                'entryDate' => $this->input->post('voucherDate'),
                'ledgerHead' => $cashOrParty,
                'salesHead' => $this->input->post('salesAccount'),
                'gstIn'=>$gstIn,
                'amount' => $this->input->post('taxableAmount') == "" ? 0 : $this->input->post('taxableAmount'),
                'salesType' => $this->input->post('salesType'),
                'taxAmount' => $this->input->post('gstAmount') == "" ? 0 : $this->input->post('gstAmount'),
                'discount' => $this->input->post('discount')==""?0:$this->input->post('discount'),
                'totalAmount' => $this->input->post('totalAmount') == "" ? 0 : $this->input->post('totalAmount'),
                'description' => $this->input->post('description'),
                'partyRef' => $this->input->post('partyRef') == "" ? "" : $this->input->post('partyRef')
            );
            $salesOrderMasterId = $this->input->post('salesOrderMasterId');
            if ($salesOrderMasterId > 0)
                $invoiceInfo['salesOrderMasterId'] = $salesOrderMasterId;
            $masterId = 0;
            if ($invoiceID == 0)
                $masterId = $this->Mdl_salesinvoice->SalesInvoiceMasterAdd($invoiceInfo);
            else if ($invoiceID > 0) {
                $this->Mdl_salesinvoice->SalesInvoiceMasterEdit($invoiceID, $invoiceInfo);
                $masterId = $invoiceID;
            }
            $arrSalesDetailsId = json_decode($this->input->post('arrSalesDetailsId'));
            $arrProductId = json_decode($this->input->post('arrProductId'));
            $arrHsnCodes = json_decode($this->input->post('arrHsnCodes'));
            $arrQty = json_decode($this->input->post('arrQty'));
            $arrUnit = json_decode($this->input->post('arrUnit'));
            $arrUnitConversionId = json_decode($this->input->post('arrUnitConversionId'));
            $arrRate = json_decode($this->input->post('arrRate'));
            $arrVaPercent = json_decode($this->input->post('arrVaPercent'));
            $arrValOfSupply = json_decode($this->input->post('arrValOfSupply'));
            $arrDiscount = json_decode($this->input->post('arrDiscount'));
            $arrTaxable = json_decode($this->input->post('arrTaxable'));
            $arrCgst = json_decode($this->input->post('arrCgst'));
            $arrSgst = json_decode($this->input->post('arrSgst'));
            $arrGstDue = json_decode($this->input->post('arrGstDue'));
            $arrTotalAmount = json_decode($this->input->post('arrTotalAmount'));
            for ($i = 0; $i < count($arrProductId); $i++) {
                $detailsInfo = array(
                    'salesMasterId' => $masterId,
                    'productId' => $arrProductId[$i],
                    'unitId' => $arrUnit[$i],
                    'unitConversionId' => $arrUnitConversionId[$i] == 0 ? null : $arrUnitConversionId[$i],
                    'taxPercent' => (($arrCgst[$i] == "" ? 0 : $arrCgst[$i]) + ($arrSgst[$i] == "" ? 0 : $arrSgst[$i])),
                    'qty' => $arrQty[$i],
                    'rate' => $arrRate[$i],
                    'hsnOrSacCode' => $arrHsnCodes[$i],
                    'vaPercent' => $arrVaPercent[$i] == "" ? 0 : $arrVaPercent[$i],
                    'vaAmount' => $arrValOfSupply[$i] == "" ? 0 : $arrValOfSupply[$i],
                    'netAmount' => ($arrTaxable[$i] == "" ? 0 : $arrTaxable[$i]),
                    'discount' => $arrDiscount[$i] == "" ? 0 : $arrDiscount[$i],
                    'taxAmount' => $arrGstDue[$i] == "" ? 0 : $arrGstDue[$i],
                    'totalAmount' => $arrTotalAmount[$i]
                );
                if ($arrSalesDetailsId[$i] == 0)
                    $this->Mdl_salesinvoice->SalesInvoiceDetailsAdd($detailsInfo);
                else if ($arrSalesDetailsId[$i] > 0)
                    $this->Mdl_salesinvoice->SalesDetailsEdit($arrSalesDetailsId[$i], $detailsInfo);
            }

            $arrRemoveIds = json_decode($this->input->post('arrRemoveIds'));
            for ($i = 0; $i < count($arrRemoveIds); $i++) {
                $this->Mdl_salesinvoice->SalesDetailsDeleteById($arrRemoveIds[$i]);
            }
            $this->transactionAdd($masterId);
            $msg=[];
            $msg['id']=$masterId;
            if ($invoiceID == 0){
                $msg['msg'] = "Saved successfully.";
            }

            else if ($invoiceID > 0){
                $msg['msg'] = "Updated successfully.";
            }

            //Update print method
            $printMethod=$this->input->post('printMethod');
            $this->Mdl_Common->UpdatePrintMethod($printMethod);
            $_SESSION['printMethod']=$printMethod;


            echo (json_encode($msg));
        }
    }

    function productViewById()
    {
        $productId = $this->input->post('productId');
        $info = $this->Mdl_salesinvoice->productViewById($productId);
        echo json_encode($info);
    }

    function transactionAdd($salesMasterId)
    {
        $masterInfo = $this->Mdl_salesinvoice->SalesInvoiceMasterViewById($salesMasterId);
        $this->Mdl_Transactions->transactionDelete($salesMasterId, ($masterInfo['salesType'] == "Sales" ? "Sales Invoice" : "Estimate Bill"));
        $transInfo = array(
            'VoucherId' => $salesMasterId,
            'VoucherType' => ($masterInfo['salesType'] == "Sales" ? "Sales Invoice" : "Estimate Bill"),
            'VoucherDate' => $masterInfo['entryDate'],
            'VoucherNo' => ($masterInfo['prefix'] . "/" . $masterInfo['voucherNo']),
            'LedgerId' => $masterInfo['ledgerHead'],
            'Dr' => $masterInfo['totalAmount'],
            'Cr' => 0
        );
        $this->Mdl_Transactions->transactionSave($transInfo);

        $transInfo['LedgerId'] = $masterInfo['salesHead'];
        $transInfo['Dr'] = 0;
        $transInfo['Cr'] = $masterInfo['totalAmount'];
        $this->Mdl_Transactions->transactionSave($transInfo);
    }

    function salesInvoiceViewById()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceId = $this->input->post('invoiceID');
            if ($invoiceId > 0) {
                $info = $this->Mdl_salesinvoice->SalesInvoiceMasterViewById($invoiceId);
                $detailsArr = $this->Mdl_salesinvoice->SalesDetailsViewByMasterId($invoiceId);
                $info['details'] = $detailsArr;
                echo json_encode($info);
            }
        }
    }

    function SalesInvoiceDeleteById()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceId = $this->input->post('invoiceID');
            $this->Mdl_Transactions->transactionDelete($invoiceId, "Sales Invoice");
            $this->Mdl_salesinvoice->SalesDetailsDeleteByMasterId($invoiceId);
            $this->Mdl_salesinvoice->SalesMasterDeleteById($invoiceId);
            echo "success";
        }
    }

}

