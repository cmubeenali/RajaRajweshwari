<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseInvoice extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Mdl_purchaseInvoice');
        $this->load->model('Dashboard/Mdl_dashboard');
        $this->load->model('Common/Mdl_Common');
        $this->load->model('Mdl_Transactions');
        $this->load->model('masters/Mdl_Masters');
    }

    function without()
    {
        $_SESSION['isWithoutTax'] = "true";
        $_SESSION['pageTitle'] = "Purchase Invoice";
        $data['cashorbill'] = $this->Mdl_Common->CashOrPartySelectBill();
        $data['selectbill'] = $this->Mdl_Common->purchaseSelectBill();
        $data['product'] = $this->Mdl_purchaseInvoice->getProducts();
        //list of all invoice (table)
        $data['tablelist'] = $this->Mdl_purchaseInvoice->getAllPurchseInvoices();
        $this->load->view('PurchaseInvoice', $data);
    }

    function index()
    {
        $_SESSION['isWithoutTax'] = "false";
        $_SESSION['pageTitle'] = "Purchase Invoice";
        $data['cashorbill'] = $this->Mdl_Common->CashOrPartySelectBill();
        $data['selectbill'] = $this->Mdl_Common->purchaseSelectBill();
        $data['product'] = $this->Mdl_purchaseInvoice->getProducts();
        //list of all invoice (table)
        $data['tablelist'] = $this->Mdl_purchaseInvoice->getAllPurchseInvoices();
        $this->load->view('PurchaseInvoice', $data);
    }

    function addNewPurchaseInvoiceForm()
    {
        //Update print method
        $printMethod = $this->input->post('printMethod');
        $this->Mdl_Common->UpdatePrintMethod($printMethod);
        $_SESSION['printMethod'] = $printMethod;


        //hidden value for updation checking
        $hiddenMasterID = $this->input->post('invoiceID');
        $VoucherNo = "";
        if ($hiddenMasterID == "") {
            //voucher no
            $VoucherNo = $this->Mdl_purchaseInvoice->voucherNumber("Vendor");
            if ($_SESSION['isWithoutTax'] == "false") {
                $VoucherNo = $this->input->post('voucherNo');
            }
        }
        //financial year
        $financialYearId = $this->Mdl_purchaseInvoice->financialYearId();
        $date = $this->input->post('date');
        $cashorbill = $this->input->post('cashorbill');
        $purchaseac = $this->input->post('purchaseac');
        $invoiceNumber = $this->input->post('invoiceNumber');

        //table_datas
        $arrPurchaseDetailsId = json_decode($this->input->post('arrPurchaseDetailsId'));
        $arrProductId = json_decode($this->input->post('arrProductId'));
        $arrHsnCodes = json_decode($this->input->post('arrHsnCodes'));
        $arrQty = json_decode($this->input->post('arrQty'));
        $arrUnit = json_decode($this->input->post('arrUnit'));
        $arrUnitConversionId = json_decode($this->input->post('arrUnitConversionId'));
        $arrRate = json_decode($this->input->post('arrRate'));
        $arrTaxable = json_decode($this->input->post('arrTaxable'));
        $arrCgst = json_decode($this->input->post('arrCgst'));
        $arrSgst = json_decode($this->input->post('arrSgst'));
        $arrGstDue = json_decode($this->input->post('arrGstDue'));
        $arrTotalAmount = json_decode($this->input->post('arrTotalAmount'));

        $desc = $this->input->post('description');
        //$prevbalance=$this->input->post('prevbalance');
        //$totaloutstanding=$this->input->post('totaloutstanding');
        $gstamount = $this->input->post('gstAmount');
        $netamount = $this->input->post('taxableAmount');
        //$discountpercent=$this->input->post('discountpercent');
        $discount = $this->input->post('discount');
        $totalamount = $this->input->post('totalAmount');
        //$paidamount=$this->input->post('paidamount');
        $isCashOrPartyIsOld = $this->input->post('isCashOrPartyIsOld');
        $gstIn = $this->input->post('gstIn');
        $vendorType = $this->input->post('vendorType');
        if ($isCashOrPartyIsOld != 'true') {

            $dataLedger = array(
                'ledgerName' => $cashorbill,
                'accountId' => "21",//22=Customer,21=Supplier
                'tableName' => 'contacts_tbl');

            $data = array(
                'contactName' => $cashorbill,
                'type' => 'Customer'
            );
            $ledgerInfo = $this->Mdl_Masters->addnewContact($dataLedger, $data);
            $cashorbill = $ledgerInfo['ledgerId'];
        }
        //data purchse invoice master
        $dataMaster = array('prefix' => "PUR",
            'entryDate' => $date,
            'ledgerHead' => $cashorbill,
            'purchaseHead' => $purchaseac,
            'gstIn' => $gstIn,
            'amount' => $netamount,
            'taxAmount' => $gstamount,
            'discount' => $discount,
            'totalAmount' => $totalamount,
            'description' => $desc,
            'purInvoiceNo' => $invoiceNumber == "" ? "" : $invoiceNumber,
            'financialYearId' => $financialYearId,
            'taxForm' => $_SESSION['isWithoutTax'],
            'vendorType' => $vendorType
        );
        if ($hiddenMasterID == "")
            $dataMaster['voucherNo'] = $VoucherNo;
        if ($hiddenMasterID == "") {
            $purchaseMasterId = $this->Mdl_purchaseInvoice->inserNewPurchaseInvoice($dataMaster);
            $this->Mdl_purchaseInvoice->transactionAdd($purchaseMasterId);

            for ($i = 0; $i < count($arrProductId); $i++) {
                $dataDetails = array('purchaseMasterId' => $purchaseMasterId,
                    'productId' => $arrProductId[$i],
                    'unitId' => $arrUnit[$i],
                    'unitConversionId' => $arrUnitConversionId[$i] == 0 ? null : $arrUnitConversionId[$i],
                    'taxPercent' => ($arrCgst[$i] + $arrSgst[$i]),
                    'qty' => $arrQty[$i],
                    'rate' => $arrRate[$i],
                    'netAmount' => $arrTaxable[$i],
                    'taxAmount' => $arrGstDue[$i],
                    'totalAmount' => $arrTotalAmount[$i],
                    'hsnOrSacCode' => $arrHsnCodes[$i]
                );
                $this->Mdl_purchaseInvoice->inserNewPurchaseInvoiceDetails($dataDetails);

                //Update product purchase rate
                $this->Mdl_Masters->UpdatePurchaseRate($arrProductId[$i], $arrRate[$i]);
            }
            $msg = [];
            $msg['id'] = $purchaseMasterId;
            $msg['msg'] = "Saved successfully.";
            echo json_encode($msg);
        } else {
            $purchaseMasterId = $this->Mdl_purchaseInvoice->updatePurchaseInvoice($dataMaster, $hiddenMasterID);
            $this->Mdl_purchaseInvoice->transactionAdd($hiddenMasterID);

            for ($i = 0; $i < count($arrProductId); $i++) {
                $dataDetails = array(
                    'purchaseMasterId' => $hiddenMasterID,
                    'productId' => $arrProductId[$i],
                    'unitId' => $arrUnit[$i],
                    'unitConversionId' => $arrUnitConversionId[$i] == 0 ? null : $arrUnitConversionId[$i],
                    'taxPercent' => ($arrCgst[$i] + $arrSgst[$i]),
                    'qty' => $arrQty[$i],
                    'rate' => $arrRate[$i],
                    'netAmount' => $arrTaxable[$i],
                    'taxAmount' => $arrGstDue[$i],
                    'totalAmount' => $arrTotalAmount[$i],
                    'hsnOrSacCode' => $arrHsnCodes[$i]
                );
                if ($arrPurchaseDetailsId[$i] == 0) {
                    $this->Mdl_purchaseInvoice->inserNewPurchaseInvoiceDetails($dataDetails);
                } else if ($arrPurchaseDetailsId[$i] > 0) {
                    $this->Mdl_purchaseInvoice->updatePurchaseInvoiceDetails($dataDetails, $arrPurchaseDetailsId[$i]);
                }

            }

            //fetching arrRremoving ids- purchse
            $arrRemoveIds = json_decode($this->input->post('arrRemoveIds'));
            for ($i = 0; $i < count($arrRemoveIds); $i++) {
                $this->Mdl_purchaseInvoice->removeDeleteRows($arrRemoveIds[$i]);
            }

            $msg = [];
            $msg['id'] = $hiddenMasterID;
            $msg['msg'] = "Updated successfully.";

            echo json_encode($msg);
        }


    }

    function getProductById()
    {

        $this->Mdl_purchaseInvoice->getProductById();
    }

    function purchaseInvoiceDeleteById()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $masterId = $this->input->post('invoiceID');
            //$this->Mdl_Transactions->transactionDelete($invoiceId, "Sales Invoice");
            $this->Mdl_purchaseInvoice->purchaseMasterAndDetailsDeleteByMasterId($masterId);

            echo "success";
        }
    }

    function purchaseInvoiceViewById()
    {
        $invoiceId = $this->input->post('invoiceID');
        $info = $this->Mdl_purchaseInvoice->purchaseInvoiceViewByIdMaster($invoiceId);
        $detailsArr = $this->Mdl_purchaseInvoice->purchaseInvoiceDetailsViewByIdMaster($invoiceId);
        $info['details'] = $detailsArr;
        echo json_encode($info);
    }

    function GetPurchaseVoucherMax(){
        echo $this->Mdl_purchaseInvoice->voucherNumber("Customer");
    }

}

?>
