<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Estimate extends MX_Controller {
function __construct(){
	parent::__construct();
	$this->load->database();
	$this->load->library('session');
	$this->load->model('Mdl_estimate');
	$this->load->model('Dashboard/Mdl_dashboard');
	$this->load->model('Common/Mdl_Common');
	$this->load->model('Mdl_purchaseInvoice');
	$this->load->model('Mdl_salesinvoice');
}

 function index(){
     $_SESSION['pageTitle']="Estimate";
 	 if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $info['cashOrParty'] = $this->Mdl_Common->CashOrPartySelectBill();
            $info['salesAccount'] = $this->Mdl_Common->SalesSelectFill();
            $info['productCode'] = $this->Mdl_Common->ProductCodeSelectFill();
            $info['productName'] = $this->Mdl_Common->ProductNameSelectFill();
            $info['unit'] = $this->Mdl_Common->UnitSelectFill();
            $info['tableData'] = $this->Mdl_salesinvoice->SalesInvoiceViewForTable($salesType='Estimate');
            $this->load->view('estimate', $info);
        }
 }

 


}