<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalesOrder extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Dashboard/Mdl_dashboard');
        $this->load->model('common/Mdl_Common');
        $this->load->model('Mdl_salesorder');
        $this->load->model('Mdl_Transactions');
        $this->load->model('masters/Mdl_Masters');

    }

    function test()
    {
        $isCashOrPartyIsOld = $this->input->post('isCashOrPartyIsOld');
        $cashOrParty = $this->input->post('cashOrParty');
        if ($isCashOrPartyIsOld != 'true') {

            $dataLedger = array(
                'ledgerName' => $cashOrParty,
                'accountId' => "22",//22=Customer,21=Supplier
                'tableName' => 'contacts_tbl');

            $data = array(
                'contactName' => $cashOrParty,
                'type' => 'Customer'
            );
            print_r($data);
        }

    }

    function index()
    {
        $_SESSION['pageTitle']="Sales Order";
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $info['cashOrParty'] = $this->Mdl_Common->CashOrPartySelectBill();
            $info['salesAccount'] = $this->Mdl_Common->SalesSelectFill();
            $info['productCode'] = $this->Mdl_Common->ProductCodeSelectFill();
            $info['productName'] = $this->Mdl_Common->ProductNameSelectFill();
            $info['unit'] = $this->Mdl_Common->UnitSelectFill();
            $info['tableData'] = $this->Mdl_salesorder->SalesInvoiceViewForTable('Sales');
            $this->load->view('salesorder', $info);
        }
    }

    function invoice()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceID = $this->input->post('invoiceID');
            $msg = "";
            //estimate Bill
            $cashOrParty = $this->input->post('cashOrParty');
            $isCashOrPartyIsOld = $this->input->post('isCashOrPartyIsOld');
            if ($isCashOrPartyIsOld != 'true') {

                $dataLedger = array(
                    'ledgerName' => $cashOrParty,
                    'accountId' => "22",//22=Customer,21=Supplier
                    'tableName' => 'contacts_tbl');

                $data = array(
                    'contactName' => $cashOrParty,
                    'type' => 'Customer'
                );
                $cashOrParty = $this->Mdl_Masters->addnewContact($dataLedger, $data);
            }

            $invoiceInfo = array('prefix' => 'SO',
                'entryDate' => $this->input->post('voucherDate'),
                'ledgerHead' => $cashOrParty,
                'salesHead' => $this->input->post('salesAccount'),
                'amount' => $this->input->post('taxableAmount') == "" ? 0 : $this->input->post('taxableAmount'),
                'discount' => $this->input->post('discount')==""?0:$this->input->post('discount'),
                'salesType' => $this->input->post('salesType'),
                'taxAmount' => $this->input->post('gstAmount') == "" ? 0 : $this->input->post('gstAmount'),
                'totalAmount' => $this->input->post('totalAmount') == "" ? 0 : $this->input->post('totalAmount'),
                'description' => $this->input->post('description'),
                'partyRef' => $this->input->post('partyRef') == "" ? "" : $this->input->post('partyRef')
            );
            $masterId = 0;
            if ($invoiceID == 0)
                $masterId = $this->Mdl_salesorder->SalesInvoiceMasterAdd($invoiceInfo);
            else if ($invoiceID > 0) {
                $this->Mdl_salesorder->SalesInvoiceMasterEdit($invoiceID, $invoiceInfo);
                $masterId = $invoiceID;
            }
            $arrSalesDetailsId = json_decode($this->input->post('arrSalesDetailsId'));
            $arrProductId = json_decode($this->input->post('arrProductId'));
            $arrHsnCodes = json_decode($this->input->post('arrHsnCodes'));
            $arrQty = json_decode($this->input->post('arrQty'));
            $arrUnit = json_decode($this->input->post('arrUnit'));
            $arrUnitConversionId = json_decode($this->input->post('arrUnitConversionId'));
            $arrRate = json_decode($this->input->post('arrRate'));
            $arrVaPercent = json_decode($this->input->post('arrVaPercent'));
            $arrValOfSupply = json_decode($this->input->post('arrValOfSupply'));
            $arrDiscount = json_decode($this->input->post('arrDiscount'));
            $arrTaxable = json_decode($this->input->post('arrTaxable'));
            $arrCgst = json_decode($this->input->post('arrCgst'));
            $arrSgst = json_decode($this->input->post('arrSgst'));
            $arrGstDue = json_decode($this->input->post('arrGstDue'));
            $arrTotalAmount = json_decode($this->input->post('arrTotalAmount'));
            for ($i = 0; $i < count($arrProductId); $i++) {
                $detailsInfo = array(
                    'salesOrderMasterId' => $masterId,
                    'productId' => $arrProductId[$i],
                    'unitId' => $arrUnit[$i],
                    'unitConversionId' => $arrUnitConversionId[$i] == 0 ? null : $arrUnitConversionId[$i],
                    'taxPercent' => (($arrCgst[$i] == "" ? 0 : $arrCgst[$i]) + ($arrSgst[$i] == "" ? 0 : $arrSgst[$i])),
                    'qty' => $arrQty[$i],
                    'rate' => $arrRate[$i],
                    'hsnOrSacCode' => $arrHsnCodes[$i],
                    'vaPercent' => $arrVaPercent[$i] == "" ? 0 : $arrVaPercent[$i],
                    'vaAmount' => $arrValOfSupply[$i] == "" ? 0 : $arrValOfSupply[$i],
                    'netAmount' => ($arrTaxable[$i] == "" ? 0 : $arrTaxable[$i]),
                    'discount' => $arrDiscount[$i] == "" ? 0 : $arrDiscount[$i],
                    'taxAmount' => $arrGstDue[$i] == "" ? 0 : $arrGstDue[$i],
                    'totalAmount' => $arrTotalAmount[$i]
                );
                if ($arrSalesDetailsId[$i] == 0)
                    $this->Mdl_salesorder->SalesInvoiceDetailsAdd($detailsInfo);
                else if ($arrSalesDetailsId[$i] > 0)
                    $this->Mdl_salesorder->SalesDetailsEdit($arrSalesDetailsId[$i], $detailsInfo);
            }

            $arrRemoveIds = json_decode($this->input->post('arrRemoveIds'));
            for ($i = 0; $i < count($arrRemoveIds); $i++) {
                $this->Mdl_salesorder->SalesDetailsDeleteById($arrRemoveIds[$i]);
            }
            $this->transactionAdd($masterId);
            if ($invoiceID == 0)
                $msg = "Saved successfully.";
            else if ($invoiceID > 0)
                $msg = "Updated successfully.";
            echo $msg;
        }
    }

    function productViewById()
    {
        $productId = $this->input->post('productId');
        $info = $this->Mdl_salesorder->productViewById($productId);
        echo json_encode($info);
    }

    function transactionAdd($salesMasterId)
    {
        $this->Mdl_Transactions->transactionDelete($salesMasterId, "Sales Order");
        $masterInfo = $this->Mdl_salesorder->SalesInvoiceMasterViewById($salesMasterId);
        $transInfo = array(
            'VoucherId' => $salesMasterId,
            'VoucherType' => 'Sales Order',
            'VoucherDate' => $masterInfo['entryDate'],
            'VoucherNo' => ($masterInfo['prefix'] . "/" . $masterInfo['voucherNo']),
            'LedgerId' => $masterInfo['ledgerHead'],
            'Dr' => $masterInfo['totalAmount'],
            'Cr' => 0
        );
        $this->Mdl_Transactions->transactionSave($transInfo);

        $transInfo['LedgerId'] = $masterInfo['salesHead'];
        $transInfo['Dr'] = 0;
        $transInfo['Cr'] = $masterInfo['totalAmount'];
        $this->Mdl_Transactions->transactionSave($transInfo);
    }

    function salesInvoiceViewById()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceId = $this->input->post('invoiceID');
            if ($invoiceId > 0) {
                $info = $this->Mdl_salesorder->SalesInvoiceMasterViewById($invoiceId);
                $detailsArr = $this->Mdl_salesorder->SalesDetailsViewByMasterId($invoiceId);
                $info['details'] = $detailsArr;
                echo json_encode($info);
            }
        }
    }

    function SalesInvoiceDeleteById()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $invoiceId = $this->input->post('invoiceID');
            $this->Mdl_Transactions->transactionDelete($invoiceId, "Sales Order");
            $this->Mdl_salesorder->SalesDetailsDeleteByMasterId($invoiceId);
            $this->Mdl_salesorder->SalesMasterDeleteById($invoiceId);
            echo "success";
        }
    }

}

?>