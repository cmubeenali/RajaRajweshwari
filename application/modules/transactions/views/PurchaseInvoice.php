<?php $this->load->view('Header/header.php'); ?>
<div class="container-fluid">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Purchase Invoice</h4>
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#addnew" role="tab" id="navNew">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#list" role="tab" id="navList">List</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="addnew" role="tabpanel">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <!-- hidden input for edit purpose, it get purchasel invoice id  -->
                                        <input hidden type="text" name="hidden_purchaseInvoiceID" id="hidden_purchaseInvoiceID"  value="">
                                        <!-- prefix and voucher no -->
                                        <?php if($_SESSION['isWithoutTax']=="false"){ ?>
                                            <input type="text" class="form-control focusable" name="prefix" id="prefix" placeholder="Voucher No">
                                        <?php }else { ?>
                                            <input disabled type="text" class="form-control" name="prefix" id="prefix" value="PUR/###">
                                        <?php } ?>
                                        <br>
                                        <select class="form-control" name="vendorType" id="vendorType" style="width: 100%">
                                            <option selected value="Vendor">Vendor</option>
                                            <option value="Customer">Customer</option>
                                        </select>
                                        <br>
                                        <!-- cash/party -->
                                        <select class="form-control " name="cashorbill" id="cashorbill" style="width: 100%" >
                                            <option value="0" style="color: #9d9ba8">Select Cash/Party</option>
                                            <?php
                                            foreach ($cashorbill as $keybill) { ?>
                                               <option value="<?= $keybill->ledgerId; ?>"><?=$keybill->ledgerName;?></option>
                                         <?php  } ?>                                             
                                        </select>
                                        <br>
                                        <br>
                                        <!-- purchase a/c-->
                                        <select class="form-control select2" name="purchaseac" id="purchaseac" style="width: 100%">
                                             
                                            <?php
                                            foreach ($selectbill as $keyselect ) { ?>
                                                 <option value="<?= $keyselect->ledgerId; ?>"><?=$keyselect->ledgerName;?></option>
                                          <?php } ?> 
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        
                                        <!-- date -->
                                       <input type="date" name="date" class="form-control focusable" id="datePicker" value="<?= date('Y-m-d');?>">
                                        <br>
                                        <!-- GSTIN -->
                                        <?php if($_SESSION['isWithoutTax']=="true"){ ?>
                                            <input  type="text" class="form-control focusable" name="gstin" id="gstin" placeholder="GSTIN" style="display:none;">
                                        <?php }else { ?>
                                            <input  type="text" class="form-control focusable" name="gstin" id="gstin" placeholder="GSTIN">
                                        <?php } ?>
                                        <!-- Invoice NO -->
                                        <input  type="text" class="form-control focusable" name="invoiceno" id="invoiceno" placeholder="Invoice Number" style="display:none;">
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- Full width Table -->
                            <div class="row eklavyapurchaseinvoicewhitediv">
                                <div class="table-responsive col-md-12" >
                                    <table class="table table-bordered  mb-0" id="table_purinvoice">
                                        <thead class="thead-inverse"  >
                                            <tr >
                                                <th width="1%" style="text-align: center;" onclick="addRow()"><i class="mdi mdi-table-row-plus-after" style="font-size: 18px;" ></i></th>
                                                <th width="1%" style="text-align: center;">#</th>
                                                <th width="5%" style="text-align: center;" >Purity</th>
                                                <th width="8%" style="text-align: center;" >Product</th>
                                                <th width="2%" style="text-align: center;">HSN </th>
                                                <th width="4%" style="text-align: center;">Qty</th>
                                                <th width="4%" style="text-align: center;">Unit </th>
                                                <th width="4%" style="text-align: center;">Rate</th>
                                                <th width="4%" style="text-align: center;" class="hidable">Taxable</th>
                                                <th width="3%" style="text-align: center;" class="hidable">CGST %  </th>
                                                <th width="5%" style="text-align: center;" class="hidable">SGST% </th>
                                                <th width="5%" style="text-align: center;" class="hidable">GST Due </th>
                                                <th width="5%" style="text-align: center;">Total </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tablebody">
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- footer options -->
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <textarea class="form-control desc focusable" rows="5" name="desc" placeholder="Description...."></textarea>
                                        <i class="form-group__bar"></i>
                                    </div>
                                    <div class="form-group">
                                        <!-- previous & total outstanding balance -->
                                        <!-- <div class="w-100"></div>
                                        <label>Previous Balance :&nbsp;</label>
                                        <input type="number" class="eklavya_inputBtmBrdr" name="prevbalance">
                                        <div class="w-100"></div>
                                        <label>Total Outstanding:&nbsp;</label>
                                        <input type="number" class="eklavya_inputBtmBrdr" name="totaloutstanding"> -->
                                        
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    
                                </div>
                                <div class="col-md-3">
                                    <table>
                                        <tr>
                                            <td>
                                                <label style="display: none;">GST Amount&nbsp;&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="text" class="eklavya_inputBtmBrdr" disabled="disabled" name="gstamount" id="gstAmount" style="display: none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="hidableTax">CGST&nbsp;&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="text" class="eklavya_inputBtmBrdr hidableTax" disabled="disabled" name="cgstAmount" id="cgstAmount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="hidableTax">SGST&nbsp;&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="text" class="eklavya_inputBtmBrdr hidableTax" disabled="disabled" name="sgstAmount" id="sgstAmount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Net Taxable&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="text" class="eklavya_inputBtmBrdr" disabled="disabled"  name="nettaxable" id="nettaxable" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Rounded&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                            </td>
                                            <td>
                                                <input type="number" class="eklavya_inputBtmBrdr"  name="roundedFinalAmt" id="roundedFinalAmt" disabled="disabled">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Amount&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="text" id="totalAmount" class="eklavya_inputBtmBrdr" disabled="disabled"  name="totalamount">
                                            </td>
                                        </tr>
                                    </table>
<!--                                    <div class="w-100"></div>-->
<!--                                    <div class="w-100"></div>-->
<!--                                    <div class="w-100"></div>-->
<!--                                    <div class="w-100"></div>-->
                                    <!-- <label>Discount %&nbsp;&nbsp;&nbsp; :</label>
                                    <input type="number" class="eklavya_inputBtmBrdr"  name="discountpercent">
                                    <div class="w-100"></div> -->


<!--                                    <div class="w-100"></div>-->
<!---->
<!---->
<!--                                    <div class="w-100"></div>-->
                                    <!-- <label>paid Amount&nbsp;&nbsp;:</label>
                                    <input type="number" class="eklavya_inputBtmBrdr"  name="paidamount"> -->
                                    
                                    
                                </div>
                                
                                
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <!-- <label> Interstate <br> Purchase</label>
                                        <div class="toggle-switch toggle-switch--blue">
                                            <input type="checkbox" class="toggle-switch__checkbox">
                                            <i class="toggle-switch__helper"></i>
                                        </div> -->
                                        <!-- <label>Print Mode  </label> -->
                                        <select class="form-control" name="printmethod" id="printmethod" style="width: 100%">
                                            <option selected value="1">Print</option>
                                            <option value="2">Without Print</option>
                                            <!-- <option value="4">Barcode Print</option> -->
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 "></div>
                                <div class="col-md-4 text-center" >
                                    <br>
                                    <br>
                                    
                                    <!-- <button  type="submit" name="submit" id="savebtn" class="btn btn-primary">Save 
                                    </button> -->
                                    <input type="button"  value="Save" id="btnSave" onclick="saveOrUpdate()" class="btn btn-primary">
                                    <button  type="button"  id="clearbtn" class="btn " onclick="location.reload();">Clear 
                                    </button>
                                    <button disabled="disabled" style="" type="button" name="dltbtn" id="dltbtn" class="btn btn-danger" onclick="purchaseInvoiceDelete($('#hidden_purchaseInvoiceID').val())">Delete </button>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                  <!--   list all invoice -->
                    <div class="tab-pane fade" id="list" name="list" role="tabpanel">
                        <br>
                        <div class="table-responsive table-hover" id="tblData" name="tblData">
                            <table id="data-table" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Voucher No</th>
                                    <th>Voucher Date</th>
                                    <th>Ledger</th>
                                    <th>Source</th>
                                    <th>Taxable</th>
                                    <th>GST Amt.</th>
                                    <th>Total Amt.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach ($tablelist as $key) {?>
                                    <tr>
                                        <th scope="row"><?=$i;?></th>
                                        <td><?=$key->voucherNo;?></td>
                                        <td><?=$key->voucherDate;?></td>
                                        <td><?=$key->ledgerName;?></td>
                                        <td><?=$key->vendorType;?></td>
                                        <td><?=$key->amount;?></td>
                                        <td><?=$key->taxAmount;?></td>
                                        <td><?=$key->totalAmount;?></td>
                                        <td><i class="mdi mdi-pencil" style="cursor:pointer;" onclick="puchaseInvoiceEdit(<?=$key->purchaseMasterId;?>)"> </i>/<i class="mdi mdi-delete" style="cursor:pointer;" onclick="purchaseInvoiceDelete(<?=$key->purchaseMasterId;?>)">  </i></td>
                                    </tr>
                                    <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('Header/footer.php'); ?>

<script>
    // enter == tab key
jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});
$(document).on('keypress', 'input,select', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});
</script>

<!-- Javascript -->

<script type="text/javascript">



    $('#data-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
        ]
    });

    $('#cashorbill').select2({
     tags: true
});

    checkRemoveBtn();
    var jsonobj="";
    var arrRemoveIds=[];
    var addSerialNumber = function () {
    var i = 0;
    $('#table_purinvoice tr').each(function(index) {        
        $(this).find('td:nth-child(2)').html(index++);
    });
}; 
addRow();


function addRow(){
 
 
      
$('#tablebody').append('<tr><th scope="row" ><button class="btn btn-light removeBtn" style="width:100%;"><i class=" mdi mdi-table-row-remove " style="color: #ff0018; font-size: 18px; " ></i></button></th><td style="text-align:center;"> <input value="" style="border: none; padding: 0;" type="text" class="form-control slNoId" disabled  name="slNoId[]"></td><td> <input type="text" name="hiddenDetailsId[]" value="0" hidden="hidden"  class="hiddenDetailsId" > <select class="myslct procode" required style="border: none; width:100%; padding: 0;" name="code[]" onchange="procode(this,event)"><option></option><?php foreach ($product as $codepro ) { ?><option value="<?=$codepro->productId?>"><?=$codepro->productCode?></option><?php } ?></select></td><td > <select class="myslct proname" style="border: none; padding: 0; width:100%; background:none;color:white;" name="product[]" onchange="proname(this)"><option></option><?php foreach ($product as $namepro ) { ?><option value="<?=$namepro->productId?>"><?=$namepro->productName?></option><?php } ?></select></td><td> <input style="border: none; padding: 0;text-align:center" class="hsnsac form-control" name="hsnsac[]" type="text" ></td><td style="text-align:center;"> <input style="border: none; padding: 0;text-align:center" type="text" class="form-control quantity" name="quantity[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;"> <select style="border: none;width:100%; padding: 0;text-align:center" type="text" class="form-control unit" name="unit[]" onchange="callCallculateAmountRow(this)"></select></td><td style="text-align:center;"> <input type="hidden" name="hidRate[]" class="hidRate" value="0"> <input type="hidden" name="hidUnitId[]" class="hidUnitId" value="0"> <input type="hidden" name="hidUnitConversionId[]" class="hidUnitConversionId" value="0"> <input style="border: none; padding: 0;text-align:center" type="text" class="form-control rate" name="rate[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;" class="hidable"> <span class="taxableamounttbl" name="taxableamounttbl[]" value=""></span></td><td style="text-align:center;" class="hidable"> <input style="border: none; padding: 0;text-align:center" type="text" class="form-control cgst" name="cgst[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;" class="hidable"> <input style="border: none; padding: 0;text-align:center" type="text" class="sgst form-control" name="sgst[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;" class="hidable"> <input style="border: none; padding: 0;text-align:center" type="text" class="gstdue form-control" name="gstdue[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;"> <span class="totalamounttbl" name="totalamounttbl[]"></span></td></tr>');
addSerialNumber();
$('.procode').select2();
$('.proname').select2();
$('.unit').select2();
$('#table_purinvoice td ').css('padding','0px 0px');
$('#table_purinvoice td ').css('vertical-align','middle');
$('#table_purinvoice th ').css('padding','0px 0px');
$(' .select2-container--default .select2-selection--single').css('border','none');
hideCols();

generalSettings();

}


$('#table_purinvoice').on('click', 'button', function(e){
var detailsId= $(this).closest('tr').find('.hiddenDetailsId').val();
    if(detailsId>0)
        arrRemoveIds.push(detailsId);
$(this).closest('tr').remove();
addSerialNumber();
calcTotalAmount();
checkRemoveBtn();

});
function checkRemoveBtn () {
    if($('.removeBtn').length==0){
       $('#gstAmount').val("0.00");
       $('#nettaxable').val("0.00");
       $('#totalAmount').val("0.00");
    }
}
//TABLE HANDLING FUNCTIONS
function procode(id,e){ //procode change
var procode = id.value;

var tab_index= $(id).closest('tr');
if(tab_index.find('.hiddenDetailsId').val()==0){
    tab_index.find('.unit').find('option').remove();
    var select2class="procode";
    getproductdetails(tab_index,procode,select2class,id);//ajax
}
}
function proname(id,e){ // name change
   var procode = id.value;
var tab_index= $(id).closest('tr');
    if(tab_index.find('.hiddenDetailsId').val()==0){
        tab_index.find('.unit').find('option').remove();
        var select2class="proname";
        getproductdetails(tab_index,procode,select2class,id);//ajax
    }
}

//calcFun
function calculateAmountsByRowIndex(row_index,id){
    var mainUnit=row_index.find('.hidUnitId').val();
    var currentUnitId=row_index.find('.unit').val();
    $.post('<?=site_url('UnitConversion/unitConversionViewByBothUnitIds');?>',{mainUnit:mainUnit,conversionUnit:currentUnitId},function (resp) {
        var qty=parseFloat(row_index.find('.quantity').val()==""?"0":row_index.find('.quantity').val());
        var rate=parseFloat(row_index.find('.rate').val()==""?"0":row_index.find('.rate').val());
        var hidRate=parseFloat(row_index.find('.hidRate').val()==""?"0":row_index.find('.hidRate').val())
        if(id.name=="unit[]"){
            if(resp)
            {
                var info=$.parseJSON(resp);
                var conversionRate=info[0]['conversionRate'];
                var newRate=(hidRate/conversionRate).toFixed(2);
                rate=newRate;
                row_index.find('.rate').val(rate);
                row_index.find('.hidUnitConversionId').val(info[0]['unitConversionId']);
            }
            else if(mainUnit==currentUnitId){
                row_index.find('.rate').val(hidRate);
                rate=hidRate;
                row_index.find('.hidUnitConversionId').val(0);
            }
        }
        var taxable=parseFloat(row_index.find('.taxableamounttbl').val()==""?"0":row_index.find('.taxableamounttbl').val());
        var cgst=parseFloat(row_index.find('.cgst').val()==""?"0":row_index.find('.cgst').val());
        var sgst=parseFloat(row_index.find('.sgst').val()==""?"0":row_index.find('.sgst').val());
        var gstdue=parseFloat(row_index.find('.gstdue').val()==""?"0":row_index.find('.gstdue').val());
        var resgstdue=0;
        var res=rate*qty;
        row_index.find('.taxableamounttbl ').html(res.toFixed(2));
        if(id.name==="gstdue[]"){
            var gstPercent=((gstdue*100)/res).toFixed(2);

            row_index.find('.cgst').val((gstPercent/2).toFixed(2));
            row_index.find('.sgst').val((gstPercent/2).toFixed(2));
            resgstdue=gstdue;
        }
        else{
            resgstdue= (res*(cgst+sgst)/100).toFixed(2);
            row_index.find('.gstdue ').val(resgstdue);
        }
        row_index.find('.totalamounttbl').html((parseFloat(res)+parseFloat(resgstdue)).toFixed(2));
        calcTotalAmount();
    });
}

function callCallculateAmountRow(id){
    var tab_index= $(id).closest('tr');
    calculateAmountsByRowIndex(tab_index,id);
}

var tAmount;
var tGst;
var tTaxable;
function calcTotalAmount(){
   tAmount=0;
   tGst=0;
   tTaxable=0;
   var tCgst=0,tSgst=0;
    $('.gstdue').each(function(){
        tGst=tGst+parseFloat($(this).val()==""?"0":$(this).val());
        tCgst=parseFloat(tGst/2).toFixed(2);
        tSgst=parseFloat(tGst/2).toFixed(2);

        $('#gstAmount').val(tGst.toFixed(2));
        $('#cgstAmount').val(tCgst);
        $('#sgstAmount').val(tSgst);
    });
    $('.taxableamounttbl').each(function(){
       tTaxable=tTaxable+parseFloat($(this).html()==""?"0":$(this).html());
        $('#nettaxable').val(tTaxable.toFixed(2)); 
    });
    $('.totalamounttbl').each(function(){
       tAmount=tAmount+parseFloat($(this).html()==""?"0":$(this).html());
       var finalAmt=tAmount.toFixed(0);
        $('#totalAmount').val(finalAmt);
        $('input[name="roundedFinalAmt"]').val((tAmount-finalAmt).toFixed(2));
    });

}


function getproductdetails(tab_index,procode,select2class,id){
// tab_index.children('td:nth-child(7)').find('input').val(procode);

    $.ajax({
data:{procode:procode},
url:'<?=site_url('transactions/PurchaseInvoice/getProductById');?>',
method:'post',
datatype:'json',
success:function(response){
    jsonobj=$.parseJSON(response);

   // hsnOrSacCode
    tab_index.find('.hsnsac').val(jsonobj[0]['hsnOrSacCode']);

   //cgst&sgst
    tab_index.find('.cgst').val(jsonobj[0]['taxPercent']/2);
    tab_index.find('.sgst').val(jsonobj[0]['taxPercent']/2);
    
    tab_index.find('.rate').val(jsonobj[0]['purchaseRate']);
    tab_index.find('.quantity').val("1");

    //unit
    tab_index.find('.unit ').append("<option value="+jsonobj[0]['unitId']+">"+jsonobj[0]['productUnitName']+"</option>");

    tab_index.find('.hidUnitId').val(jsonobj[0]['unitId']);
    tab_index.find('.hidRate').val(jsonobj[0]['retailRate']);

    var conversionUnitIds=jsonobj[0]['conversionUnitIds'];
    var conversionUnitName=jsonobj[0]['conversionUnitName'];
    if(conversionUnitIds!=null){
    arrUnitIds=conversionUnitIds.split(',');
    arrUnitNames=conversionUnitName.split(',');
    for (var i = 0; i < arrUnitIds.length; i++) {
    tab_index.find('.unit ').append("<option value="+arrUnitIds[i]+">"+arrUnitNames[i]+"</option>");   
    }
    }
    if(select2class=="proname"){
            tab_index.find('.procode').val(procode); 
            tab_index.find('td:nth-child(3) .select2-selection__rendered').attr('title',jsonobj[0]['productCode']);
            tab_index.find('td:nth-child(3) .select2-selection__rendered').html(jsonobj[0]['productCode']);

    }
    else if(select2class=="procode"){
            tab_index.find('.proname').val(procode);
            tab_index.find('td:nth-child(4) .select2-selection__rendered').attr('title',jsonobj[0]['productName']);
            tab_index.find('td:nth-child(4) .select2-selection__rendered').html(jsonobj[0]['productName']);       
            
    }

     calculateAmountsByRowIndex(tab_index,id);
     calcTotalAmount();

},
});
}

function purchaseInvoiceDelete(invoiceId) {
    if(invoiceId>0){
        swal({
            title: 'Are you sure?',
            text: 'This invoice will be deleted!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'rgb(221, 51, 51)',
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonClass: 'swal2-cancel swal2-styled'
        }).then((result)=>{
            if(result.value){
                $.ajax({
                    url: '<?php echo site_url('transactions/PurchaseInvoice/purchaseInvoiceDeleteById') ?>',
                    datatype: 'json',
                    data: {
                        invoiceID: invoiceId
                    },
                    method: 'post',
                    success: function (resp) {
                        if(resp=="success")
                        {
                            swal({
                                title: 'Sales Invoice',
                                text: 'Deleted successfully',
                                type: 'success',
                                showConfirmButton: false
                            });
                        }
                        else if(resp=="reference"){
                            swal({
                                title: 'Sales Invoice',
                                text: 'Cannot delete record. Reference exists',
                                type: 'warning',
                                showConfirmButton: false
                            });
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 800);
                    }
                });
            }
        });
    }
}

function puchaseInvoiceEdit(invoiceID) {
    if (invoiceID>0) {
        $.ajax({
            url: '<?php echo site_url('transactions/PurchaseInvoice/purchaseInvoiceViewById') ?>',
            datatype: 'json',
            data: {
                invoiceID: invoiceID
            },
            method: 'post',
            success: function (resp) {
                $('#test').html(resp);
                $('#myTab a:first').tab('show');
                var info = $.parseJSON(resp);
                //masterId
                $('#hidden_purchaseInvoiceID').val(info['purchaseMasterId']);

                <?php if($_SESSION['isWithoutTax']=="false") {?>
                $('#prefix').val(+info['voucherNo']);
                <?php }else { ?>
                $('#prefix').val(info['prefix']+"/"+info['voucherNo']);
                <?php } ?>
                $('#datePicker').val((info['entryDate'].slice(0,10)));
                $('#cashorbill').val(info['ledgerHead']).trigger('change');
                $('#purchaseac').val(info['purchaseHead']).trigger('change');
                $('#gstin').val(info['gstIn']);
                $('#description').html(info['description']);
                $('#nettaxable').val(info['amount']);
                $('#gstAmount').val(info['taxAmount']);

                $('#cgstAmount').val((parseFloat(info['taxAmount'])/2).toFixed(2));
                $('#sgstAmount').val((parseFloat(info['taxAmount'])/2).toFixed(2));

                $('#totalAmount').val(info['totalAmount']);
                $('#roundedFinalAmt').val(info['discount']);
                
                //$('#btnSave').html('Update')
                $('#btnSave').val('Update');;
                $('#dltbtn').removeAttr("disabled");

                $('#tablebody').find('tr').remove();
                $('#tablebody').find('tr').remove();
                $('#vendorType').val(info['vendorType']);

                var details=info['details'];
                var index=0;
                $.each(details,function () {
                    addRow();
                    var tab_index= $('#tablebody').find('tr:eq('+index+')');
                    tab_index.find('.hiddenDetailsId').val(this.purchaseDetailsId);
                    tab_index.find('.procode').val(this.productId).trigger('change');
                    tab_index.find('.proname').val(this.productId).trigger('change');
                    tab_index.find('.hsnsac').val(this.hsnOrSacCode);
                    tab_index.find('.quantity').val(this.qty);
                    tab_index.find('.rate').val(this.rate);
                    tab_index.find('.hidRate').val(this.retailRate);
                    tab_index.find('.taxableamounttbl').html(this.netAmount);
                    tab_index.find('.cgst').val(parseFloat(this.taxPercent)/2);
                    tab_index.find('.sgst').val(parseFloat(this.taxPercent)/2);
                    tab_index.find('.gstdue').val(this.taxAmount);
                    tab_index.find('.totalamounttbl').html(this.totalAmount);
                    tab_index.find('.hidUnitId').val(this.unitId);
                    tab_index.find('.hidUnitConversionId').val(this.unitConversionId==null?0:this.unitConversionId);

                    $.post('<?=site_url('transactions/PurchaseInvoice/getProductById');?>',{procode:this.productId},function (resp) {
                        jsonobj = $.parseJSON(resp);
                        if (jsonobj[0]['conversionUnitIds'] != null) {
                            var ids = jsonobj[0]['conversionUnitIds'];
                            var names = jsonobj[0]['conversionUnitName'];
                            var convIds=jsonobj[0]['unitConversionIds'];
                            var arrUnitId = ids.split(',');
                            var arrUnitName = names.split(',');
                            var arrLen = arrUnitId.length;
                            var arrConvIds=convIds.split(',');
                            for (var i = 0; i < arrLen; i++) {
                                var unitConversionId=tab_index.find('.hidUnitConversionId').val();
                                if(arrConvIds[i]==unitConversionId){
                                    tab_index.find('.unit').append('<option value="' + arrUnitId[i] + '">' + arrUnitName[i] + '</option>');
                                }
                            }
                            tab_index.find('.unit').append('<option value="' + jsonobj[0]['unitId'] + '">' + jsonobj[0]['productUnitName'] + '</option>');
                            for (var i = 0; i < arrLen; i++) {
                                if(arrConvIds[i]!=unitConversionId){
                                    tab_index.find('.unit').append('<option value="' + arrUnitId[i] + '">' + arrUnitName[i] + '</option>');
                                }
                            }
                        }
                        else
                            tab_index.find('.unit').append('<option value="' + jsonobj[0]['unitId'] + '">' + jsonobj[0]['productUnitName'] + '</option>');
                        // tab_index.find('.unit').trigger('change');
                    });

                    index++;
                });
            }
        });
    }
}
function saveOrUpdate(){
  
var tAmount=$('.totalamounttbl')[0];
    if((!$('#prefix').attr('disabled')) && $('#prefix').val()==""){
        toastr["warning"]("Enter voucher no");
        $('#prefix').focus();
    }
    else if($('#cashorbill option:selected').val()==0){
        toastr["warning"]("Choose cash or party");
        $('#cashorbill').focus();
    }
    else if($('#purchaseac option:selected').text()==""){
        toastr["warning"]("Choose purchase account");
        $('#purchaseac').focus();
    }
    else if(tAmount.innerHTML==""||tAmount.innerHTML==0){
        toastr["warning"]("Enter atleast one product details");
    }
    else{
        var invoiceID=$('#hidden_purchaseInvoiceID').val();
        var arrPurchaseDetailsId=JSON.stringify($("input[name='hiddenDetailsId[]']")
            .map(function(){return $(this).val();}).get());
        var arrProductId = JSON.stringify($("select[name='product[]']")
              .map(function(){return $(this).val();}).get());
        var arrHsnCodes=JSON.stringify($("input[name='hsnsac[]']")
              .map(function(){return $(this).val();}).get());
        var arrQty=JSON.stringify($("input[name='quantity[]']")
              .map(function(){return $(this).val();}).get());
        var arrUnit=JSON.stringify($("input[name='hidUnitId[]']")
              .map(function(){return $(this).val();}).get());
        var arrRate=JSON.stringify($("input[name='rate[]']")
              .map(function(){return $(this).val();}).get());
        var arrTaxable=JSON.stringify($("span[name='taxableamounttbl[]']")
              .map(function(){return $(this).html();}).get());
        var arrCgst=JSON.stringify($("input[name='cgst[]']")
              .map(function(){return $(this).val();}).get());
        var arrSgst=JSON.stringify($("input[name='sgst[]']")
              .map(function(){return $(this).val();}).get());
        var arrGstDue=JSON.stringify($("input[name='gstdue[]']")
              .map(function(){return $(this).val();}).get());
        var arrTotalAmount=JSON.stringify($("span[name='totalamounttbl[]']")
              .map(function(){return $(this).html();}).get());
        var arrUnitConversionId=JSON.stringify($("input[name='hidUnitConversionId[]']")
            .map(function(){return $(this).val();}).get());
        var select= $('#cashorbill').val();
        var isCashOrPartyIsOld =$.isNumeric(select);
$.ajax({
    url:'<?=site_url('transactions/PurchaseInvoice/addNewPurchaseInvoiceForm');?>',
    method:'POST',
    datatype:'json',
    data:{ 
            invoiceID:invoiceID,
            voucherNo:$('#prefix').val(),
            invoiceNumber:$('#invoiceno').val(),
            gstIn:$('#gstin').val(),
            date:$('#datePicker').val(),
            cashorbill:$('#cashorbill').val(),
            purchaseac:$('#purchaseac').val(),
            taxableAmount:$('#nettaxable').val(),
            gstAmount:$('#gstAmount').val(),
            discount:$('#roundedFinalAmt').val(),
            totalAmount:$('#totalAmount').val(),
            description:$('#desc').val(),
            arrPurchaseDetailsId:arrPurchaseDetailsId,
            arrProductId:arrProductId,
            arrHsnCodes:arrHsnCodes,
            arrQty:arrQty,
            arrUnit:arrUnit,
            arrUnitConversionId:arrUnitConversionId,
            arrRate:arrRate,
            arrTaxable:arrTaxable,
            arrCgst:arrCgst,
            arrSgst:arrSgst,
            arrGstDue:arrGstDue,
            arrTotalAmount:arrTotalAmount,
            arrRemoveIds:JSON.stringify(arrRemoveIds),
            isCashOrPartyIsOld:isCashOrPartyIsOld==true?true:$('#cashOrParty option:selected').text(),
            printMethod:$('#printmethod').val(),
            vendorType:$('#vendorType').val()
                                                        },
            success: function (resp) {
                var obj= $.parseJSON(resp);
                    var masterId=obj['id'];
                swal({
                    title: 'Purchase Invoice',
                    text: obj['msg'],
                    type: 'success',
                    showConfirmButton: false
                });
                if($('#printmethod').val()!=2){
                    window.open('<?=site_url()?>/printbill/purchase/'+masterId,"_blank");
                    }
                setTimeout(function () {
                    location.reload();
                }, 800);
            }
        });
    }
}

$('body').ready(function () {
    <?php if($_SESSION['printMethod']=="1"){ ?>
        $('#printmethod').val(1).trigger('change');
    <?php } ?>
    <?php if($_SESSION['printMethod']=="2"){ ?>
        $('#printmethod').val(2).trigger('change');
    <?php } ?>

    $('#prefix').focus();

    generalSettings();

    //let isWithoutTax="<?//=$_SESSION['isWithoutTax']?>//";
    //if(isWithoutTax==="true"){
    //    $('#vendorType').css('display','none');
    //}

});

$('#cashorbill').change(function () {
    $.post('<?= site_url('masters/contactViewByLedgerId'); ?>',{ledgerId:this.value},function (resp) {
        if(resp!="null")
        {
            var info= $.parseJSON(resp);
            $('#gstin').val(info['gstIn']);
        }
    });
});

function hideCols() {
    let isWithoutTax="<?=$_SESSION['isWithoutTax']?>";
    if(isWithoutTax==="true"){
        $('#table_purinvoice tr').each(function () {
            $(this).find('.hidable').each(function () {
                $(this).css('display','none');
            });
        });
        $('.hidableTax').css('display','none');

    }
}

$('#vendorType').change(function () {
    let vendorType= this.value;
    if(vendorType==="Customer" && $('#hidden_purchaseInvoiceID').val()==""){
        $.post('<?= site_url('transactions/PurchaseInvoice/GetPurchaseVoucherMax') ?>',function (resp) {
            $('#prefix').val(resp);
        });
    }
    else if(vendorType==="Vendor" && $('#hidden_purchaseInvoiceID').val()=="")
        $('#prefix').val("");
});

    function generalSettings() {
        $('select').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('select').focusout(function () {
            $(this).css('border','');
        });
        $('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.select2-selection').focusout(function () {
            $(this).css('border','');
        });
        $('#tablebody input:text').focus(function () {
            $(this).closest('td').css('border','2px solid #528bb3');
        });
        $('#tablebody input:text').focusout(function () {
            $(this).closest('td').css('border','');
        });
        $('#tablebody').find('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('#tablebody').find('.select2-selection').focusout(function () {
            $(this).css('border','none');
        });
        $('#tablebody button').focus(function () {
            $(this).closest('th').css('border','2px solid #528bb3');
        });
        $('#tablebody button').focusout(function () {
            $(this).closest('th').css('border','');
        });
        $('.focusable').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.focusable').focusout(function () {
            $(this).css('border','');
        });
    }


</script>



</body>
</html>