<?php $this->load->view('Header/header.php');
?>
<section class="container-fluid col-md-8">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Payment Voucher</h4>

            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab" id="navNew">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab" id="navList">List</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" name="list" role="tabpanel">
                        <br>
                        <div class="table-responsive table-hover" id="tblData" name="tblData">
                            <table id="data-table" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Voucher No</th>
                                    <th>Voucher Date</th>
                                    <th>Bank/Cash</th>
                                    <th>Ledger</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach ($paymentGridData as $key) { ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $key->VoucherNo; ?></td>
                                        <td><?= $key->voucherDate; ?></td>
                                        <td><?= $key->bankOrCash; ?></td>
                                        <td><?= $key->account; ?></td>
                                        <td><?= $key->total; ?></td>
                                        <td><i class="mdi mdi-pencil" style="cursor:pointer;"
                                               onclick="paymentVoucherEdit(<?= $key->paymentID; ?>)"> </i>/<i
                                                    class="mdi mdi-delete" style="cursor:pointer;"
                                                    onclick="paymentVoucherDelete(<?= $key->paymentID; ?>)"> </i></td>
                                    </tr>
                                    <?php $i++;
                                } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        <div class="card-body">
                            <input type="hidden" id="hidPaymentID" value="0" name="hidPaymentID">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Voucher No</label>
                                    <div class="form-group">
                                        <input type="text" id="voucherNo" name="voucherNo" class="form-control focusable"
                                               value="###" readonly="readonly">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-4">
                                    <label class="control-label">Voucher Date</label>
                                    <input type="date" class="form-control focusable" id="voucherDate" name="voucherDate"
                                           value="<?= date('Y-m-d') ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Cash/Bank</label>
                                    <div class="form-group">
                                        <select id="cashOrBank" name="cashOrBank" class="select2" style="width:100%;">
                                            <option value="0">--Select--</option>
                                            <?php
                                            $i = 1;
                                            foreach ($bankOrCash as $key) {
                                                ?>
                                                <option value="<?= $key->ledgerId ?>"><?= $key->ledgerName ?></option>
                                                <?php $i++;
                                            } ?>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <label class="control-label">Account</label>
                                    <div class="form-group">
                                        <select id="account" name="account" class="select2" style="width:100%;">
                                            <option value="0">--Select--</option>
                                            <?php
                                            $i = 1;
                                            foreach ($ledger as $key) {
                                                ?>
                                                <option value="<?= $key->ledgerId ?>"><?= $key->ledgerName ?></option>
                                                <?php $i++;
                                            } ?>
                                        </select>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">Total Amount</label>
                                    <div class="form-group">
                                        <input type="number" id="totalAmount" name="totalAmount" class="form-control focusable"
                                               placeholder="0.00">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="description" id="description" rows="3" class="form-control focusable"
                                                  placeholder="Description">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-right">
                                    <div class="btn-demo form-group">
                                        <button class="btn btn-primary pull-right" id="btnSave">Save</button>
                                        <button class="btn btn-light pull-right" id="btnClear">Clear</button>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->
<script type="text/javascript">
    function formattedDate(d) {
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return `${year}-${month}-${day}`;
    }

    $(document).ready(function () {
        $('.mydatepicker').datepicker();
        $('#data-table').DataTable();
        $('#description').val('');
        $('#voucherDate').val(formattedDate(new Date()));
        // $('#voucherDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
    });

    function switchTabNew() {
        $('#navList').removeClass('active');
        $('#navList').attr('aria-expanded', 'false')
        $('#navNew').addClass('nav-link active');
        $('#navNew').attr('aria-expanded', 'true')
        $('#list').attr('aria-expanded', 'false')
        $('#addnew').attr('aria-expanded', 'true')
        $('#list').removeClass('active show');
        $('#addnew').addClass('active show');
    }

    $('#btnSave').click(function () {
        if ($('#voucherDate').val() == "") {
            $('#voucherDate').focus();
        }
        else if ($('#cashOrBank option:selected').val() == 0) {
            toastr["warning"]("Select payment method.");
            $('#cashOrBank').focus();
        }
        else if ($('#account option:selected').val() == 0) {
            toastr["warning"]("Select payment received account.");
            $('#account').focus();
        }
        else if ($('#totalAmount').val() == 0) {
            toastr["warning"]("Enter amount to be paid.");
            $('#totalAmount').focus();
        }
        else {
            var paymentID = $('#hidPaymentID').val();
            $.ajax({
                url: '<?php echo site_url('transactions/PaymentVoucher')?>',
                datatype: 'json',
                data: {
                    paymentID: paymentID,
                    voucherDate: $('#voucherDate').val(),
                    cashOrBank: $('#cashOrBank option:selected').val(),
                    account: $('#account option:selected').val(),
                    totalAmount: $('#totalAmount').val(),
                    description: $('#description').val()
                },
                method: 'post',
                success: function (resp) {
                    swal({
                        title: 'Payment Voucher',
                        text: resp,
                        type: 'success',
                        showConfirmButton: false
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 800);
                }
            });
        }
    });

    function paymentVoucherEdit(paymentID) {
        if (paymentID > 0) {
            $.ajax({
                url: '<?php echo site_url('transactions/paymentVoucherViewById') ?>',
                datatype: 'json',
                data: {
                    paymentID: paymentID
                },
                method: 'post',
                success: function (resp) {
                    $('#myTab a:first').tab('show');
                    var payment = $.parseJSON(resp);
                    $('#voucherNo').val(payment['prefix'] + "/" + payment['VoucherNo']);
                    $('#voucherDate').val(payment['voucherDate']);
                    $('#cashOrBank').val(payment['ledgerFirst']).trigger('change');
                    $('#account').val(payment['ledgerSecond']).trigger('change');
                    $('#totalAmount').val(payment['total']);
                    $('#description').val(payment['description']);
                    $('#hidPaymentID').val(payment['paymentID']);
                    $('#btnSave').html('Update');
                }
            });
        }
    }

    function paymentVoucherDelete(paymentID) {
        if (paymentID > 0) {
            swal({
                title: 'Are you sure?',
                text: 'This voucher will be deleted!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'rgb(221, 51, 51)',
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonClass: 'swal2-cancel swal2-styled'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '<?php echo site_url('transactions/paymentVoucherDeleteById') ?>',
                        datatype: 'json',
                        data: {
                            paymentID: paymentID
                        },
                        method: 'post',
                        success: function (resp) {
                            if (resp == "success") {
                                swal({
                                    title: 'Payment Voucher',
                                    text: 'Deleted successfully',
                                    type: 'success',
                                    showConfirmButton: false
                                });
                            }
                            else if (resp == "reference") {
                                swal({
                                    title: 'Payment Voucher',
                                    text: 'Cannot delete record. Reference exists',
                                    type: 'warning',
                                    showConfirmButton: false
                                });
                            }
                            setTimeout(function () {
                                location.reload();
                            }, 800);
                        }
                    });
                }
            });
        }
    }

    $('#btnClear').click(function () {
        $('#voucherNo').val('####');
        // flatpickr("#voucherDate", {
        //     dateFormat: 'Y-m-d',
        //     defaultDate: formattedDate(new Date())
        // });
        $('#cashOrBank').val(0).trigger('change');
        $('#account').val(0).trigger('change');
        $('#totalAmount').val('');
        $('#description').val('');
        $('#hidPaymentID').val('0');
        $('#btnSave').html('Save');
    });
    $('body').ready(function () {
        $('#voucherDate').focus();
        generalSettings();
    });
    function generalSettings() {
        $('select').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('select').focusout(function () {
            $(this).css('border','');
        });
        $('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.select2-selection').focusout(function () {
            $(this).css('border','');
        });
        $('#tablebody input:text').focus(function () {
            $(this).closest('td').css('border','2px solid #528bb3');
        });
        $('#tablebody input:text').focusout(function () {
            $(this).closest('td').css('border','');
        });
        $('#tablebody').find('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('#tablebody').find('.select2-selection').focusout(function () {
            $(this).css('border','none');
        });
        $('#tablebody button').focus(function () {
            $(this).closest('th').css('border','2px solid #528bb3');
        });
        $('#tablebody button').focusout(function () {
            $(this).closest('th').css('border','');
        });
        $('.focusable').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.focusable').focusout(function () {
            $(this).css('border','');
        });
    }

</script>

</body>
</html>