<?php $this->load->view('Header/header.php'); ?>
<div class="container-fluid col-md-8">
     <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Receipt Voucher </h4>
            
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                     <li class="nav-item" id="addli">
                        <a class="nav-link active" data-toggle="tab" href="#addnew" role="tab">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#list" role="tab">List</a>
                    </li>
                   
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade " id="list" role="tabpanel">
                                       
                        <br>
						<div class="table-responsive table-hover" id="tblData" name="tblData">
							<table id="data-table" class="table">
								<thead >
									<tr>
										<th>#</th>
										<th>Voucher No</th>
                                        <th>Bank/Cash</th>
										<th>Account</th>
										<th>Amount</th>
										<th>Edit/Delete</th>
									</tr>
								</thead>
								<tbody >
									<?php
									$i=1;
									foreach ($receiptvoucher as $keyvoucher) { ?>
									<tr>
										<td scope="row"><?= $i; ?></td>
										<td><?= $keyvoucher->prefix."/".$keyvoucher->VoucherNo; ?></td>
										<td><?= $keyvoucher->ledgerNameFirst ?></td>
                                        <td><?= $keyvoucher->ledgerNameSecond ?></td>
										<td ><?= $keyvoucher->total; ?></td>
										<td><i class="mdi mdi-pencil" onclick="editVoucher(<?= $keyvoucher->receiptID; ?>)"> </i>/<i class="mdi mdi-delete" onclick="delVoucher(<?= $keyvoucher->receiptID; ?>)">  </i></td>
									</tr>
									<?php $i++; } ?>
									
									
								</tbody>
							</table>
						</div>
                    </div>
               
                   
                    <div class="tab-pane active fade show" id="addnew" role="tabpanel">
                        
                        <div class="card-body">
                           
                           <form method="post" action="<?= site_url('transactions/addNewReceiptVoucher'); ?>" >
                               <input type="hidden" name="hiddenreceiptvoucherid" id="hiddenreceiptvoucherid" value="0">
                        <div class="row createNewVoucher">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Prefix - Voucher</label>
                                    <input disabled type="text" class="form-control focusable" name="prefix" id="prefix" value="RV/##">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="form-group">
                                        <input type="date" id="datepicker" name="datepicker" class="form-control focusable" placeholder="Pick a date">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Bank/Cash</label>
                                    <select required="required" class="form-control" name="bankorcash" id="bankorcash">
                                        <option value="0">--Select--</option>
                                        <?php foreach ($bankorcash as $key ) { ?>
                                        <option value="<?= $key->ledgerId; ?>"><?= $key->ledgerName; ?></option>
                                    <?php } ?>                                            </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Account</label>
                                    
                                    <select required="required" class="select2" style="width:100%;" name="account" id="account">
                                        <option value="0">--Select--</option>
                                        <?php foreach ($ledger as $keyaccount ) { ?>
                                        <option value="<?= $keyaccount->ledgerId; ?>"><?=  $keyaccount->ledgerName; ?> </option>
                                        <?php  } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" id="desc" name="desc" class="form-control focusable" >
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input required="required" type="number" name="amount" id="amount" class="form-control focusable" >
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="button" name="submit" id="submitnew"  class="btn btn-primary">Save</button>
                                    <button type="button" name="clear" id="clear"  class="btn btn-light">Clear </button>
                                </div>
                            </div>
                            
                        </div></form>
                           
                            
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    <?php $this->load->view('Header/footer.php'); ?>
    <!-- javascript code -->
<script type="text/javascript">
  $('#clear').click(function(){
    $('#datepicker').attr('hidden','hidden');
    $('#datenow').removeAttr('hidden');
    $('#submitnew').html('save');
    $('#submitnew').prop('class','btn btn-primary');
    $('.createNewVoucher #desc').val("");
    $('.createNewVoucher #amount').val("");
    $('.createNewVoucher #bankorcash').val(0);
    $('.createNewVoucher #account').val(0).trigger('change');
  });

  function formattedDate(d) {
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${year}-${month}-${day}`;
  }
 $(document).ready(function(){
     $('#data-table').DataTable();
     $('.mydatepicker').datepicker();
     $('#datepicker').val(formattedDate(new Date()));
 });
 
  $('#submitnew').click(function(){
    $('#submitnew').attr('type','submit');
    $('#submitnew').click();
  });
  
  function delVoucher(id){
    swal({
      title: 'Are you sure?',
      text: 'This Receipt Voucher will be deleted!',
      type: 'warning',
      showCancelButton: true,
		confirmButtonColor: 'rgb(221, 51, 51)',
		confirmButtonClass: 'btn-danger',
		confirmButtonText: 'Yes, delete it!',
		cancelButtonClass: 'swal2-cancel swal2-styled'
    }).then((result)=>{
		if(result.value){
			cnfrmDel();
			  swal({
				timer: 800,
				title: 'Deleted!',
				text: '',
				type: 'success',
				showConfirmButton: false
			  });
			  setTimeout(function() {
				location.reload();
			  }, 800);
		}
	});
    function cnfrmDel(){
      $.ajax({
        data:{
          id:id}
        ,
        url:'<?= site_url('transactions/delReceiptVoucher'); ?>',
        method:'post',
        datatype:'json',
        success:function(response){
			
		}
	  });
    }
  }
  function editVoucher(id){
    //fetch with id
    $.ajax({
      url:'<?= site_url('transactions/fetchVoucherWithId'); ?>',
      datatype:'json',
      method:'post',
      data:{id:id},
      success:function(response){
      var obj= $.parseJSON(response);
      $('#myTab a:first').tab('show');
      $('#submitnew').html('update');
      $('#submitnew').prop('class','btn btn-info');
      $('#hiddenreceiptvoucherid').val(obj[0]['receiptID']);
      $('#prefix').val(obj[0]['prefix']+'/'+obj[0]['VoucherNo']);
      $('#bankorcash').val(obj[0]['ledgerFirst']);
      $('#account').val(obj[0]['ledgerSecond']).trigger('change');
      $('#desc').val(obj[0]['description']);
      $('#amount').val(obj[0]['total']);
      var momentDate = moment(obj[0]['voucherDate']+" 00:00:00", 'DD-MM-YYYY HH:mm:ss');
      var voucherDate=momentDate.toDate();
      $('#datepicker').val(obj[0]['voucherDate'].slice(0,10));
    }
  }
  );
  }

  $('body').ready(function () {
      $('#datepicker').focus();
      generalSettings();
  });
  function generalSettings() {
      $('select').focus(function () {
          $(this).css('border','2px solid #528bb3');
      });
      $('select').focusout(function () {
          $(this).css('border','');
      });
      $('.select2-selection').focus(function () {
          $(this).css('border','2px solid #528bb3');
      });
      $('.select2-selection').focusout(function () {
          $(this).css('border','');
      });
      $('#tablebody input:text').focus(function () {
          $(this).closest('td').css('border','2px solid #528bb3');
      });
      $('#tablebody input:text').focusout(function () {
          $(this).closest('td').css('border','');
      });
      $('#tablebody').find('.select2-selection').focus(function () {
          $(this).css('border','2px solid #528bb3');
      });
      $('#tablebody').find('.select2-selection').focusout(function () {
          $(this).css('border','none');
      });
      $('#tablebody button').focus(function () {
          $(this).closest('th').css('border','2px solid #528bb3');
      });
      $('#tablebody button').focusout(function () {
          $(this).closest('th').css('border','');
      });
      $('.focusable').focus(function () {
          $(this).css('border','2px solid #528bb3');
      });
      $('.focusable').focusout(function () {
          $(this).css('border','');
      });
  }


</script>

    <!-- javascript code -->
</body>
</html>