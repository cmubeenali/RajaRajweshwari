<?php $this->load->view('Header/header.php'); ?>
<section class="container-fluid col-md-12">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Sales Invoice</h4>

            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#addnew" role="tab" id="navNew">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#list" role="tab" id="navList">List</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="list" name="list" role="tabpanel">
                        <br>
                        <div class="table-responsive table-hover" id="tblData" name="tblData">
                            <table id="data-table" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Voucher No</th>
                                    <th>Voucher Date</th>
                                    <th>Ledger</th>
                                    <th>Taxable</th>
                                    <th>GST Amt.</th>
                                    <th>Total Amt.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach ($tableData as $key) { ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $key->voucherNo; ?></td>
                                        <td><?= $key->voucherDate; ?></td>
                                        <td><?= $key->ledgerName; ?></td>
                                        <td><?= $key->amount; ?></td>
                                        <td><?= $key->taxAmount; ?></td>
                                        <td><?= $key->totalAmount; ?></td>
                                        <td><i class="mdi mdi-pencil" style="cursor:pointer;"
                                               onclick="salesInvoiceEdit(<?= $key->salesMasterId; ?>)"> </i>/<i
                                                    class="mdi mdi-delete" style="cursor:pointer;"
                                                    onclick="salesInvoiceDelete(<?= $key->salesMasterId; ?>)"> </i></td>
                                    </tr>
                                    <?php $i++;
                                } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane active fade show" id="addnew" role="tabpanel">
                        <input type="hidden" id="hidInvoiceID" value="0" name="hidInvoiceID">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" id="voucherNo" disabled class="form-control focusable" value="INV/##"/>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="input-group">
                                            <select id="cashOrParty" name="cashOrParty" class="select2"
                                                    style="width:88%;">
                                                <option value="0">--Select Cash/Party--</option>
                                                <?php
                                                foreach ($cashOrParty as $key) {
                                                    ?>
                                                    <option value="<?= $key->ledgerId; ?>"><?= $key->ledgerName; ?></option>
                                                <?php } ?>
                                            </select>
                                            <!--                                            <input type="button" value="+" class="btn btn-default" id="btnAddNewContact">-->
                                            <div class="input-group-append" id="addNewContact" style="cursor:pointer;">
                                                <span class="input-group-text"
                                                      style="font-size: 16px; font-weight: bold">+</span>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <select id="salesAccount" name="salesAccount" style="width:100%;">
                                            <?php
                                            foreach ($salesAccount as $key) {
                                                ?>
                                                <option value="<?= $key->ledgerId; ?>"><?= $key->ledgerName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">&nbsp;</div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="date" id="voucherDate" name="voucherDate" class="form-control focusable"
                                               placeholder="Voucher Date">
                                        <br>
                                        <select id="salesOrder" name="salesOrder" style="width: 100%;">
                                            <option value="0">--Select Order--</option>
                                            <?php
                                            foreach ($salesOrder as $key) {
                                                ?>
                                                <option value="<?= $key->salesOrderMasterId; ?>"><?= $key->voucherNo; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="clearfix">&nbsp;</div>
                                        <input  type="text" class="form-control focusable" name="gstin" id="gstin" placeholder="GSTIN">
                                        <div class="clearfix">&nbsp;</div>
                                        <input type="text" id="partyRef" name="partyRef" class="form-control focusable"
                                               placeholder="Party Reference">
                                    </div>
                                </div>
                            </div>
                            <div class="row eklavyapurchaseinvoicewhitediv">
                                <div class="table-responsive col-md-12">
                                    <table class="table table-bordered  mb-0" id="tblSalesInvoice">
                                        <thead class="thead-inverse">
                                        <tr>
                                            <th width="1%" style="text-align: center;"><i
                                                        class="mdi mdi-table-row-plus-after" style="font-size: 18px;"
                                                        onclick="addRow()"></i></th>
                                            <th width="1%" style="text-align: center;">#</th>
                                            <th width="5%" style="text-align: center;">Purity</th>
                                            <th width="8%" style="text-align: center;">Product</th>
                                            <th width="2%" style="text-align: center;">HSN</th>
                                            <th width="2%" style="text-align: center;">Qty</th>
                                            <th width="5%" style="text-align: center;">Unit</th>
                                            <th width="4%" style="text-align: center;">Rate</th>
                                            <th width="3%" style="text-align: center;">VA(%)</th>
                                            <th width="3%" style="text-align: center;">Val.(Supply)</th>
                                            <th width="3%" style="text-align: center;">Disc.</th>
                                            <th width="5%" style="text-align: center;">Taxable</th>
                                            <th width="3%" style="text-align: center;">CGST(%)</th>
                                            <th width="3%" style="text-align: center;">SGST(%)</th>
                                            <th width="5%" style="text-align: center;">GST Due</th>
                                            <th width="5%" style="text-align: center;">Total Amt.</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tablebody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-3">Description<textarea name="description" id="description"
                                                                           class="form-control focusable" rows="4"></textarea>
                                </div>
                                <div class="col-md-5">&nbsp;</div>
                                <div class="col-md-4">
                                    <table border="0" class="col-md-12">
                                        <tr>
                                            <td>
                                                <label>Taxable Amount</label>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control focusable" id="totalTaxableAmount"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td>
                                                <label>GST Amount</label>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control focusable" id="totalGstAmount"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>CGST</label>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control focusable" id="totalCgstAmount"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>SGST</label>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control focusable" id="totalSgstAmount"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Rounded</label>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control focusable" id="roundFix"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Amount</label>
                                            </td>
                                            <td>
                                                <input type="number" id="totalAmount" class="form-control focusable"
                                                       readonly="readonly">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-9">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <!-- <label> Interstate <br> Purchase</label>
                                        <div class="toggle-switch toggle-switch--blue">
                                            <input type="checkbox" class="toggle-switch__checkbox">
                                            <i class="toggle-switch__helper"></i>
                                        </div> -->
                                        <!-- <label>Print Mode  </label> -->
                                        <select class="form-control" name="printmethod" id="printmethod" style="width: 100%">
                                            <option selected value="1">Print</option>
                                            <option value="2">Without Print</option>
                                            <!-- <option value="4">Barcode Print</option> -->
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="submit" id="btnSave" value="Save" class="btn btn-primary">
                                    <button type="button" id="btnClear" class="btn" onclick="location.reload();">Clear
                                    </button>
                                    <button style="" type="button" disabled="disabled" id="btnDelete"
                                            class="btn btn-danger"
                                            onclick="salesInvoiceDelete($('#hidInvoiceID').val())">Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Default Modal strat-->
<div class="modal fade" id="modal-default" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Add New Contact</h5>
            </div>
            <form action="masters/addNewContact" method="post" id="frmData"></form>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Contact Name</label>
                        <div class="form-group">
                            <input type="text" id="name" name="name" value="" required="required"
                                   class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Address</label>
                        <div class="form-group">
                            <input type="text" id="add" name="add" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">City</label>
                        <div class="form-group">
                            <input type="text" id="city" name="city" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">State</label>
                        <div class="form-group">
                            <input type="text" id="state" name="state" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">State Code</label>
                        <div class="form-group">
                            <input type="text" id="sc" name="sc" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Land Phone</label>
                        <div class="form-group">
                            <input type="text" id="land" name="land" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Mobile</label>
                        <div class="form-group">
                            <input type="text" id="mobile" name="mobile" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Email</label>
                        <div class="form-group">
                            <input type="text" id="email" name="email" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Dl Number</label>
                        <div class="form-group">
                            <input type="text" id="dlNumber" name="dlNumber" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Credit Period</label>
                        <div class="form-group">
                            <input type="text" id="creditPeriod" name="creditPeriod" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">GSTIN </label>
                        <div class="form-group">
                            <input type="text" id="gst" name="gst" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Description</label>
                        <div class="form-group">
                            <input type="text" id="description" name="description" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Opening Balance</label>
                        <div class="form-group">
                            <input type="text" id="op" name="op" value="" class="form-control">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-light">
                                <input type="radio" name="crordr" value="Cr" id="creditradiomodal" autocomplete="off">
                                Credit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-light">
                                <input type="radio" name="crordr" value="Dr" id="debitradiomodal" autocomplete="off">
                                Debit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                        </div>
                        <div>&nbsp;</div>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-light">
                                <input type="radio" name="type" value="Supplier" id="supplierradiomodal"
                                       autocomplete="off">Supplier
                            </label>
                            <label class="btn btn-light">
                                <input type="radio" name="type" value="Customer" id="customerradiomodal"
                                       autocomplete="off">Customer
                            </label>
                        </div>
                    </div>


                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" name="btnSaveContact" id="btnSaveContact" class="btn btn-link">Save changes
                </button>
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script type="text/javascript">
    // Bind & initialization an event select2
    $('#cashOrParty').select2({
        tags: true
    });
    $('#salesAccount').select2();
    $('#salesOrder').select2();
    // Bind an event select2 close
    var arrRemoveIds = [];

    function formattedDate(d) {
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return `${year}-${month}-${day}`;
    }

    $(document).ready(function () {
        $('#voucherDate').val(formattedDate(new Date()));
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf'
            ]
        });
    });

    var jsonobj = "";

    var addSerialNumber = function () {
        var i = 0;
        $('#tblSalesInvoice tr').each(function (index) {
            $(this).find('td:nth-child(2)').html(index++);
        });
    };
    addRow();


    function addRow() {


        $('#tablebody').append('<tr><th scope="row"><input type="hidden" value="0" class="hidSalesDetailsId" name="hidSalesDetailsId[]"><button class="btn btn-light" style="width:100%;"><i class=" mdi mdi-table-row-remove " style="color: #ff0018; font-size: 18px; " ></i></button></th><td style="text-align:center;"><input value="" style="border: none; padding: 0;" type="text" class="form-control" disabled name="slNo[]" ></td><td><select class="myslct procode" style="border: none; width:100%; padding: 0;"  name="code[]" onchange="procode(this,event)"><option></option><?php foreach ($productCode as $codepro ) { ?><option value="<?=$codepro->productId?>"><?=$codepro->productCode?></option><?php } ?></select></td><td ><select class="myslct proname" style="border: none; padding: 0; width:100%; background:none;color:white;" name="product[]" onchange="proname(this)"><option></option><?php foreach ($productName as $namepro ) { ?><option value="<?=$namepro->productId?>"><?=$namepro->productName?></option><?php } ?></select></td><td><input style="border: none; padding: 0; text-align:center;" type="text" class="form-control hsnorsaccode" name="hsnorsaccode[]"></td><td><input style="border: none; padding: 0; text-align:center;" type="text" class="form-control quantity" name="quantity[]" onkeyup="callCallculateAmountRow(this)"></td><td><select style="border: none; padding: 0; width:100%;" type="text" class="form-control unit" name="unit[]" onchange="callCallculateAmountRow(this)"></select></td><td><input type="hidden" name="hidRate[]" class="hidRate" value="0"> <input type="hidden" name="hidUnitId[]" class="hidUnitId" value="0"> <input type="hidden" name="hidUnitConversionId[]" class="hidUnitConversionId" value="0"> <input style="border: none; padding: 0; text-align:center;" type="text" class="form-control rate" name="rate[]" onkeyup="callCallculateAmountRow(this)"></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control vapercent" name="vapercent[]" onkeyup="callCallculateAmountRow(this)"></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control valueofsupply" name="valueofsupply[]" onkeyup="callCallculateAmountRow(this)"></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control discountamount" name="discountamount[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;"><span class="taxableamount" name="taxableamount[]"></span></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control cgst" name="cgst[]" onkeyup="callCallculateAmountRow(this)"></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control sgst" name="sgst[]" onkeyup="callCallculateAmountRow(this)"></td><td><input type="text" style="border: none; padding: 0; text-align:center;" class="form-control gstdue" name="gstdue[]" onkeyup="callCallculateAmountRow(this)"></td><td style="text-align:center;"><span class="totalamounttbl" name="totalamounttbl[]"></span></td></tr>');
        addSerialNumber();
        $('.procode').select2();
        $('.proname').select2();
        $('.unit').select2();
        $('#tblSalesInvoice td ').css('padding', '0px 0px');
        $('#tblSalesInvoice td ').css('vertical-align', 'middle');
        $('#tblSalesInvoice th ').css('padding', '0px 0px');
        $('#tablebody .select2-container--default .select2-selection--single').css('border', 'none');

        generalSettings();
    }


    $('#tblSalesInvoice').on('click', 'button', function (e) {

        var detailsId = $(this).closest('tr').find('.hidSalesDetailsId').val()
        if (detailsId > 0)
            arrRemoveIds.push(detailsId);
        $(this).closest('tr').remove();
        addSerialNumber();
        calculateTotalAmount();
    });

    //TABLE HANDLING FUNCTIONS
    function procode(id, e) { //procode change
        var procode = id.value;
        var tab_index = $(id).closest('tr');
        //tab_index.children('td:nth-child(7)').find('input').val(procode);
        var select2class = "procode";
        getproductdetails(tab_index, procode, select2class, id); //ajax

    }

    function proname(id, e) { // name change
        var procode = id.value;
        var tab_index = $(id).closest('tr');
        var select2class = "proname";
        getproductdetails(tab_index, procode, select2class, id); //ajax

    }

    function getproductdetails(tab_index, procode, select2class, id) {
// tab_index.children('td:nth-child(7)').find('input').val(procode);
        $.ajax({
            data: {productId: procode},
            url: '<?=site_url('transactions/SalesInvoice/productViewById');?>',
            method: 'post',
            datatype: 'json',
            success: function (response) {
                jsonobj = $.parseJSON(response);
                tab_index.children('td:nth-child(5)').find('input').val(jsonobj['hsnOrSacCode']);
                if (select2class == "proname") {
                    tab_index.find('.procode').val(procode);
                    tab_index.find('td:nth-child(3) .select2-selection__rendered').attr('title', jsonobj[0]['productCode']);
                    tab_index.find('td:nth-child(3) .select2-selection__rendered').html(jsonobj[0]['productCode']);

                }
                else if (select2class == "procode") {
                    tab_index.find('.proname').val(procode);
                    tab_index.find('td:nth-child(4) .select2-selection__rendered').attr('title', jsonobj[0]['productName']);
                    tab_index.find('td:nth-child(4) .select2-selection__rendered').html(jsonobj[0]['productName']);

                }
                tab_index.find('.hsnorsaccode').val(jsonobj[0]['hsnOrSacCode']);
                tab_index.find('.cgst').val(parseFloat(jsonobj[0]['taxPercent']) / 2);
                tab_index.find('.sgst').val(parseFloat(jsonobj[0]['taxPercent']) / 2);
                tab_index.find('.unit').find('option').remove();
                tab_index.find('.unit').append('<option value="' + jsonobj[0]['unitId'] + '">' + jsonobj[0]['productUnitName'] + '</option>');
                if (jsonobj[0]['conversionUnitIds'] != null) {
                    var ids = jsonobj[0]['conversionUnitIds'];
                    var names = jsonobj[0]['conversionUnitName'];
                    var arrUnitId = ids.split(',');
                    var arrUnitName = names.split(',');
                    var arrLen = arrUnitId.length;
                    for (var i = 0; i < arrLen; i++) {
                        tab_index.find('.unit').append('<option value="' + arrUnitId[i] + '">' + arrUnitName[i] + '</option>');
                    }
                }
                tab_index.find('.quantity').val('1');
                tab_index.find('.rate').val(jsonobj[0]['retailRate']);

                tab_index.find('.hidUnitId').val(jsonobj[0]['unitId']);
                tab_index.find('.hidRate').val(jsonobj[0]['retailRate']);

                callCallculateAmountRow(id);
            },
        });
    }

    function calculateDiscount(id) {
        var row_index = $(id).closest('tr');
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var discount = parseFloat(row_index.find('.discountamount').val() == "" ? "0" : row_index.find('.discountamount').val());
        taxableAmount = (parseFloat(taxableAmount) - parseFloat(discount)).toFixed(2);
        row_index.find('.taxableamount').html(taxableAmount);
    }

    function calculateValueOfSupply(id) {
        var row_index = $(id).closest('tr');
        var vaPercent = parseFloat(row_index.find('.vapercent').val() == "" ? "0" : row_index.find('.vapercent').val());
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var valueOfSupply = ((taxableAmount * vaPercent) / 100).toFixed(2);
        row_index.find('.valueofsupply').val(valueOfSupply);
        taxableAmount = (parseFloat(taxableAmount) + parseFloat(valueOfSupply)).toFixed(2);
        row_index.find('.taxableamount').html(taxableAmount);
    }

    function calculateVaPercent(id) {
        var row_index = $(id).closest('tr');
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var valueOfSupply = parseFloat(row_index.find('.valueofsupply').val() == "" ? "0" : row_index.find('.valueofsupply').val())
        var vaPercent = ((valueOfSupply * 100) / taxableAmount).toFixed(2);
        row_index.find('.vapercent').val(vaPercent);
        taxableAmount = (parseFloat(taxableAmount) + parseFloat(valueOfSupply)).toFixed(2);
        row_index.find('.taxableamount').html(taxableAmount);
    }

    function calculateTotalAmountRow(id) {
        var row_index = $(id).closest('tr');
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var gstDueAmount = parseFloat(row_index.find('.gstdue').val() == "" ? "0" : row_index.find('.gstdue').val())
        var totalAmount = (parseFloat(taxableAmount) + parseFloat(gstDueAmount)).toFixed(2);
        row_index.find('.totalamounttbl').html(totalAmount);

    }

    function calculateGstAmount(id) {
        var row_index = $(id).closest('tr');
        var cgst = parseFloat(row_index.find('.cgst').val() == "" ? "0" : row_index.find('.cgst').val());
        var sgst = parseFloat(row_index.find('.sgst').val() == "" ? "0" : row_index.find('.sgst').val());
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var gstDueAmount = ((taxableAmount * (cgst + sgst)) / 100).toFixed(2);
        row_index.find('.gstdue').val(gstDueAmount);
    }

    function calculateGstPercent(id) {
        var row_index = $(id).closest('tr');
        var taxableAmount = parseFloat(row_index.find('.taxableamount').html() == "" ? "0" : row_index.find('.taxableamount').html());
        var gstDueAmount = parseFloat(row_index.find('.gstdue').val() == "" ? "0" : row_index.find('.gstdue').val())
        var gstPercent = ((gstDueAmount * 100) / taxableAmount).toFixed(2);
        row_index.find('.cgst').val((gstPercent / 2).toFixed(2));
        row_index.find('.sgst').val((gstPercent / 2).toFixed(2));
    }

    function calculateTaxableAmount(row_index,resp,id) {
        var mainUnit=row_index.find('.hidUnitId').val();
        var currentUnitId=row_index.find('.unit').val();
        var rate = parseFloat(row_index.find('.rate').val() === "" ? "0" : row_index.find('.rate').val());
        var qty = parseFloat(row_index.find('.quantity').val() === "" ? "0" : row_index.find('.quantity').val());
        var hidRate=parseFloat(row_index.find('.hidRate').val() === "" ?"0":row_index.find('.hidRate').val());
        if(id.name=="unit[]"){
            if(resp)
            {
                var info=$.parseJSON(resp);
                var conversionRate=info[0]['conversionRate'];
                var newRate=(hidRate/conversionRate).toFixed(2);
                rate=newRate;
                row_index.find('.rate').val(rate);
                row_index.find('.hidUnitConversionId').val(info[0]['unitConversionId']);
            }
            else if(mainUnit==currentUnitId){
                row_index.find('.rate').val(hidRate);
                rate=hidRate;
                row_index.find('.hidUnitConversionId').val(0);
            }
        }
        var taxableAmount = (qty * rate).toFixed(2);
        row_index.find('.taxableamount').html(taxableAmount);
    }


    function callCallculateAmountRow(id) {
        var tab_index = $(id).closest('tr');
        var mainUnit=tab_index.find('.hidUnitId').val();
        var currentUnitId=tab_index.find('.unit').val();
        $.post('<?= site_url('UnitConversion/unitConversionViewByBothUnitIds'); ?>',{mainUnit:mainUnit,conversionUnit:currentUnitId},function (resp) {
            calculateTaxableAmount(tab_index,resp,id);
            if (id.name == "valueofsupply[]")
                calculateVaPercent(id);
            else
                calculateValueOfSupply(id);
            calculateDiscount(id);
            if (id.name == "gstdue[]")
                calculateGstPercent(id);
            else
                calculateGstAmount(id);
            calculateTotalAmountRow(id);
            calculateTotalAmount();
        });
    }

    var tAmount = 0, nAmount = 0, tDiscount = 0, tGstAmount = 0;

    function calculateTotalAmount() {
        nAmount = 0, tDiscount = 0;
        $('.taxableamount').each(function () {
            nAmount += parseFloat($(this).text() == "" ? "0" : $(this).text());
        });
        nAmount = nAmount.toFixed(2);
        $('#totalTaxableAmount').val(nAmount);
        tGstAmount = 0;
        $('.gstdue').each(function () {
            tGstAmount += parseFloat($(this).val() == "" ? "0" : $(this).val());
        });
        tGstAmount = tGstAmount.toFixed(2);
        $('#totalGstAmount').val(tGstAmount);

        $('#totalCgstAmount').val((tGstAmount/2).toFixed(2));
        $('#totalSgstAmount').val((tGstAmount/2).toFixed(2));

        tAmount = 0;
        tAmount = (parseFloat(nAmount) + parseFloat(tGstAmount)).toFixed(2);
        var finalAmt=parseFloat(tAmount).toFixed(0);
        $('#totalAmount').val(finalAmt);
        $('#roundFix').val((tAmount-finalAmt).toFixed(2));
    }

    $('#btnSave').click(function () {
        var tAmount = $('.totalamounttbl')[0];
        if ($('#cashOrParty option:selected').val() == 0) {
            toastr["warning"]("Choose cash or party");
            $('#cashOrParty').focus();
        }
        else if ($('#salesAccount option:selected').text() == "") {
            toastr["warning"]("Choose sales account");
            $('#salesAccount').focus();
        }
        else if (tAmount.innerHTML == "" || tAmount.innerHTML == 0) {
            toastr["warning"]("Enter atleast one product details");
        }
        else {
            var invoiceID = $('#hidInvoiceID').val();
            var arrSalesDetailsId = JSON.stringify($("input[name='hidSalesDetailsId[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrProductId = JSON.stringify($("select[name='product[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrHsnCodes = JSON.stringify($("input[name='hsnorsaccode[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrQty = JSON.stringify($("input[name='quantity[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrUnit = JSON.stringify($("input[name='hidUnitId[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrRate = JSON.stringify($("input[name='rate[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrVaPercent = JSON.stringify($("input[name='vapercent[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrValOfSupply = JSON.stringify($("input[name='valueofsupply[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrDiscount = JSON.stringify($("input[name='discountamount[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrTaxable = JSON.stringify($("span[name='taxableamount[]']")
                .map(function () {
                    return $(this).html();
                }).get());
            var arrCgst = JSON.stringify($("input[name='cgst[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrSgst = JSON.stringify($("input[name='sgst[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrGstDue = JSON.stringify($("input[name='gstdue[]']")
                .map(function () {
                    return $(this).val();
                }).get());
            var arrTotalAmount = JSON.stringify($("span[name='totalamounttbl[]']")
                .map(function () {
                    return $(this).html();
                }).get());
            var arrUnitConversionId=JSON.stringify($("input[name='hidUnitConversionId[]']")
                .map(function(){return $(this).val();}).get());
            var select = $('#cashOrParty').val();
            var isCashOrPartyIsOld = $.isNumeric(select);

            $.ajax({
                url: '<?php echo site_url('transactions/SalesInvoice/invoice') ?>',
                datatype: 'json',
                data: {
                    invoiceID: invoiceID,
                    voucherDate: $('#voucherDate').val(),
                    cashOrParty: $('#cashOrParty').val(),
                    salesAccount: $('#salesAccount option:selected').val(),
                    gstIn:$('#gstin').val(),
                    taxableAmount: $('#totalTaxableAmount').val(),
                    gstAmount: $('#totalGstAmount').val(),
                    discount:$('#roundFix').val(),
                    totalAmount: $('#totalAmount').val(),
                    description: $('#description').val(),
                    arrSalesDetailsId: arrSalesDetailsId,
                    arrProductId: arrProductId,
                    arrHsnCodes: arrHsnCodes,
                    arrQty: arrQty,
                    arrUnit: arrUnit,
                    arrUnitConversionId:arrUnitConversionId,
                    arrRate: arrRate,
                    arrVaPercent: arrVaPercent,
                    arrValOfSupply: arrValOfSupply,
                    arrDiscount: arrDiscount,
                    arrTaxable: arrTaxable,
                    arrCgst: arrCgst,
                    arrSgst: arrSgst,
                    arrGstDue: arrGstDue,
                    arrTotalAmount: arrTotalAmount,
                    arrRemoveIds: JSON.stringify(arrRemoveIds),
                    salesType: 'Sales',
                    partyRef: $('#partyRef').val() == "" ? "" : $('#partyRef').val(),
                    isCashOrPartyIsOld: isCashOrPartyIsOld == true ? true : $('#cashOrParty option:selected').text(),
                    salesOrderMasterId:$('#salesOrder option:selected').val(),
                    printMethod:$('#printmethod').val()
                },
                method: 'post',
                success: function (resp) {
                    var obj= $.parseJSON(resp);
                    var masterId=obj['id'];
                    swal({
                        title: 'Sales Invoice',
                        text: obj['msg'],
                        type: 'success',
                        showConfirmButton: false
                    });
                        if($('#printmethod').val()!=2){
                    window.open('<?=site_url()?>/printbill/sales/'+masterId,"_blank");
                    }
                    setTimeout(function () {
                        location.reload();
                    }, 800);
                }

            });
        }
    });

    function salesInvoiceEdit(invoiceID) {
        if (invoiceID > 0) {
            $.ajax({
                url: '<?php echo site_url('transactions/SalesInvoice/salesInvoiceViewById') ?>',
                datatype: 'json',
                data: {
                    invoiceID: invoiceID
                },
                method: 'post',
                success: function (resp) {
                    $('#myTab a:first').tab('show');
                    var info = $.parseJSON(resp);
                    $('#voucherNo').val(info['prefix'] + "/" + info['voucherNo']);
                    var momentDate = moment(info['entryDate'], 'YYYY-MM-DD HH:mm:ss');
                    var voucherDate = momentDate.toDate();
                    $('#voucherDate').val(formattedDate(voucherDate));
                    $('#cashOrParty').val(info['ledgerHead']).trigger('change');
                    $('#salesAccount').val(info['salesHead']).trigger('change');
                    $('#gstin').val(info['gstIn']);
                    $('#description').val(info['description']);
                    $('#totalTaxableAmount').val(info['amount']);
                    $('#totalGstAmount').val(info['taxAmount']);

                    $('#totalCgstAmount').val((parseFloat(info['taxAmount'])/2).toFixed(2));
                    $('#totalSgstAmount').val((parseFloat(info['taxAmount'])/2).toFixed(2));

                    $('#totalAmount').val(info['totalAmount']);
                    $('#hidInvoiceID').val(info['salesMasterId']);
                    $('#partyRef').val(info['partyRef']);
                    $('#btnSave').html('Update')
                    $('#btnSave').val('Update');
                    $('#salesOrder').val(info['salesOrderMasterId']==null?0:info['salesOrderMasterId']).trigger('change');
                    $('#btnDelete').removeAttr("disabled");

                    $('#tablebody').find('tr').remove();
                    $('#roundFix').val(info['discount']);

                    var details = info['details'];
                    var index = 0;
                    $.each(details, function () {
                        addRow();
                        var tab_index = $('#tablebody').find('tr:eq(' + index + ')');
                        tab_index.find('.hidSalesDetailsId').val(this.salesDetailsId);
                        tab_index.find('.procode').val(this.productId);
                        tab_index.find('td:nth-child(3) .select2-selection__rendered').attr('title', this.productCode);
                        tab_index.find('td:nth-child(3) .select2-selection__rendered').html(this.productCode);

                        tab_index.find('.proname').val(this.productId);
                        tab_index.find('td:nth-child(4) .select2-selection__rendered').attr('title', this.productName);
                        tab_index.find('td:nth-child(4) .select2-selection__rendered').html(this.productName);

                        tab_index.find('.hsnorsaccode').val(this.hsnOrSacCode);
                        tab_index.find('.quantity').val(this.qty);
                        tab_index.find('.hidUnitId').val(this.unitId);
                        tab_index.find('.rate').val(this.rate);
                        tab_index.find('.hidRate').val(this.retailRate);
                        tab_index.find('.vapercent').val(this.vaPercent);
                        tab_index.find('.valueofsupply').val(this.vaAmount);
                        tab_index.find('.discountamount').val(this.discount);
                        tab_index.find('.taxableamount').html(this.netAmount);
                        tab_index.find('.cgst').val(parseFloat(this.taxPercent) / 2);
                        tab_index.find('.sgst').val(parseFloat(this.taxPercent) / 2);
                        tab_index.find('.gstdue').val(this.taxAmount);
                        tab_index.find('.totalamounttbl').html(this.totalAmount);
                        tab_index.find('.hidUnitConversionId').val(this.unitConversionId==null?0:this.unitConversionId);
                        tab_index.find('.unit').find('option').remove();

                        $.post('<?=site_url('transactions/SalesInvoice/productViewById');?>',{productId:this.productId},function (resp) {
                            jsonobj = $.parseJSON(resp);
                            if (jsonobj[0]['conversionUnitIds'] != null) {
                                var ids = jsonobj[0]['conversionUnitIds'];
                                var names = jsonobj[0]['conversionUnitName'];
                                var convIds=jsonobj[0]['unitConversionIds'];
                                var arrUnitId = ids.split(',');
                                var arrUnitName = names.split(',');
                                var arrLen = arrUnitId.length;
                                var arrConvIds=convIds.split(',');
                                for (var i = 0; i < arrLen; i++) {
                                    var unitConversionId=tab_index.find('.hidUnitConversionId').val();
                                    if(arrConvIds[i]==unitConversionId){
                                        tab_index.find('.unit').append('<option value="' + arrUnitId[i] + '">' + arrUnitName[i] + '</option>');
                                    }
                                }
                                tab_index.find('.unit').append('<option value="' + jsonobj[0]['unitId'] + '">' + jsonobj[0]['productUnitName'] + '</option>');
                                for (var i = 0; i < arrLen; i++) {
                                    if(arrConvIds[i]!=unitConversionId){
                                        tab_index.find('.unit').append('<option value="' + arrUnitId[i] + '">' + arrUnitName[i] + '</option>');
                                    }
                                }
                            }
                            else
                                tab_index.find('.unit').append('<option value="' + jsonobj[0]['unitId'] + '">' + jsonobj[0]['productUnitName'] + '</option>');
                            // tab_index.find('.unit').trigger('change');
                        });

                        index++;
                    });
                }
            });
        }
    }

    function salesInvoiceDelete(invoiceId) {
        if (invoiceId > 0) {
            swal({
                title: 'Are you sure?',
                text: 'This invoice will be deleted!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'rgb(221, 51, 51)',
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonClass: 'swal2-cancel swal2-styled'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '<?php echo site_url('transactions/SalesInvoice/SalesInvoiceDeleteById') ?>',
                        datatype: 'json',
                        data: {
                            invoiceID: invoiceId
                        },
                        method: 'post',
                        success: function (resp) {
                            if (resp == "success") {
                                swal({
                                    title: 'Sales Invoice',
                                    text: 'Deleted successfully',
                                    type: 'success',
                                    showConfirmButton: false
                                });
                            }
                            else if (resp == "reference") {
                                swal({
                                    title: 'Sales Invoice',
                                    text: 'Cannot delete record. Reference exists',
                                    type: 'warning',
                                    showConfirmButton: false
                                });
                            }
                            setTimeout(function () {
                                location.reload();
                            }, 800);
                        }
                    });
                }
            });
        }
    }

    $('#addNewContact').click(function () {
        $('#modal-default').modal();
        $('#btnSaveContact').click(function () {
            if ($('#name').val() == "") {
                toastr['warning']("Enter contact name");
                $('#name').focus();
            }
            else {
                $.ajax({
                    url: '<?php echo site_url('masters/addnewContact') ?>',
                    datatype: 'json',
                    data: {
                        name:$('#name').val(),
                        add:$('#add').val(),
                        city:$('#city').val(),
                        state:$('#state').val(),
                        sc:$('#sc').val(),
                        land:$('#land').val(),
                        mobile:$('#mobile').val(),
                        email:$('#email').val(),
                        dlNumber:$('#dlNumber').val(),
                        creditPeriod:$('#creditPeriod').val(),
                        gst:$('#gst').val(),
                        type:$('input[name="type"]:checked').val(),
                        op:$('#op').val(),
                        description:$('#description').val(),
                        crordr:$('input[name="crordr"]:checked').val(),
                        isFromContact:false
                    },
                    method: 'post',
                    success: function (resp) {
                        if(resp){
                            var info=$.parseJSON(resp);
                            $('#cashOrParty').append('<option value="'+info.ledgerId+'">'+info.ledgerName+'</option>');
                            $('#cashOrParty').val(info.ledgerId).trigger('change');
                            $('#modal-default').modal('toggle');
                        }
                    }
                });
            }
        });
    });

    $('#cashOrParty').change(function () {
        $.post('<?= site_url('masters/contactViewByLedgerId'); ?>',{ledgerId:this.value},function (resp) {
            if(resp!="null")
            {
                var info= $.parseJSON(resp);
                $('#gstin').val(info['gstIn']);
            }
        });
    });

    $('body').ready(function () {
        <?php if($_SESSION['printMethod']=="1"){ ?>
        $('#printmethod').val(1).trigger('change');
        <?php } ?>
        <?php if($_SESSION['printMethod']=="2"){ ?>
        $('#printmethod').val(2).trigger('change');
        <?php } ?>
        $('#cashOrParty').focus();
        generalSettings();
    });

    function generalSettings() {
        $('select').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('select').focusout(function () {
            $(this).css('border','');
        });
        $('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.select2-selection').focusout(function () {
            $(this).css('border','');
        });
        $('#tablebody input:text').focus(function () {
            $(this).closest('td').css('border','2px solid #528bb3');
        });
        $('#tablebody input:text').focusout(function () {
            $(this).closest('td').css('border','');
        });
        $('#tablebody').find('.select2-selection').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('#tablebody').find('.select2-selection').focusout(function () {
            $(this).css('border','none');
        });
        $('#tablebody button').focus(function () {
            $(this).closest('th').css('border','2px solid #528bb3');
        });
        $('#tablebody button').focusout(function () {
            $(this).closest('th').css('border','');
        });
        $('.focusable').focus(function () {
            $(this).css('border','2px solid #528bb3');
        });
        $('.focusable').focusout(function () {
            $(this).css('border','');
        });
    }

</script>

</body>
</html>