<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Common/Mdl_Common');
    }

    function ChangeGoldRate(){
        $rate=$this->input->post('rate');
        $unit=$this->input->post('unit');
        $this->Mdl_Common->UpdateRate($rate,$unit);
    }


}