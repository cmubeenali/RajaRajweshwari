<?php $this->load->view('Header/header.php'); ?>
  <div class="container-fluid col-md-10">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Unit Conversion</h4>

            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab" id="navNew">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab" id="navList">List</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" name="list" role="tabpanel">
                        <div class="table-responsive table-hover" id="tblData" name="tblData">
                            <table id="data-table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Main Unit</th>
                                        <th>Conv. Unit</th>
                                        <th>Rate</th>
                                        <th>Action</th>
                                   </tr>
                                </thead>
                                <tbody>
                                <?php
									$i = 1;
									foreach ($conversions as $key) {?>
                                <tr>
                                    <th scope="row"><?=$i;?></th>
									<td><?=$key->mainUnit;?></td>
                                    <td><?=$key->conversionUnit;?></td>
                                    <td><?=$key->conversionRate;?></td>
                                    <td><i class="mdi mdi-pencil " style="cursor:pointer;" onclick="unitConversionEdit(<?=$key->unitConversionId;?>)"> </i>/<i class="mdi mdi-delete " style="cursor:pointer;" onclick="unitConversionDelete(<?=$key->unitConversionId;?>)">  </i></td>
                                </tr>
                                <?php $i++;}?>

                            </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        <div class="card-body">
                        <input type="hidden" id="hidUnitConversionId" value="0" name="hidUnitConversionId">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Main Unit</label>                                    
                                    <div class="form-group">
										<select id="mainUnit" name="mainUnit" class="form-control">
                                            <option value="0">--Select--</option>
                                            <?php
                                            $i=1;
                                            foreach($units as $key){?>
                                            <option value="<?=$key->unitId?>" ><?=$key->unitName?></option>
                                            <?php $i++;} ?>
										</select>                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Conversion Unit</label>
                                    <div class="form-group">
                                    <select id="conversionUnit" name="conversionUnit" class="form-control">
                                        <option value="0">--Select--</option>
                                        <?php
                                        $i=1;
                                        foreach($units as $key){?>
                                        <option value="<?=$key->unitId?>"><?=$key->unitName?></option>
                                        <?php $i++;} ?>
                                    </select>
                                    <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
							<div class="row">								
								<div class="col-md-6">
                                    <label class="control-label">Conversion Rate</label>
									<div class="form-group">
										<input type="text" id="conversionRate" name="conversionRate" class="form-control" onkeypress="return validateFloatKeyPress(this,event);">
										<i class="form-group__bar"></i>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 pull-right">
                                    <div class="btn-demo form-group">
										<button class="btn btn-primary pull-right" id="btnSave">Save</button>
										<button class="btn btn-light pull-right" id="btnClear">Clear</button>
									</div>
                                </div>
							</div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->


<script type="text/javascript">
    $(document).ready(function () {
    	$('.dataTables_buttons ').remove();
    	$('#data-table_length select option').css('background-color', '#020203');
    });

    toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    function validateFloatKeyPress(el, evt) {
    	var charCode = (evt.which) ? evt.which : event.keyCode;
    	var number = el.value.split('.');
    	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    		return false;
    	}
    	//just one dot (thanks ddlab)
    	if (number.length > 1 && charCode == 46) {
    		return false;
    	}
    	//get the carat position
    	var caratPos = getSelectionStart(el);
    	var dotPos = el.value.indexOf(".");
    	if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
    		return false;
    	}
    	return true;
    }

    function getSelectionStart(o) {
    	if (o.createTextRange) {
    		var r = document.selection.createRange().duplicate()
    		r.moveEnd('character', o.value.length)
    		if (r.text == '') return o.value.length
    		return o.value.lastIndexOf(r.text)
    	} else return o.selectionStart
    }    

    $('#btnSave').click(function(){
        if($('#mainUnit option:selected').val()==0){
            toastr["warning"]("Unit conversion cannot proceed without main unit");
            $('#mainUnit').focus();
        }
        else if($('#conversionUnit option:selected').val()==0){
            toastr["warning"]("Unit conversion cannot proceed without conversion unit");
            $('#conversionUnit').focus();
        }
        else if($('#conversionRate').val()==""||$('#conversionRate').val()==0){
            toastr["warning"]("Please specify conversion rate.");
            $('#conversionRate').focus();
        }
        else if($('#mainUnit option:selected').val()==$('#conversionUnit option:selected').val()){
            toastr["warning"]("Both units are same");
        }
        else{
            var unitConversionId = $('#hidUnitConversionId').val();
            $.ajax({
    			url: '<?php echo site_url('UnitConversion/Conversions')?>',
    			datatype: 'json',
    			data: {
    				unitConversionId: unitConversionId,
                    mainUnit:$('#mainUnit option:selected').val(),
                    conversionUnit:$('#conversionUnit option:selected').val(),
                    conversionRate:$('#conversionRate').val()
    			},
    			method: 'post',
    			success: function (resp) {
    				swal({
    					title: 'Unit Conversions',
    					text: resp,
    					type: 'success',
                        showConfirmButton: false,
    				})
    				setTimeout(function () {
    					location.reload();
    				}, 800);
    			}
    		}); 
        }
    });

    function switchTabNew() {
    	$('#navList').removeClass('active');
    	$('#navList').attr('aria-expanded', 'false')
    	$('#navNew').addClass('nav-link active');
    	$('#navNew').attr('aria-expanded', 'true')
    	$('#list').attr('aria-expanded', 'false')
    	$('#addnew').attr('aria-expanded', 'true')
    	$('#list').removeClass('active show');
    	$('#addnew').addClass('active show');
    }

    function clear(){
        $('#mainUnit option[value="0"]').attr('selected','selected');
        $('#conversionUnit option[value="0"]').attr('selected','selected');
        $('#select2-mainUnit-container').attr('title','--Select--');
        $('#select2-mainUnit-container').html('--Select--');
        $('#select2-conversionUnit-container').attr('title','--Select--');
        $('#select2-conversionUnit-container').html('--Select--');
        $('#conversionRate').val('');
        $('#btnSave').html('Save');
        $('#hidUnitConversionId').val('0');
    }

    $('#btnClear').click(function(){
        clear();
    });

    function unitConversionEdit(conversionId){
        if(conversionId>0){
            $.ajax({
    			url: '<?php echo site_url('UnitConversion/unitConversionViewById') ?>',
    			datatype: 'json',
    			data: {
    				unitConversionId: conversionId
    			},
    			method: 'post',
    			success: function (resp) {
    				$('#myTab a:first').tab('show');
    				var conversion = $.parseJSON(resp);                                                            
                    $('#mainUnit option[value="'+conversion['unitId']+'"]').attr('selected','selected');
                    $('#conversionUnit option[value="'+conversion['conversionUnitId']+'"]').attr('selected','selected');
                    $('#conversionRate').val(conversion['conversionRate']);
                    $('#btnSave').html('Update');
                    $('#hidUnitConversionId').val(conversion['unitConversionId']);
    			}
    		});
        }
    }

    function unitConversionDelete(unitConversionId){
        if(unitConversionId>0){
            swal({
    			title: 'Are you sure?',
    			text: 'This unit conversion will be deleted!',
    			type: 'warning',
    			showCancelButton: true,
				confirmButtonColor: 'rgb(221, 51, 51)',
    			confirmButtonClass: 'btn-danger',
    			confirmButtonText: 'Yes, delete it!',
    			cancelButtonClass: 'swal2-cancel swal2-styled'
    		}).then((result)=>{
                if(result.value){
                    $.ajax({
                        url: '<?php echo site_url('UnitConversion/unitConversionDeleteById') ?>',
                        datatype: 'json',
                        data: {
                            unitConversionId: unitConversionId
                        },
                        method: 'post',
                        success: function (resp) {
                            if(resp=="success")
                            {
                                swal({
                                    title: 'Unit Conversion',
                                    text: 'Deleted successfully',
                                    type: 'success'
                                })
                            }
                            else if(resp=="reference"){
                                swal({
                                    title: 'Unit Conversion',
                                    text: 'Cannot delete record. Reference exists',
                                    type: 'warning'
                                })
                            }
                            setTimeout(function () {
                                location.reload();
                            }, 800);
                        }
                    });
                }
            });
        }
    }
</script>

