<?php $this->load->view('Header/header.php'); ?>
<section class="container-fluid col-md-12">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Tax Report</h4>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="<?= site_url('Reports/TaxReport') ?>" method="post">
                    <div class="row" id="myDiv">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="fromDate" id="fromDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($fromDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="toDate" id="toDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($toDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="submit" value="Search" class="btn btn-primary" id="btnSearch">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive table-hover" id="tblData" name="tblData">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Voucher No</th>
                                <th>Voucher Date</th>
                                <th>Ledger</th>
                                <th>Taxable</th>
                                <th>CGST</th>
                                <th>SGST</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($tableData as $key) { ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $key->prefix . '/' . $key->voucherNo; ?></td>
                                    <td><?= date('d-m-Y', strtotime($key->entryDate)); ?></td>
                                    <td><?= $key->ledgerName; ?></td>
                                    <td><?= $key->amount; ?></td>
                                    <td><?= ($key->taxAmount / 2); ?></td>
                                    <td><?= ($key->taxAmount / 2); ?></td>
                                    <td><?= $key->totalAmount; ?></td>
                                </tr>
                                <?php $i++;
                            } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script>

    $(document).ready(function () {
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
        });
    });
    $('body').ready(function () {
        appendFinalRows();
    });

    function appendFinalRows() {
        $('#data-table tbody').append('<tr><th scope="row"></th><td></td><td></td><td style="color:red; font-weight:bold">Total : </td><td style="color:red; font-weight:bold"><?php echo $totalTaxable ?></td><td style="color:red; font-weight:bold"><?php echo($totalGst / 2) ?></td><td style="color:red; font-weight:bold"><?php echo($totalGst / 2) ?><td style="color:red; font-weight:bold"><?php echo $totalAmount ?></td></tr>');
    }

</script>

</body>
</html>