<?php $this->load->view('Header/header.php'); ?>
<section class="container-fluid col-md-12">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title"><?php echo ($salesType=="Sales"?"Stock Report" : "Stock Report(Estimate)") ?></h4>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="<?= site_url('Reports/StockReport') ?>" method="post">
                    <div class="row" id="myDiv">
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="product" id="product" class="select2" style="width: 100%;">
                                    <option value="0">--All--</option>
                                    <?php
                                    $i = 1;
                                    foreach ($productData as $key) {
                                        ?>
                                        <option value="<?= $key->productId ?>"> <?= $key->productName ?></option>
                                        <?php $i++;
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="submit" value="Search" class="btn btn-primary" id="btnSearch">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive table-hover" id="tblData" name="tblData">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Purity</th>
                                <th>Name</th>
                                <th>Unit</th>
                                <th>Inward</th>
                                <th>Outward</th>
                                <th>Current Stock</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($tableData as $key) { ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $key->Code; ?></td>
                                    <td><?= $key->Product; ?></td>
                                    <td><?= $key->Unit; ?></td>
                                    <td><?= $key->Inward; ?></td>
                                    <td><?= $key->Outward; ?></td>
                                    <td><?= $key->CurrentStock; ?></td>
                                </tr>
                                <?php $i++;
                            } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script>

    $(document).ready(function () {
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
        });
    });

    $('body').ready(function () {
        var productId =<?php echo $productId; ?>;
        $('#product').val(productId).trigger('change');
    });

</script>

</body>
</html>