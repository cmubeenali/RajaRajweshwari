<?php $this->load->view('Header/header.php'); ?>
<section class="container-fluid col-md-12">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title"><?php echo ($type=="Purchase"?"Purchase Report" : "Purchase Report(Without)") ?></h4>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="<?= site_url('Reports/PurchaseReport') ?>" method="post">
                    <div class="row" id="myDiv">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="fromDate" id="fromDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($fromDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="toDate" id="toDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($toDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="ledger" id="ledger" class="select2" style="width: 100%;">
                                    <option value="0">--All--</option>
                                    <?php
                                    $i = 1;
                                    foreach ($ledgerData as $key) {
                                        ?>
                                        <option value="<?= $key->ledgerId ?>"> <?= $key->ledgerName ?></option>
                                        <?php $i++;
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!--                            <div class="form-group">-->
                            <!--                                <input type="text" name="search" id="search" class="form-control"-->
                            <!--                                       placeholder="Search by column name">-->
                            <!--                            </div>-->
                            &nbsp;
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="submit" value="Search" class="btn btn-primary" id="btnSearch">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive table-hover" id="tblData" name="tblData">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Voucher No</th>
                                <th>Voucher Date</th>
                                <th>Ledger</th>
                                <th>Source</th>
                                <th>GSTIN</th>
                                <th>Taxable</th>
                                <th>Tax Amt.</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($tableData as $key) { ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $key->voucherNo; ?></td>
                                    <td><?= $key->voucherDate; ?></td>
                                    <td><?= $key->ledgerName; ?></td>
                                    <td><?= $key->vendorType; ?></td>
                                    <td><?= $key->gstIn; ?></td>
                                    <td><?= $key->amount; ?></td>
                                    <td><?= $key->taxAmount; ?></td>
                                    <td><?= $key->totalAmount; ?></td>
                                </tr>
                                <?php $i++;
                            } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script>

    $(document).ready(function () {
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
        });
    });
    $('body').ready(function () {
        var ledgerId =<?= $ledgerId; ?>;
        $('#ledger').val(ledgerId).trigger('change');
        appendFinalRows();
    });

    function appendFinalRows() {
        var totalTaxable=0;
        var totalGstAmt=0;
        var totalAmount=0;
        $('#data-table tbody tr').each(function () {
            var val= $(this).find('td:nth-child(6)').html();
            totalTaxable= parseFloat(parseFloat(totalTaxable)+parseFloat(val)).toFixed(2);
            val= $(this).find('td:nth-child(7)').html();
            totalGstAmt= parseFloat(parseFloat(totalGstAmt)+parseFloat(val)).toFixed(2);
            val= $(this).find('td:nth-child(8)').html();
            totalAmount= parseFloat(parseFloat(totalAmount)+parseFloat(val)).toFixed(2);
        });
        $('#data-table tbody').append("<tr><td colspan='3'></td><td colspan='2' style='font-weight:bold; color:#e30000;'>Total</td><td style='font-weight:bold; color:#e30000;'>"+totalTaxable+"</td><td style='font-weight:bold; color:#e30000;'>"+totalGstAmt+"</td><td style='font-weight:bold; color:#e30000;'>"+totalAmount+"</td></tr>");
    }

</script>

</body>
</html>