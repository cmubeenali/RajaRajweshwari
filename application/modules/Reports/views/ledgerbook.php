<?php $this->load->view('Header/header.php'); ?>
<section class="container-fluid col-md-12">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title"><?php echo ($type=="LedgerBook"?"Ledger Book" : "Leger Book(Estimate)") ?></h4>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <?php if($type=="LedgerBook"){ ?>
                <form action="<?= site_url('Reports/LedgerBook') ?>" method="post">
                <?php } else { ?>
                    <form action="<?= site_url('Reports/LedgerBookEstimate') ?>" method="post">
                <?php } ?>
                    <div class="row" id="myDiv">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="fromDate" id="fromDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($fromDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" name="toDate" id="toDate" class="form-control"
                                       value="<?= date('Y-m-d', strtotime($toDate)); ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="ledger" id="ledger" class="select2" style="width: 100%;">
                                    <option value="0">--All--</option>
                                    <?php
                                    $i = 1;
                                    foreach ($ledgerData as $key) {
                                        ?>
                                        <option value="<?= $key->ledgerId ?>"> <?= $key->ledgerName ?></option>
                                        <?php $i++;
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!--                            <div class="form-group">-->
                            <!--                                <input type="text" name="search" id="search" class="form-control"-->
                            <!--                                       placeholder="Search by column name">-->
                            <!--                            </div>-->
                            &nbsp;
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="submit" value="Search" class="btn btn-primary" id="btnSearch">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive table-hover" id="tblData" name="tblData">
                        <table id="data-table" class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Voucher No</th>
                                <th>Voucher Date</th>
                                <th>Type</th>
                                <th>Ledger</th>
                                <th>Reference</th>
                                <th>Dr</th>
                                <th>Cr</th>
                                <th>Close Bal.</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($tableData as $key) { ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $key->voucherNo; ?></td>
                                    <td><?= $key->voucherDate; ?></td>
                                    <td><?= $key->voucherType; ?></td>
                                    <td><?= $key->ledgerName; ?></td>
                                    <td><?= $key->partyRef; ?></td>
                                    <td><?= $key->Dr; ?></td>
                                    <td><?= $key->Cr; ?></td>
                                    <td><?= $key->closingBalance; ?></td>
                                </tr>
                                <?php $i++;
                            } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script>

    $(document).ready(function () {
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
        });
    });
    $('body').ready(function () {
        var ledgerId =<?php echo $ledgerId; ?>;
        $('#ledger').val(ledgerId).trigger('change');
        appendFinalRows();
    });

    function appendFinalRows() {
        if ($('#ledger').val() > 0) {
            $('#data-table tr').eq(0).after('<tr style><th scope="row"></th><td></td><td></td><td></td><td style="color:red; font-weight:bold">Opening Balance</td><td></td><td style="color:red; font-weight:bold"><?php echo($openingBalance[0]["CurrentStock"] >= 0 ? $openingBalance[0]["CurrentStock"] : "") ?></td><td style="color:red; font-weight:bold"><?php echo($openingBalance[0]["CurrentStock"] < 0 ? ($openingBalance[0]['CurrentStock'] * -1) : "") ?></td><td style="color:red; font-weight:bold"></td><td></td></tr>');
            $('#data-table tbody').append('<tr><th scope="row"></th><td></td><td></td><td></td><td style="color:red; font-weight:bold">Total : </td><td style="color:red; font-weight:bold">Closing Balance</td><td style="color:red; font-weight:bold"><?php echo($closingBalance[0]["CurrentStock"] >= 0 ? $closingBalance[0]["CurrentStock"] : "") ?></td><td style="color:red; font-weight:bold"><?php echo($closingBalance[0]["CurrentStock"] < 0 ? ($closingBalance[0]['CurrentStock'] * -1) : "") ?></td><td style="color:red; font-weight:bold"></td><td></td></tr>');
        }
    }

    function transView(voucherId, voucherType) {

    }

</script>

</body>
</html>
