<?php

class Mdl_ledgerbook extends CI_Model
{
    function LedgerBookFill($fromDate, $toDate, $ledgerId, $type)
    {
        $sql_estimate = ($type == "LedgerBook" ? " and voucherType != 'Estimate Bill' and voucherType!='Purchase Without' " : " ");
        $sql = "select voucherId,voucherNo,voucherType,Dr,Cr,ledger_tbl.ledgerName, DATE_FORMAT(voucherDate,\"%d/%m/%Y\") as voucherDate from transaction_tbl INNER JOIN  
                     ledger_tbl ON transaction_tbl.ledgerId = ledger_tbl.ledgerId   
        where (voucherDate between '" . $fromDate . "' and '" . $toDate . "') {$sql_estimate} ";
        $str_extra = $ledgerId == 0 ? " " : (" and transaction_tbl.ledgerId = " . $ledgerId);
        $query = $this->db->query($sql . $str_extra);
        return $query->result();
    }

    function LedgerOpeningBalance($fromDate, $ledgerId, $type)
    {
        $sql_estimate = ($type == "LedgerBook" ? " and voucherType != 'Estimate Bill' and voucherType!='Purchase Without' " : "  ");
        $str_extra = $ledgerId == 0 ? " " : (" and transaction_tbl.ledgerId = " . $ledgerId);
        $sql = "select ifnull(SUM(TEMP2.CurrentStock),0) as CurrentStock from(
      SELECT transId,
            sum(Dr) as Debit,
            sum(Cr)as Credit,
            sum(Dr)-sum(Cr) as CurrentStock
            FROM(
                select transId,voucherNo,voucherType,Dr,Cr,ledger_tbl.ledgerName from transaction_tbl INNER JOIN
                                 ledger_tbl ON  transaction_tbl.ledgerId = ledger_tbl.ledgerId 
                                 where (voucherDate < '" . $fromDate . "' ) {$sql_estimate} {$str_extra})AS TEMP GROUP BY TEMP.transId )
                as TEMP2";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}