<?php

class Mdl_reports extends CI_Model
{
    function StockFill($productId, $salesType)
    {
        $sql_estimate = $salesType == "Sales" ? " " : ('UNION ALL
        
                  SELECT
                    SM.productId as ProductId,
        
                    0            as Inward,
                    SUM(unitConversion(SM.unitId,ifnull(SM.unitConversionId,0),SM.qty))  as Outward
                  FROM salesdetails_tbl as SM, product_tbl P, salesmaster_tbl SIM
                  where SM.productId = P.productId and SM.salesMasterId=SIM.salesMasterId and SIM.salesType = "Estimate"
                  Group by SM.productId
                  UNION ALL
                  
                  SELECT
                    PM.productId as ProductId,
        
                    SUM(unitConversion(PM.unitId,ifnull(PM.unitConversionId,0),PM.qty))  as Inward,
                    0            as Outward
                  FROM purchaseldetails_tbl as PM, product_tbl P, purchasemaster_tbl PIM
                  where PM.productId = P.productId and PM.purchaseMasterId=PIM.purchaseMasterId and PIM.taxForm="true"
                  Group by PM.productId');
        $sql = "
        select
          TEMP2.ProductId,
          product_tbl.productCode as Code,
          product_tbl.productName as Product,
          unit_tbl.unitName       AS Unit,
          TEMP2.Inward,
          TEMP2.Outward,
          TEMP2.CurrentStock
        from (
        
               SELECT
        
                 ProductId,
                 sum(Inward)                as Inward,
                 sum(Outward)               as Outward,
                 sum(Inward) - sum(Outward) as CurrentStock
        
               FROM
        
                 (SELECT
                    PM.productId as ProductId,
        
                    SUM(unitConversion(PM.unitId,ifnull(PM.unitConversionId,0),PM.qty))  as Inward,
                    0            as Outward
                  FROM purchaseldetails_tbl as PM, product_tbl P, purchasemaster_tbl PIM
                  where PM.productId = P.productId and PM.purchaseMasterId=PIM.purchaseMasterId and PIM.taxForm='false'
                  Group by PM.productId
                  UNION ALL
        
                  SELECT
                    SM.productId as ProductId,
        
                    0            as Inward,
                    SUM(unitConversion(SM.unitId,ifnull(SM.unitConversionId,0),SM.qty))  as Outward
                  FROM salesdetails_tbl as SM, product_tbl P, salesmaster_tbl SIM
                  where SM.productId = P.productId and SM.salesMasterId=SIM.salesMasterId and SIM.salesType = 'Sales'
                  Group by SM.productId
                  UNION ALL
        
                  SELECT
                    product_tbl.productId    as ProductId,
        
                    product_tbl.openingStock as Inward,
                    0                        as Outward
                  FROM product_tbl
        
                  UNION ALL
                  SELECT
                    R.productId as ProductId,
        
                    0           as Inward,
                    SUM(unitConversion(R.unitId,ifnull(R.unitConversionId,0),R.qty))  as Outward
                  FROM purchasereturndetails_tbl as R, product_tbl P
                  where R.productId = P.productId
                  Group by R.productId
                  UNION ALL
        
                  SELECT
                    S.productId as ProductId,
        
                    SUM(unitConversion(S.unitId,ifnull(S.unitConversionId,0),S.qty))  as Inward,
                    0           as Outward
                  FROM salesreturndetails_tbl as S, product_tbl P
                  where S.productId = P.productId
                  Group by S.productId
                                    
                  {$sql_estimate}
                  
                 )
                   AS TEMP
               Group by ProductId) as TEMP2 left join product_tbl on product_tbl.productId = TEMP2.ProductId
          left join unit_tbl On product_tbl.unitId = unit_tbl.unitId ";
        $sql_extra = $productId == 0 ? " " : (" where TEMP2.ProductId = " . $productId);
        $query = $this->db->query($sql . $sql_extra);
        return $query->result();
    }

    function TaxReport($fromDate, $toDate)
    {
        $sql = "select * from salesmaster_tbl t1
          inner join ledger_tbl lt on t1.ledgerHead = lt.ledgerId 
          where t1.entryDate between '{$fromDate}' and '{$toDate}' 
          and t1.salesType='Sales' 
        order by salesMasterId desc";
        return $this->db->query($sql)->result();
    }

    function PurchaseReport($fromDate, $toDate, $ledgerId, $repType)
    {
        $sql_extra_rep = $repType == "Purchase" ? " and t1.taxForm='false'" : " and t1.taxForm='true'";
        $sql_extra = ($ledgerId == 0 ? " " : " and t1.ledgerHead=" . $ledgerId);
        $sql = "select t1.purchaseMasterId,concat(t1.prefix,\"/\",convert (t1.voucherNo,char)) as voucherNo,
          DATE_FORMAT(t1.entryDate,'%d/%m/%Y')as voucherDate,
          lt.ledgerName,t1.gstIn,t1.amount,t1.taxAmount,t1.totalAmount,
          t1.vendorType
        from purchasemaster_tbl t1
        inner join ledger_tbl lt on t1.ledgerHead = lt.ledgerId
        where t1.entryDate between \"$fromDate\" and \"$toDate\"
        {$sql_extra_rep}
        {$sql_extra}
        order by t1.purchaseMasterId desc ";
        return $this->db->query($sql)->result();
    }

    function SalesReport($fromDate, $toDate, $ledgerId)
    {
        $sql_extra = ($ledgerId == 0 ? " " : " and t1.ledgerHead=" . $ledgerId);
        $sql = "select t1.salesMasterId,concat(t1.prefix,\"/\",convert (t1.voucherNo,char)) as voucherNo,
                  concat(sot.prefix,\"/\",convert (sot.voucherNo,char)) as orderNo,
                  DATE_FORMAT(t1.entryDate,'%d/%m/%Y')as voucherDate,
                  lt.ledgerName,t1.gstIn,t1.amount,t1.taxAmount,t1.totalAmount
                  from salesmaster_tbl t1
                  inner join ledger_tbl lt on t1.ledgerHead = lt.ledgerId
                  left join salesordermaster_tbl sot on t1.salesOrderMasterId=sot.salesOrderMasterId
                  where t1.entryDate between \"$fromDate\" and \"$toDate\"
                  and t1.salesType='Sales' 
                  {$sql_extra}
                  order by t1.salesMasterId desc;";
        return $this->db->query($sql)->result();
    }

}