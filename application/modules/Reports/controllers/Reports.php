<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller
{
    private $repType = "LedgerBook";
    private $repTypePurchase = "Purchase";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Dashboard/Mdl_dashboard');
        $this->load->model('Mdl_ledgerbook');
        $this->load->model('common/Mdl_common');
        $this->load->model('transactions/Mdl_salesinvoice');
        $this->load->model('Mdl_reports');
    }

    public function index()
    {
        if ($this->session->userdata('userName')) {
            //if "superuser found"
            return $this->load->view('Reports/dashboard');
        } else {
            //if "no admin found"
            return $this->load->view('Dashboard/login');

        }
    }

    public function LedgerBook()
    {

        $_SESSION['pageTitle'] = "Ledger Book";

        $fromDate = null;
        $toDate = null;
        $ledgerId = 0;

        $data['ledgerData'] = $this->Mdl_common->LedgerSelectFill();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data['fromDate'] = $this->input->get('fromDate') == "" ? date("Y-m-d") : $this->input->get('fromDate');
            $data['toDate'] = $this->input->get('toDate') == "" ? date("Y-m-d") : $this->input->get('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->get('ledger') == "" ? 0 : $this->input->get('ledger');
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['fromDate'] = $this->input->post('fromDate') == "" ? date("Y-m-d") : $this->input->post('fromDate');
            $data['toDate'] = $this->input->post('toDate') == "" ? date("Y-m-d") : $this->input->post('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->post('ledger') == "" ? 0 : $this->input->post('ledger');
        }
        $tableData = $this->Mdl_ledgerbook->LedgerBookFill($fromDate, $toDate, $ledgerId, $this->repType);
        $openingBalance = $this->Mdl_ledgerbook->LedgerOpeningBalance($fromDate, $ledgerId, $this->repType);
        $closingBalance = $openingBalance;
        foreach ($tableData as $item) {
            $closingBalance[0]['CurrentStock'] = $closingBalance[0]['CurrentStock'] + ($item->Dr - $item->Cr);
            $item->closingBalance = $closingBalance[0]['CurrentStock'];
            $item->partyRef = "";
            if ($item->voucherType == "Sales Invoice") {
                $masterInfo = $this->Mdl_salesinvoice->SalesInvoiceMasterViewById($item->voucherId);
                $item->partyRef = $masterInfo['partyRef'];
            }
        }
        $data['tableData'] = $tableData;
        $data['openingBalance'] = $openingBalance;
        $data['closingBalance'] = $closingBalance;
        $data['ledgerId'] = $ledgerId;
        $data['type'] = $this->repType;
        return $this->load->view('ledgerbook', $data);
    }

    function LedgerBookEstimate()
    {
        $_SESSION['pageTitle'] = "Ledger Book Estimate";
        $this->repType = "LedgerBookEstimate";
        $this->LedgerBook();
    }

    function StockReport()
    {
        $_SESSION['pageTitle'] = "Stock Report";
        $productId = 0;
        $data['productData'] = $this->Mdl_common->ProductNameSelectFill();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $productId = $this->input->get('product');
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $productId = $this->input->post('product');
        }
        $data['tableData'] = $this->Mdl_reports->StockFill($productId, "Sales");
        $data['productId'] = $productId;
        $data['salesType'] = "Sales";
        return $this->load->view('stockreport', $data);
    }

    function StockReportEstimate()
    {
        $_SESSION['pageTitle'] = "Stock Report Estimate";
        $productId = 0;
        $data['productData'] = $this->Mdl_common->ProductNameSelectFill();
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $productId = $this->input->get('product');
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $productId = $this->input->post('product');
        }
        $data['tableData'] = $this->Mdl_reports->StockFill($productId, "Estimate");
        $data['productId'] = $productId;
        $data['salesType'] = "Estimate";
        return $this->load->view('stockreport', $data);
    }

    function TaxReport()
    {
        $_SESSION['pageTitle'] = "Tax Report";

        $fromDate = null;
        $toDate = null;
        $data = [];
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data['fromDate'] = $this->input->get('fromDate') == "" ? date("Y-m-d") : $this->input->get('fromDate');
            $data['toDate'] = $this->input->get('toDate') == "" ? date("Y-m-d") : $this->input->get('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['fromDate'] = $this->input->post('fromDate') == "" ? date("Y-m-d") : $this->input->post('fromDate');
            $data['toDate'] = $this->input->post('toDate') == "" ? date("Y-m-d") : $this->input->post('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
        }

        $data['tableData'] = $this->Mdl_reports->TaxReport($fromDate, $toDate);
        $totalTaxable = 0;
        $totalGstAmount = 0;
        $totalAmount = 0;
        foreach ($data['tableData'] as $item) {
            $totalTaxable = $totalTaxable + $item->amount;
            $totalGstAmount = $totalGstAmount + $item->taxAmount;
            $totalAmount = $totalAmount + $item->totalAmount;
        }
        $data['totalTaxable'] = $totalTaxable;
        $data['totalGst'] = $totalGstAmount;
        $data['totalAmount'] = $totalAmount;
        return $this->load->view('taxreport', $data);
    }

    function PurchaseReport()
    {
        $_SESSION['pageTitle'] = "Purchase Report";

        $fromDate = null;
        $toDate = null;
        $data = [];
        $ledgerId = 0;
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data['fromDate'] = $this->input->get('fromDate') == "" ? date("Y-m-d") : $this->input->get('fromDate');
            $data['toDate'] = $this->input->get('toDate') == "" ? date("Y-m-d") : $this->input->get('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->get('ledger') == "" ? 0 : $this->input->get('ledger');
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['fromDate'] = $this->input->post('fromDate') == "" ? date("Y-m-d") : $this->input->post('fromDate');
            $data['toDate'] = $this->input->post('toDate') == "" ? date("Y-m-d") : $this->input->post('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->post('ledger') == "" ? 0 : $this->input->post('ledger');
        }
        $data['ledgerData'] = $this->Mdl_common->LedgerSelectFill();
        $data['tableData'] = $this->Mdl_reports->PurchaseReport($fromDate, $toDate, $ledgerId, $this->repTypePurchase);
        $data['ledgerId'] = $ledgerId;
        $data['type'] = $this->repTypePurchase;
        return $this->load->view('purchasereport', $data);
    }

    function PurchaseReportWithout()
    {
        $this->repTypePurchase = "PurchaseWithout";
        $this->PurchaseReport();
    }

    function SalesReport()
    {
        $_SESSION['pageTitle'] = "Sales Report";

        $fromDate = null;
        $toDate = null;
        $data = [];
        $ledgerId = 0;
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data['fromDate'] = $this->input->get('fromDate') == "" ? date("Y-m-d") : $this->input->get('fromDate');
            $data['toDate'] = $this->input->get('toDate') == "" ? date("Y-m-d") : $this->input->get('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->get('ledger') == "" ? 0 : $this->input->get('ledger');
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['fromDate'] = $this->input->post('fromDate') == "" ? date("Y-m-d") : $this->input->post('fromDate');
            $data['toDate'] = $this->input->post('toDate') == "" ? date("Y-m-d") : $this->input->post('toDate');
            $fromDate = $data['fromDate'];
            $toDate = $data['toDate'];
            $ledgerId = $this->input->post('ledger') == "" ? 0 : $this->input->post('ledger');
        }
        $data['ledgerData'] = $this->Mdl_common->LedgerSelectFill();
        $data['tableData'] = $this->Mdl_reports->SalesReport($fromDate, $toDate, $ledgerId);
        $data['ledgerId'] = $ledgerId;
        return $this->load->view('salesreport', $data);
    }

}