<?php

class Mdl_print extends CI_Model
{
    public function fetchUserDataByLedgerHead($ledgerHead)
    {
        // echo $ledgerHead;
        $this->db->select('ledgerName');
        $this->db->from('ledger_tbl');
        $this->db->where('ledgerId', $ledgerHead);
        $result = $this->db->get()->row();
        return $result;
    }

    function purchaseInvoiceDetailsViewByIdMaster($masterId)
    {

        $query = $this->db->query('select t1.purchaseDetailsId,
     t1.purchaseMasterId,
     t1.unitId,
     t1.unitConversionId,
     t1.unitConversionRate,
     t1.taxType,
     t2.taxPercent,
     t1.qty,
     t1.rate,
     t1.netAmount,
     t1.taxAmount,
     t1.totalAmount,
     t1.hsnOrSacCode,
     t1.grossWeight,
     t1.stoneWeight,
     t1.stoneRate,
     t1.nettWeight,
     t1.wastage,
     t1.makingCharge,
     t2.productCode,
     t2.productName,
     ReturnUnitName(t1.unitId,ifnull(t1.unitConversionId,0)) as unitName
    from purchaseldetails_tbl t1
    inner join product_tbl t2 on t1.productId=t2.productId
    inner join unit_tbl t3 on t1.unitId=t3.unitId 
    where t1.purchaseMasterId=' . $masterId);
        return $query->result_array();
    }

    function purchaseInvoiceViewByIdMaster($invoiceId)
    {
        $this->db->where('purchaseMasterId', $invoiceId);
        return $qry = $this->db->get('purchasemaster_tbl')->row_array();
    }
}