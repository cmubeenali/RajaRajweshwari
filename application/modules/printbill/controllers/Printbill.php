<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printbill extends MX_Controller
{
    

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Dashboard/Mdl_dashboard');
        $this->load->model('Mdl_print');
        $this->load->model('transactions/Mdl_salesinvoice');

    }

    public function estimate($masterId)
    {
        $data['masterInfo']=$this->Mdl_salesinvoice->SalesInvoiceMasterViewById($masterId);
        $data['details']=$this->Mdl_salesinvoice->SalesDetailsViewByMasterId($masterId);
        $data['invoiceType']="estimate";
        $this->load->view('dot',$data);
    }
    public function sales($masterId)
    {
        $data['masterInfo']=$this->Mdl_salesinvoice->SalesInvoiceMasterViewById($masterId);
        $data['details']=$this->Mdl_salesinvoice->SalesDetailsViewByMasterId($masterId);
        $data['invoiceType']="Sales Invoice";
        $this->load->view('dot',$data);
    }
    public function purchase($masterId)
    {
        $data['masterInfo']=$this->Mdl_print->purchaseInvoiceViewByIdMaster($masterId);
        $data['details']=$this->Mdl_print->purchaseInvoiceDetailsViewByIdMaster($masterId);
        $data['invoiceType']="Purchase Invoice";
        $this->load->view('dot',$data);
    }

}