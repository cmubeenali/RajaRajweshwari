<?php
// print_r ($masterInfo);
$customerName= $this->Mdl_print->fetchUserDataByLedgerHead($masterInfo['ledgerHead']);
// echo $customerName->ledgerName;
?>
<html>
<head>
    <style>
         @page 
    {
        size: A5;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    .right{
        float: right;
        /* width: 100%; */
    }
    .left{
        float: left;
        /* width: 100%; */
    }
    .center{
        text-align: left;
    }
    .center p { font-family: Arial; letter-spacing: 2px;}
    .table_line_full{font-family: Arial; letter-spacing: 50px;}
    .center p span { letter-spacing: 2px; margin: 0 5px;font-weight: bold;text-transform: uppercase;  }
   .itemTable th,td{
        padding-right: 15px;
       text-align: left;
   }
    </style>
</head>
<body>
    <br>
    <br>
    <br>
    <!-- header_data -->
    <table style="width: 100%;border-collapse: collapse;">

<tr>
    <td class="left">Voucher Date&nbsp;:&nbsp;<?=date('d-m-Y',strtotime($masterInfo['entryDate']));?></td>
    <td class="right">Invoice Number&nbsp;:&nbsp;<?=$masterInfo['prefix']."/".$masterInfo['voucherNo']?></td>
</tr>
<tr>
        <td class="left"> Time&nbsp;:&nbsp; <?php echo date("h:i A"); ?></td>
        <td class="right">Sales Person&nbsp;:&nbsp;<?= $_SESSION['userName'];?></td>
 </tr>
 <tr>
            <td class="left">Customer Name&nbsp;:&nbsp;<?=$customerName->ledgerName;?></td>
            <td class="right">Gold rate per gram and pavan</td>
</tr>
<tr>
    <td colspan="2" class="center heading" style="text-align: center;"><p>&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;
        <span><?=$invoiceType?></span> 
        &#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;&#x2015;</p></td>
</tr>
</table>
<br>
<table  class="itemTable" style=" border-collapse: collapse;width: 100%"> 
    <thead>
        <?php 
        if($invoiceType== "estimate"){ ?>
        <tr>
            <th>Purity</th>
            <th>Product</th>
            <th>HSN</th>
            <th> Qty</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>VA%</th>
            <th>VAL(Supply)</th>
            <th>Disc.</th>
            <th>Total </th>
        </tr>
        <?php } ?>
        <?php
        if($invoiceType == "Purchase Invoice"){ ?>
            <?php
            if($_SESSION['isWithoutTax']=='true'||$masterInfo['vendorType']=='Customer'){ ?>
            <tr>
                <th>Purity</th>
                <th>Product</th>
                <th>HSN</th>
                <th>Qty</th>
                <th>Unit</th>
                <th>Rate</th>
                <th>Total </th>
            </tr>
               <?php } else {?>
                <tr>
                    <th>Purity</th>
                    <th>Product</th>
                    <th>HSN</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Taxable</th>
                    <th>CGST</th>
                    <th>SGST</th>
                    <th>Total </th>
                </tr>
                 <?php } ?>
        <?php } ?>
        <tr>
            <td class="center" colspan="10" style="letter-spacing: 15px">........................................... </td>
        </tr>
    </thead>
    <tbody>
        <?php
        if($invoiceType=="estimate"){
            $totalRate=0;
            $totalVA=0;
            $totalgsts=0;
            $totalTaxable=0;
            $totalDiscount=0;
            $totalAmount=0;
        foreach($details as $detail){
            $totalRate+=$detail['rate'];
            $totalVA+=$detail['vaAmount'];
            $totalgsts+=$detail['taxAmount']/2;
            $totalTaxable+=$detail['netAmount'];
            $totalAmount+=$detail['totalAmount'];
            $totalDiscount+=$detail['discount'];
            ?>
              <tr>
                    <td><?=$detail['productCode'];?></td>
                    <td><?=$detail['productName'];?></td>
                    <td><?=$detail['hsnOrSacCode'];?></td>
                    <td><?=$detail['qty'];?></td>
                    <td><?=$detail['unitName'];?></td>
                    <td><?=$detail['rate'];?></td>
                    <td><?=$detail['vaPercent'];?></td>
                    <td><?=$detail['vaAmount'];?></td>
                    <td><?=$detail['discount'];?></td>
                    <td><?=$detail['totalAmount'];?></td>
                </tr>
                <?php } ?>
                    <tr>
                        <td class="center" colspan="10" style="letter-spacing: 15px">........................................... </td>
                    </tr>
                    <tr>
                            <td colspan="5">Total</td>
                            
                            <td><?=$totalRate?></td>
                            <td></td>
                            <td><?=$totalVA?></td>
                            <td><?=$totalDiscount?></td>
                            <td><?=$masterInfo['totalAmount']?></td>
                            
                        </tr>
                        <tr>
                                <td class="center" colspan="13" style="letter-spacing: 15px">........................................... </td>
                            </tr>
       <?php } ?>

       <?php
      $totalRate=0; 
    
      $totalgsts=0;
      $totalTaxable=0;
      $totalAmount=0;
        if($invoiceType=="Purchase Invoice"){
        foreach($details as $detail){ 
            $totalRate+=$detail['rate'];
            
            $totalgsts+=$detail['taxAmount']/2;
            $totalTaxable+=$detail['netAmount'];
            $totalAmount+=$detail['totalAmount'];
            ?>
              <tr>
                    <td><?=$detail['productCode'];?></td>
                    <td><?=$detail['productName'];?></td>
                    <td><?=$detail['hsnOrSacCode'];?></td>
                    <td><?=$detail['qty'];?></td>
                    <td><?=$detail['unitName'];?></td>
                    <td><?=$detail['rate'];?></td>
                    <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?> <td><?= $detail['netAmount'];?></td><?php }} ?>
            <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?> <td><?=$detail['taxAmount']/2;?></td><?php }} ?>
            <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?> <td><?=$detail['taxAmount']/2;?></td><?php }} ?>
                    <?php if($masterInfo['vendorType']=="Customer"){?><td><?=$detail['netAmount'];?></td><?php }else{ ?><td><?=$detail['totalAmount'];?></td><?php }?>
                </tr>
       <?php } ?>
                    <tr>
                        <td class="center" colspan="14" style="letter-spacing: 15px">........................................... </td>
                    </tr>
                    <tr>
                            <td colspan="5">Total</td>

                            <td></td>
                        <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?><td><?=$totalTaxable?></td><?php }} ?>
                        <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?> <td><?=$totalgsts?></td><?php }} ?>
                        <?php if($_SESSION['isWithoutTax']=="false"){ if($masterInfo['vendorType']=="Vendor"){ ?> <td><?=$totalgsts?></td><?php }} ?>
                        <?php if($masterInfo['vendorType']=="Customer"){?><td><?=$totalTaxable?></td><?php }else{ ?><td><?=$totalAmount?></td><?php }?>
                        </tr>
                        <tr>
                                <td class="center" colspan="10" style="letter-spacing: 15px">........................................... </td>
                            </tr>
       <?php } ?>
          
               
                       
                
    </tbody>
</table>
    <?php if($invoiceType=="Sales Invoice"){ ?>
    <table style="width:100%;text-align: left; font-size: 13px;">
        <tr>
            <th>Purity</th>
            <th>Product</th>
            <th>HSN</th>
            <th>Qty</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>VA%</th>
            <th style="display: none;">VAL(Supply)</th>
            <th>Disc.</th>
            <th>Taxable</th>
            <th>CGST</th>
            <th>SGST</th>
            <th>Amount </th>
        </tr>
            <?php
            $totalRate=0;
            $totalDiscount=0;
            $totalVA=0;
            $totalgsts=0;
            $totalTaxable=0;
            $totalAmount=0;
            if($invoiceType=="Sales Invoice"){
                foreach($details as $detail){

                    $totalRate+=$detail['rate'];
                    $totalVA+=$detail['vaAmount'];
                    $totalgsts+=$detail['taxAmount']/2;
                    $totalTaxable+=$detail['netAmount'];
                    $totalAmount+=$detail['totalAmount'];
                    $totalDiscount+=$detail['discount'];
                    ?>
                    <tr>
                        <td><?=$detail['productCode'];?></td>
                        <td><?=$detail['productName'];?></td>
                        <td><?=$detail['hsnOrSacCode'];?></td>
                        <td><?=$detail['qty'];?></td>
                        <td><?=$detail['unitName'];?></td>
                        <td><?=$detail['rate'];?></td>
                        <td><?=$detail['vaPercent'];?></td>
                        <td style="display: none;"><?=$detail['vaAmount'];?></td>
                        <td><?=$detail['discount'];?></td>
                        <td><?=$detail['netAmount'];?></td>
                        <td><?=$detail['taxAmount']/2;?></td>
                        <td><?=$detail['taxAmount']/2;?></td>
                        <td><?=$detail['totalAmount'];?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="center" colspan="13" style="letter-spacing: 15px">........................................... </td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?=$totalRate;?></td>
                    <td></td>
                    <td><?=$totalDiscount?></td>
                    <td><?=$totalTaxable?></td>
                    <td><?=$totalgsts?></td>
                    <td><?=$totalgsts?></td>
                    <td><?=$totalAmount;?></td>

                </tr>
                <tr>
                    <td class="center" colspan="13" style="letter-spacing: 15px">........................................... </td>
                </tr>
            <?php } ?>
    </table>
    <?php } ?>

    <table style="width: 100%; display: table; font-size: 13px;" id="lastRowForPurchase">
        <tr>
            <td colspan="8"> <span class="left" id="rupeeWord"></span> </td>
            <td class="colException" colspan="1"> <span class="right" > <b>Total Amount&nbsp;(₹)</b> </span> </td>
            <?php if($invoiceType=="Purchase Invoice"){?>
                <td colspan="1" > </span><span class="right">/-</span><span class="right"  id="cashReceived" > <?php if($masterInfo['vendorType']=="Customer"){?><?=$masterInfo['amount']?><?php }else{ ?><?=$masterInfo['totalAmount']?><?php }?></td>
            <?php } else{ ?>
                <td colspan="1" > </span><span class="right">/-</span><span class="right"  id="cashReceived" > <?=$masterInfo['totalAmount']?></td>
            <?php } ?>
        </tr>
    </table>
<br>
<br>
<br>
<br>
Thank You Visit Again...
<!-- header_data -->


  
</body>
<script src="<?=asset_url();?>/libs/jquery/dist/jquery.min.js"></script>
<script>

    function convertNumberToWords() {
    var amount= document.getElementById('cashReceived').innerHTML;
    // alert(amount);
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    // return words_string;
    // document.getElementById('rupeeWord').innerHTML="In Ruppes :"+words_string+"Rupees Only";
        $('#rupeeWord').html("In Ruppes :"+words_string+"Rupees Only");
}
</script>
<script type="text/javascript">
$('#cashReceived').css('font-weight','bold');
$(document).keypress(function(e) {
  if(e.which == 13) {
    window.close();
  }
});

$('body').ready(function () {
    var invoiceType= "<?= $invoiceType ?>";
    if(invoiceType==="Purchase Invoice")
    {
        var vendorType=<?php if(array_key_exists("vendorType",$masterInfo)){ ?>"<?=$masterInfo['vendorType']?>";<?php } else { ?> "";<?php } ?>
        var isWithoutTax="<?=$_SESSION['isWithoutTax']?>";
        if(vendorType==="Customer"||isWithoutTax==="true")
        {
            $('.itemTable').find('th:nth-child(2)').attr('colspan','3');
            $('.itemTable').find('td:nth-child(2)').attr('colspan','3');
        }
    }

    if(invoiceType==="estimate" || invoiceType==="Purchase Invoice")
    {
       $('#lastRowForPurchase').css('font-size','16px');
    }

    convertNumberToWords();
    window.print();
});

</script>

</html>