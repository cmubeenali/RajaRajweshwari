<?php $this->load->view('Header/header.php'); ?>
        <div class="container-fluid">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Product </h4>
            
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab">List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab">Add New</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" role="tabpanel">
                    <br>
                        <div class="table-responsive table-hover">
                            <table id="data-table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Purity</th>
                                        <th>Product Name</th>
                                        <th>Unit</th>
                                        <th>Group</th>
                                        <th>Purchase Rate</th>
                                        <th>Sales Rate</th>
                                        <th>Opening Stock</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach ($product as $key) { ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $key->productCode; ?></td>
                                        <td><?= $key->productName; ?></td>
                                        <td><?= $key->unitName; ?></td>
                                        <td><?= $key->productGroupName; ?></td>
                                        <td><?= $key->purchaseRate; ?></td>
                                        <td><?= $key->retailRate; ?></td>
                                        <td><?= $key->openingStock; ?></td>
                                        <td><i class="mdi mdi-pencil " onclick="editProduct(<?= $key->productId; ?>)"> </i>/<i class="mdi mdi-delete" onclick="delProduct(<?= $key->productId; ?>)">  </i></td>
                                    </tr>
                                    <?php $i++; } ?>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        
                        <div class="content__inner content__inner--sm">
                            <div class="card new-contact">
                                <div class="new-contact__header">
                                    
                                    
                                    <form action="<?php echo site_url('masters/addnewProducts'); ?>"  method="POST"   >

                                        
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Purity</label>
                                                    <input type="text"  class="form-control" name="Productcode">
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" name="Productname" required class="form-control">
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Group</label>
                                                    <select class="select2" name="Group" id="grpSelect">
                                                        <?php
                                                        foreach ($productgroup as $keygrp ) { ?>
                                                        <option  value="<?= $keygrp->productGroupId; ?>" > <?= $keygrp->productGroupName; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Unit</label>
                                                    <select class="select2" name="Unit">
                                                        <?php
                                                        foreach ($unit as $keyunit ) { ?>
                                                        <option value="<?= $keyunit->unitId; ?>" > <?= $keyunit->unitName; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>HSN/SAC</label>
                                                    <input type="text" class="form-control" name="HSNSAC" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>GST Rate</label>
                                                    <input type="text" class="form-control" name="GST" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Purchase Rate</label>
                                                    <input type="number" class="form-control" name="pr" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Wholesale Rate</label>
                                                    <input type="number" class="form-control" name="ws" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Retail Rate</label>
                                                    <input type="number" class="form-control" name="rr" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Consumer Rate</label>
                                                    <input type="number" class="form-control" name="cr" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>MRP</label>
                                                    <input type="number" name="mrp" class="form-control"  >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Opening Stock</label>
                                                    <input type="number" n class="form-control" name="os" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Reorder Level</label>
                                                    <input type="number" class="form-control" name="rl" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control textarea-autosize" name="desc" ></textarea>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        <div class="mt-5 text-center">
                                            <button type="submit" id="submitnew"  class="btn btn-primary">Save </button>
                                            <!-- <a href="#" class="btn btn-light">Save new contact</a> -->
                                            <a href="<?= site_url('masters/Products') ?>" class="btn btn-light">Clear</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->
    <div class="modal fade" id="modal-default" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Edit Product</h5>
                </div>
                <div class="modal-body">
                    <?= form_open('masters/editPostdataproduct'); ?>
                    <input type="hidden" id="hiddenproductvalue" value="" name="hiddenproductvalue">
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Product Code<h6>
                            <div class="form-group">
                                <input type="text" id="editProductcode"  name="editProductcode" value="" class="form-control"  >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Product Name<h6>
                            <div class="form-group">
                                <input type="text" id="editProductname" name="editProductname" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Group<h6>
                            <div class="form-group">
                                <select id="editGroup" name="editGroup"  class=" form-control" >
                                    <?php foreach ($productgroup as $keygrp) { ?>
                                    <option  value="<?= $keygrp->productGroupId ?>"><?= $keygrp->productGroupName; ?></option>
                                    <?php } ?>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Unit<h6>
                            <div class="form-group">
                                <select id="editUnit" name="editUnit"  class=" form-control" >
                                    <?php foreach ($unit as $keyunit) { ?>
                                    <option  value="<?= $keyunit->unitId; ?>"><?= $keyunit->unitName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>HSN/SAC<h6>
                            <div class="form-group">
                                <input type="text" id="editHSNSAC" name="editHSNSAC" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>GST<h6>
                            <div class="form-group">
                                <input type="text" id="editGST" name="editGST" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Purchase Rate<h6>
                            <div class="form-group">
                                <input type="text" id="editpr" name="editpr" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Wholesale Rate<h6>
                            <div class="form-group">
                                <input type="text" id="editws" name="editws" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Retail Rate <h6>
                            <div class="form-group">
                                <input type="text" id="editrr" name="editrr" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Consumer Rate<h6>
                            <div class="form-group">
                                <input type="text" id="editcr" name="editcr" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>MRP<h6>
                            <div class="form-group">
                                <input type="text" id="editmrp" name="editmrp" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Opening Stock<h6>
                            <div class="form-group">
                                <input type="text" id="editos" name="editos" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Reorder level<h6>
                            <div class="form-group">
                                <input type="text" id="editrl" name="editrl" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6>Description<h6>
                            <div class="form-group">
                                <input type="text" id="editdesc" name="editdesc" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-link" >Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->

    </div>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script type="text/javascript">
$(document).ready(function () {
    $('#data-table_length select option').css('background-color', '#020203');
    $('#data-table').DataTable();
});
// Warning Message

</script>
<script type="text/javascript">
function clearfun() {
    $('#addnew input[type="text"] ').val("");
    $('#addnew #desc ').val("");
}
</script>
<script type="text/javascript">
function delProduct(id) {
    // Warning Message with function
    swal({
        title: 'Are you sure?',
        text: 'This Product will be deleted!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'rgb(221, 51, 51)',
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonClass: 'swal2-cancel swal2-styled'
    }).then((result)=>{
        if (result.value) {
            cnfrmDel();
            swal({
                timer: 800,
                title: 'Deleted!',
                text: '',
                type: 'success',
                showConfirmButton: false
            });
            setTimeout(function () {
                location.reload();
            }, 800);
        }
    });

    function cnfrmDel() {
        $.ajax({
            data: {
                id: id
            },
            url: '<?= site_url('masters/delProduct'); ?>',
            method: 'post',
            datatype: 'json',
            success: function (response) {}
        })
    }
}

function editProduct(id) {
    $('#modal-default').modal();
    $('#hiddenproductvalue').val(id);
    $.ajax({
        data: {
            id: id
        },
        url: '<?= site_url('masters/editProductfetchdata') ?>',
        datatype: 'json',
        method: 'post',
        success: function (response) {
            var data = response.split('/#/');
            $('#editProductcode').val(data[0]);
            $('#editProductname').val(data[1]);
            $('#editGroup option[value="' + data[2] + '"').attr('selected', 'selected');
            $('#editUnit option[value="' + data[3] + '"').attr('selected', 'selected');
            $('#editHSNSAC').val(data[4]);
            $('#editGST').val(data[5]);
            $('#editpr').val(data[6]);
            $('#editws').val(data[7]);
            $('#editrr').val(data[8]);
            $('#editcr').val(data[9]);
            $('#editmrp').val(data[10]);
            $('#editos').val(data[11]);
            $('#editrl').val(data[12]);
            $('#editdesc').val(data[13]);
        }
    });
}
</script>

