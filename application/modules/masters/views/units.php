<?php $this->load->view('Header/header.php');
?>
 <div class="container-fluid col-md-10">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Unit</h4>
            
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab">List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab">Add New</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" role="tabpanel">
                        <div class="table-hover table-sm mb-0">
                            <table id="data-table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Unit</th>
                                        <th>Description</th>
                                        <th>Edit </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach ($units as $key) { ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $key->unitName; ?></td>
                                        <td><?= $key->description; ?></td>
                                        <td><i class="mdi mdi-pencil " onclick="editUnit(<?= $key->unitId; ?>)"> </i>/<i class="mdi mdi-delete" onclick="delUnit(<?= $key->unitId; ?>)">  </i></td>
                                    </tr>
                                    <?php $i++; } ?>
                                    
                                    
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        
                        <div class="card-body">
                            
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Unit</label>
                                    
                                    <div class="form-group">
                                        <input type="text" id="unit" class="form-control"  required="required">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Description</label>                                    
                                    <div class="form-group">
                                        <input type="text" id="desc" class="form-control"  required='required'>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="btn-demo col-md-6">
                                    <button type="submit"  class="btn btn-primary" id="sa-success" >Save</button>
                                    <button type="" class="btn btn-light" onclick="clearfun()">Clear</button>
                                </div>
                                
                                
                                
                            </div>
                            <br>
                            
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->
    <div class="modal fade" id="modal-default" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Edit Unit</h5>
                </div>
                <div class="modal-body">
                    <?= form_open('masters/editPostdata'); ?>
                    <input type="hidden" id="hiddenunitvalue" value="" name="hiddenunitvalue">
                    <div class="col-md-6">
                        <label class="control-label">Unit</label>                                    
                        <div class="form-group">
                            <input type="text" id="unitEdit"  name="editunitpost" value="" class="form-control"  >
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Description</label>                            
                        <div class="form-group">
                            <input type="text" id="descEdit" name="editdescpost" value="" class="form-control" >
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-link" >Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->
</div>
<?php $this->load->view('Header/footer.php'); ?>
<!-- Javascript -->

<script type="text/javascript">
   $(document).ready(function(){
    $('#data-table_filter').remove();
    $('#data-table_length').remove();
    $('#data-table_info').remove();
    $('#data-table_paginate').remove();
    $('.dataTables_buttons').remove();
   });
   toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        } 
// Warning Message
$('#sa-success').click(function () {
    if ($('#unit').val() == "") {
        toastr["warning"]("Please enter valid unit.");
        $('#unit').focus();
    } else {
        var desc = $('#desc').val();
        var unit = $('#unit').val();
        $.ajax({
            method: 'post',
            url: '<?= site_url('masters/addUnits') ?>',
            data: {
                unit: unit,
                desc: desc
            },
            datatype: 'json',
            success: function (response) {
                swal({
                    title: 'New Unit Added!',
                    showConfirmButton: false,
                    text: '',
                    type: 'success',
                    showConfirmButton:false
                });
                setTimeout(function () {
                    location.reload();
                }, 800);
            }
        });
    }
}); 
</script> 
<script type = "text/javascript" >
    function clearfun() {
        $('#desc').val('');
        $('#unit').val('');
    } 
</script> 
<script type = "text/javascript" >
    function delUnit(id) {
        // Warning Message with function
        swal({
            title: 'Are you sure?',
            text: 'This unit will be deleted!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'rgb(221, 51, 51)',
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonClass: 'swal2-cancel swal2-styled'    			
        }).then((result)=>{
            if(result.value){
                cnfrmDel();
                setTimeout(function () {
                    location.reload();
                }, 800);
            }
        });

        function cnfrmDel() {
            $.ajax({
                data: {
                    id: id
                },
                url: '<?= site_url('masters/delUnit'); ?>',
                method: 'post',
                datatype: 'json',
                success: function (response) {
                    swal({
                        timer: 800,
                        title: 'Deleted!',
                        text: '',
                        type: 'success',
                        showConfirmButton: false
                    });
                }
            })
        }
    }

function editUnit(id) {
    $('#modal-default').modal();
    $('#hiddenunitvalue').val(id);
    $.ajax({
        data: {
            id: id
        },
        url: '<?= site_url('masters/editUnit') ?>',
        datatype: 'json',
        method: 'post',
        success: function (response) {
            var data = response.split('/');
            $('#unitEdit').val(data[0]);
            $('#descEdit').val(data[1]);
        }
    });
}
</script>
