<?php $this->load->view('Header/header.php'); ?>
        <div class="container-fluid">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Product </h4>
            
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab">List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab">Add New</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" role="tabpanel">
                    <br>
                        <div class="table-responsive table-hover">
                            <table id="data-table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>City</th>
                                        <th>Type</th>
                                        <th>Opening Balance</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach ($contacts as $key) { ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $key->contactName; ?></td>
                                        <td><?= $key->mobile; ?></td>
                                        <td><?= $key->city; ?></td>
                                        <td><?= $key->type; ?></td>
                                        <td><?= $key->openingBalance; ?></td>
                                        <td><i class="mdi mdi-pencil " onclick="editContact(<?= $key->contactId; ?>)"> </i>/<i class="mdi mdi-delete " onclick="delContact(<?= $key->contactId; ?>)">  </i></td>
                                    </tr>
                                    <?php $i++; } ?>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        <div class="content__inner content__inner--sm">
                            <div class="card new-contact">
                                <!-- form -->
                                
                                <div class="new-contact__header">
                                    <form action="<?php echo site_url('masters/addnewContact'); ?>"  method="POST"   >
                                    </div>
                                <input type="hidden" name="isFromContact" value="true">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Contact Name</label>
                                                    <input type="text" class="form-control"  name="name">
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input type="text" name="add" class="form-control" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input type="text" name="city" class="form-control" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <input type="text" name="state" class="form-control" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>State Code</label>
                                                    <input type="text" class="form-control" name="sc" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Land Phone</label>
                                                    <input type="number" class="form-control" name="land" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Mobile</label>
                                                    <input type="tel" class="form-control" name="mobile" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" class="form-control" name="email" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DL Number</label>
                                                    <input type="number" class="form-control" name="dlNumber" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Credit Period</label>
                                                    <input type="number" class="form-control" name="creditPeriod" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>GSTIN</label>
                                                    <input type="text" n class="form-control" name="gst" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <input type="text" name="desc" class="form-control"  >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Opening Balance</label>
                                                    <input type="number" class="form-control" name="op" >
                                                    <i class="form-group__bar"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-6 eklavyaradiodown">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="btn-group" data-toggle="buttons">
                                                            <label class="btn btn-light">
                                                                <input type="radio" name="crordr" value="Cr"   autocomplete="off"> Credit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </label>
                                                            <label class="btn btn-light">
                                                                <input type="radio" name="crordr" value="Dr"  autocomplete="off"> Debit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <div class="col-md-3">
                                                        <div class="btn-group" data-toggle="buttons">
                                                            <label class="btn btn-light">
                                                                <input type="radio" name="type" value="Supplier"   autocomplete="off"> Supplier&nbsp;
                                                            </label>
                                                            <label class="btn btn-light">
                                                                <input type="radio" name="type" value="Customer"  autocomplete="off"> Customer
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="clearfix"></div>
                                        <div class="mt-5 text-center">
                                            <button type="submit" name="submit" id="save"  class="btn btn-primary">Save </button>
                                            <!-- <a href="#" class="btn btn-light">Save new contact</a> -->
                                            <a href="<?= site_url('masters/Contacts') ?>" class="btn btn-light">Clear</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    <!-- Default Modal strat-->
    <div class="modal fade" id="modal-default" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Edit Contact</h5>
                </div>
                <div class="modal-body">
                    <?= form_open('masters/editPostdatacontact'); ?>
                    <input type="hidden" id="hiddencontactvalue" value="" name="hiddencontactvalue">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Contact Name</label>
                            <div class="form-group">
                                <input type="text" id="editname"  name="editname" value="" required="required" class="form-control"  >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Address</label>
                            <div class="form-group">
                                <input type="text" id="editadd" name="editadd" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">City</label>
                            <div class="form-group">
                                <input type="text" id="editcity" name="editcity" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <label class="control-label">State</label>                            
                            <div class="form-group">
                                <input type="text" id="editstate" name="editstate" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">State Code</label>
                            <div class="form-group">
                                <input type="text" id="editsc" name="editsc" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Land Phone</label>
                            <div class="form-group">
                                <input type="text" id="editland" name="editland" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Mobile</label>
                            <div class="form-group">
                                <input type="text" id="editmobile" name="editmobile" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Email</label>
                            <div class="form-group">
                                <input type="text" id="editemail" name="editemail" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Dl Number</label>
                            <div class="form-group">
                                <input type="text" id="dlNumber" name="dlNumber" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Credit Period</label>
                            <div class="form-group">
                                <input type="text" id="creditPeriod" name="creditPeriod" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">GSTIN </label>                            
                            <div class="form-group">
                                <input type="text" id="editgst" name="editgst" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Description</label>
                            <div class="form-group">
                                <input type="text" id="description" name="description" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Opening Balance</label>
                            <div class="form-group">
                                <input type="text" id="editop" name="editop" value="" class="form-control" >
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-light">
                                    <input type="radio" name="crordr" value="Cr" id="creditradiomodal"  autocomplete="off"> Credit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
                                <label class="btn btn-light">
                                    <input type="radio" name="crordr" value="Dr" id="debitradiomodal" autocomplete="off"> Debit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
                            </div>
                            <div>&nbsp;</div>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-light">
                                    <input type="radio" name="type" value="Supplier" id="supplierradiomodal" autocomplete="off">Supplier
                                </label>
                                <label class="btn btn-light">
                                    <input type="radio" name="type" value="Customer" id="customerradiomodal"  autocomplete="off" >Customer
                                </label>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-link" >Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
    
    <!-- Default Modal strat-->

</div>
<?php $this->load->view('Header/footer.php'); ?>

<!-- Javascript -->


<script type="text/javascript">
$(document).ready(function () {
    $('#data-table_length select option').css('background-color', '#020203');
    $('#data-table').DataTable();
});
// Warning Message

</script>
<script type="text/javascript">
function clearfun() {
    $('#desc').val('');
    $('#groupname').val('');
}
</script>
<script type="text/javascript">
function delContact(id) {
    // Warning Message with function
    swal({
        title: 'Are you sure?',
        text: 'This Contact will be deleted!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'rgb(221, 51, 51)',
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonClass: 'swal2-cancel swal2-styled'    			
    }).then((result)=>{
        if(result.value){
            cnfrmDel();
        }
    });

    function cnfrmDel() {
        $.ajax({
            data: {
                id: id
            },
            url: '<?= site_url('masters/delContact'); ?>',
            method: 'post',
            datatype: 'json',
            success: function (response) {
                if (response == "success") {
                    swal({
                        timer: 1000,
                        title: 'Deleted!',
                        text: '',
                        type: 'success',
                        showConfirmButton: false
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 800);
                } else if (response == "reference") {
                    swal({
                        timer: 1000,
                        title: 'Reference Occured!',
                        text: 'Cannot delete this record, reference exists!',
                        type: 'error',
                        showConfirmButton: false
                    });
                }
            }
        })
    }
}

function editContact(id) {
    $('#modal-default').modal();
    $('#hiddencontactvalue').val(id);
    $.ajax({
        data: {
            id: id
        },
        url: '<?= site_url('masters/editContactfetchdata') ?>',
        datatype: 'json',
        method: 'post',
        success: function (response) {
            var data = $.parseJSON(response);
            $('#editname').val(data[0]['contactName']);
            $('#editadd').val(data[0]['address']);
            $('#editcity').val(data[0]['city']);
            $('#editstate').val(data[0]['state']);
            $('#editsc').val(data[0]['stateCode']);
            $('#editland').val(data[0]['phoneNumber']);
            $('#editmobile').val(data[0]['mobile']);
            $('#editemail').val(data[0]['emailId']);
            $('#dlNumber').val(data[0]['dlNumber']);
            $('#creditPeriod').val(data[0]['creditPeriod']);
            $('#editgst').val(data[0]['gstIn']);
            $('#description').val(data[0]['description']);
            $('#editop').val(data[0]['openingBalance']);
            var crordr = data[0]['CrOrDr'];
            if (!crordr == "") {
                if (crordr == "Cr") {
                    $('#creditradiomodal').click()
                } else if (crordr == "Dr") { //type==dr
                    $('#debitradiomodal').click();
                }
            }
            var type = data[0]['type'];
            if (!type == "") {
                if (type == "Supplier") {
                    $('#supplierradiomodal').click();
                } else if (type == "Customer") {
                    $('#customerradiomodal').click();
                }
            }
        }
    });
}
</script>

