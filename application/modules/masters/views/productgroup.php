<?php $this->load->view('Header/header.php'); ?>
        <div class="container-fluid col-md-10">
    <div class="card ">
        <div class="card-body">
            <h4 class="card-title">Product Group</h4>
            
            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#addnew" role="tab">Add New</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#list" role="tab">List</a>
                    </li>                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="list" role="tabpanel">
                        <br>
                        <div class="table-responsive table-hover">
                            <table id="data-table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Description</th>
                                        <th>Edit</th>
                                   </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i=1;
                                foreach ($productdata as $key) { ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $key->productGroupName; ?></td>
                                    <td><?= $key->description; ?></td>
                                    <td><i class="mdi mdi-pencil " onclick="editProductGroup(<?= $key->productGroupId; ?>)"> </i>/<i class="mdi mdi-delete " onclick="delProductGroup(<?= $key->productGroupId; ?>)">  </i></td>
                                </tr>
                                <?php $i++; } ?>
                                
                                
                            </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addnew" role="tabpanel">
                        
                        <div class="card-body">
                            
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Group Name</label>
                                    <div class="form-group">
                                        <input type="text" id="groupname" class="form-control"  required="required">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Description</label>
                                    <div class="form-group">
                                        <input type="text" id="desc" class="form-control"  required='required'>
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="btn-demo col-md-6">
                                    <button type="submit"  class="btn btn-primary" id="sa-success">Save</button>
                                    <button type="" class="btn btn-light" onclick="clearfun()">Clear</button>
                                </div>
                            </div>
                            <br>
                            
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->
    <div class="modal fade" id="modal-default" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Edit Product</h5>
                </div>
                <div class="modal-body">
                    <?= form_open('masters/editPostdataproductgroup'); ?>
                    <input type="hidden" id="hiddenproductvalue" value="" name="hiddenproductvalue">
                    <div class="col-md-6"> 
                        <label class="control-label">Product Group</label>
                        <div class="form-group">
                            <input type="text" id="nameEdit"  name="editproductpost" value="" class="form-control"  >
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Description</label>
                        <div class="form-group">
                            <input type="text" id="descEdit" name="editdescpost" value="" class="form-control" >
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-link" >Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
    <!-- Default Modal strat-->
</div>
<?php $this->load->view('Header/footer.php'); ?>

<!-- Javascript -->

<script type="text/javascript">
$(document).ready(function () {
    $('.dataTables_buttons ').remove();
    $('#data-table_length select option').css('background-color', '#020203');
});
// Warning Message
$('#sa-success').click(function () {
    if ($('#groupname').val() == "") {
        toastr["warning"]("Please Enter Valid Group Name.");
        $('#groupname').focus();
    } else {
        var desc = $('#desc').val();
        var groupname = $('#groupname').val();
        $.ajax({
            method: 'post',
            url: '<?= site_url('masters/addProductGroup') ?>',
            data: {
                groupname: groupname,
                desc: desc
            },
            datatype: 'json',
            success: function (response) {
                swal({
                    title: 'New Product Group Added!',
                    showConfirmButton: false,
                    text: '',
                    type: 'success',
                    showConfirmButton: false
                });
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
        });
    }
});
</script>
<script type="text/javascript">
function clearfun() {
    $('#desc').val('');
    $('#groupname').val('');
}
</script>
<script type="text/javascript">
function delProductGroup(id) {
    // Warning Message with function
    swal({
        title: 'Are you sure?',
        text: 'This unit will be deleted!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'rgb(221, 51, 51)',
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonClass: 'swal2-cancel swal2-styled'
    }).then((result) => {
        if (result.value) {
            cnfrmDel();
            swal({
                title: 'Product Group',
                text: 'Deleted',
                type: 'success',
                showConfirmButton: false
            });
            setTimeout(function () {
                location.reload();
            }, 1000);
        }
    });

    function cnfrmDel() {
        $.ajax({
            data: {
                id: id
            },
            url: '<?= site_url('masters/delProductGroup'); ?>',
            method: 'post',
            datatype: 'json',
            success: function (response) {}
        })
    }
}

function editProductGroup(id) {
    $('#modal-default').modal();
    $('#hiddenproductvalue').val(id);
    $.ajax({
        data: {
            id: id
        },
        url: '<?= site_url('masters/editProductGroupfetchdata') ?>',
        datatype: 'json',
        method: 'post',
        success: function (response) {
            var data = response.split('/');
            $('#nameEdit').val(data[0]);
            $('#descEdit').val(data[1]);
        }
    });
}


</script>
