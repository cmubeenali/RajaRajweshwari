<?php $this->load->view('Header/header.php'); ?>
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- ============================================================== -->
                <!-- Info Box -->
                <!-- ============================================================== -->
                <div class="card-body border-top">
                    <div class="row m-b-0">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="d-flex align-items-center">
<!--                                <div class="m-r-10"><span class="text-primary display-5"><i-->
<!--                                                class="mdi mdi-currency-inr"></i></span></div>-->
                                <div><span style="font-size: 16px;">Gold Rate in <b>Gram</b></span>
                                    <a href="#" id="goldRateGram" alt="default" data-toggle="modal"
                                       data-target="#responsive-modal" class="model_img img-fluid"><h3
                                                class="font-medium m-b-0">₹ <?= $gramRate ?></h3></a>
                                </div>
                            </div>
                        </div>
                        <!-- col -->
                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="d-flex align-items-center">
<!--                                <div class="m-r-10"><span class="text-primary display-5"><i-->
<!--                                                class="mdi mdi-currency-inr"></i></span></div>-->
                                <div><span style="font-size: 16px;">Gold Rate in <b>Pavan</b></span>
                                    <a href="#" id="goldRatePavan" alt="default" data-toggle="modal"
                                       data-target="#responsive-modal" class="model_img img-fluid"><h3
                                                class="font-medium m-b-0">₹ <?= $pavanRate ?></h3></a>
                                </div>
                            </div>
                        </div>
                        <!-- col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="col-md-4">
            <!-- sample modal content -->
            <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Change Gold Rate </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <input type="hidden" name="unit" id="unit" value="">
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="goldRate" class="control-label">Rate Today</label>
                                    <input type="number" class="form-control" id="goldRate">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSaveChanges" class="btn btn-default waves-effect waves-light">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <?php $this->load->view('Header/footer.php'); ?>

    <script>
        $('#goldRateGram').click(function () {
            $('#goldRate').val(<?= $gramRate ?>);
            $('.modal-title').html('Change Gold Rate (Gram)');
            $('#unit').val('Gram');
        });
        $('#goldRatePavan').click(function () {
            $('#goldRate').val(<?= $pavanRate ?>);
            $('.modal-title').html('Change Gold Rate (Pavan)');
            $('#unit').val('Pavan');
        });
        $('#btnSaveChanges').click(function () {
            $.post('<?=site_url('common/ChangeGoldRate') ?>', {
                rate: $('#goldRate').val(),
                unit: $('#unit').val()
            }, function () {
                swal({
                    title: 'Gold Rate Changed',
                    text: 'Today\'s gold rate for ' + $('#unit').val() + ' has been changed successfully!',
                    type: 'success',
                    showConfirmButton: false
                });
                setTimeout(function () {
                    location.reload();
                }, 3000);
            });
        });
    </script>
