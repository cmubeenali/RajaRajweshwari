<footer class="footer text-center">
<!--      Powerd By <a href="https://github.com/Benjith/Dakshayaniyamma-Biscuits"></a>.-->
</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    
    <!-- <div class="chat-windows"></div> -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=asset_url();?>/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=asset_url();?>/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=asset_url();?>/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="<?=asset_url();?>/js/app.min.js"></script>
    <script src="<?=asset_url();?>/js/app.init.horizontal.js"></script>
    <script src="<?=asset_url();?>/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=asset_url();?>/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?=asset_url();?>/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?=asset_url();?>/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=asset_url();?>/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=asset_url();?>/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <!-- <script src="<?=asset_url();?>/libs/chartist/dist/chartist.min.js"></script> -->
    <!-- <script src="<?=asset_url();?>/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script> -->
    <!--c3 charts -->
    <script src="<?=asset_url();?>/extra-libs/c3/d3.min.js"></script>
    <script src="<?=asset_url();?>extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="<?=asset_url();?>libs/chart.js/dist/Chart.min.js"></script>

    <!-- Select2 -->
    <script src="<?= asset_url(); ?>/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= asset_url(); ?>/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?= asset_url(); ?>/js/pages/forms/select2/select2.init.js"></script>
    <script src="<?= asset_url(); ?>/libs/toastr/build/toastr.min.js"></script>
    <script src="<?= asset_url(); ?>/extra-libs/toastr/toastr-init.js"></script>
    <script src="<?= asset_url(); ?>/libs/swal2/dist/sweetalert2.all.min.js"></script>
    <script src="<?= asset_url(); ?>/libs/swal2/dist/sweetalert2.min.js"></script>
    <script src="<?= asset_url(); ?>/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?= asset_url(); ?>/js/pages/datatable/datatable-basic.init.js"></script>
    <script src="<?= asset_url(); ?>/libs/moment_new/moment.js"></script>
    <!-- <script src="<?= asset_url(); ?>/libs/flatpickr/dist/flatpickr.min.js"></script> -->
    <script src="<?= asset_url(); ?>/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- start - This is for export functionality only -->
<script src="<?= asset_url(); ?>/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="<?= asset_url(); ?>/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="<?= asset_url(); ?>/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="<?= asset_url(); ?>/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="<?= asset_url(); ?>/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="<?= asset_url(); ?>/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="<?= asset_url(); ?>/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>



    <script>
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>

    
</body>

</html>