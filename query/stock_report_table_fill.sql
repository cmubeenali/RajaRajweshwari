# NOT APPLIED IN THE FRONTEND

select  TEMP2.ProductId,
  product_tbl.productCode as Code,
  product_tbl.productName as Product,
  unit_tbl.unitName AS Unit,
  TEMP2.Inward,
  TEMP2.Outward,
  TEMP2.CurrentStock from(

                           SELECT

                             ProductId,
                             sum(Inward) as Inward,
                             sum(Outward)as Outward,
                             sum(Inward)-sum(Outward) as CurrentStock

                           FROM

                             (SELECT   PM.productId as ProductId,

                                       SUM(PM.qty) as Inward,
                                       0 as Outward
                              FROM purchaseldetails_tbl as PM , product_tbl P where PM.productId=P.productId
                              Group by	PM.productId
                              UNION ALL

                              SELECT SM.productId as ProductId,

                                     0 as Inward,
                                     SUM(SM.qty) as Outward
                              FROM salesdetails_tbl as SM , product_tbl P where SM.productId=P.productId
                              Group by SM.productId
                              UNION ALL

                              SELECT   product_tbl.productId as ProductId,

                                       product_tbl.openingStock as Inward,
                                       0 as Outward
                              FROM product_tbl

                              UNION ALL
                              SELECT R.productId as ProductId,

                                     0 as Inward,
                                     SUM(R.qty) as Outward
                              FROM purchasereturndetails_tbl as R , product_tbl P where R.productId=P.productId
                              Group by R.productId
                              UNION ALL

                              SELECT
                                S.productId as ProductId,

                                SUM(S.qty) as Inward,
                                0 as Outward
                              FROM salesreturndetails_tbl as S, product_tbl P where S.productId=P.productId
                              Group by	S.productId
                             )
                               AS TEMP  Group by ProductId) as TEMP2 left join product_tbl on product_tbl.productId=TEMP2.ProductId left join unit_tbl On product_tbl.unitId=unit_tbl.unitId