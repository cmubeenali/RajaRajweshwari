-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 21, 2018 at 02:43 PM
-- Server version: 10.1.29-MariaDB-6+b1
-- PHP Version: 7.0.29-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eklavya`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `ReturnUnitName` (`mainUnitId` INT, `conversionId` INT) RETURNS VARCHAR(50) CHARSET utf8mb4 begin
    if(conversionId>0)then
      begin
        set @unitName := (select u.unitName from unitconversion_tbl t1 inner join unit_tbl u on t1.conversionUnitId = u.unitId where t1.unitConversionId=conversionId);
        return @unitName;
      end;
    else
        return (select t1.unitName from unit_tbl t1 where t1.unitId=mainUnitId);
    end if;
  end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `unitConversion` (`mainUnit` INT, `conversionId` INT, `qty` DECIMAL(18,5)) RETURNS DECIMAL(18,5) begin
    if (conversionId>0)then
      begin
        set @conversionRate:=(select conversionRate from unitconversion_tbl t1 where t1.unitConversionId=conversionId);
        return qty/@conversionRate;
      end;
    else
      return qty;
    end if;
  end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account_tbl`
--

CREATE TABLE `account_tbl` (
  `accountId` int(11) NOT NULL,
  `accountName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `accountCode` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `accountParentId` int(11) DEFAULT NULL,
  `defaultGroup` tinyint(1) DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8_bin,
  `nature` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `account_tbl`
--

INSERT INTO `account_tbl` (`accountId`, `accountName`, `accountCode`, `accountParentId`, `defaultGroup`, `entryDate`, `description`, `nature`) VALUES
(1, 'Expense', '0', 0, 1, '2015-03-17 11:37:32', NULL, 'Expenses'),
(2, 'Current Liability', '3', 23, 1, '2015-03-17 11:37:32', NULL, 'Liabilities'),
(3, 'Short Term Deposits (Asset)', '9', 49, 1, '2015-03-17 11:37:32', NULL, 'Assets'),
(4, 'Direct Expenses', '1', 1, 1, '2015-03-17 11:37:32', NULL, 'Expenses'),
(5, 'Direct Incomes', '2', 12, 1, '2015-03-17 11:37:32', NULL, 'Income'),
(6, 'Duties & Taxes', '10', 3, 1, '2015-03-17 11:37:32', NULL, 'Liabilities'),
(7, 'Fixed Assets', '4', 34, 1, '2015-03-17 11:37:33', NULL, 'Assets'),
(8, 'Indirect Expenses', '1', 1, 1, '2015-03-17 11:37:33', NULL, 'Expenses'),
(9, 'Indirect Incomes', '2', 12, 1, '2015-03-17 11:37:33', NULL, 'Income'),
(10, 'Investments', '4', 34, 1, '2015-03-17 11:37:33', NULL, 'Assets'),
(11, 'Loans & Advances(Asset)', '4', 34, 1, '2015-03-17 11:37:33', NULL, 'Assets'),
(12, 'Income', '0', 0, 1, '2015-03-17 11:37:33', NULL, 'Income'),
(13, 'Loans & Liability(Liability)', '3', 23, 1, '2015-03-17 11:37:33', NULL, 'Liabilities'),
(14, 'Miscellaneous  Expenses', '4', 34, 1, '2015-03-17 11:37:33', NULL, 'Expenses'),
(15, 'Provisions', '10', 2, 1, '2015-03-17 11:37:33', NULL, 'Liabilities'),
(16, 'Purchase Accounts', '1', 1, 1, '2015-03-17 11:37:33', NULL, 'Expenses'),
(17, 'Reserves & Surplus', '7', 47, 1, '2015-03-17 11:37:33', NULL, 'Liabilities'),
(18, 'Sales Accounts', '2', 12, 1, '2015-03-17 11:37:33', NULL, 'Income'),
(19, 'Secured Loans', '20', 13, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(20, 'Stock In Hand', '9', 49, 1, '2015-03-17 11:37:34', NULL, 'Assets'),
(21, 'Sundry Creditors', '10', 2, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(22, 'Sundry Debtors', '9', 49, 1, '2015-03-17 11:37:34', NULL, 'Assets'),
(23, 'Liability', '0', 0, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(24, 'Suspense Account(Asset)', '4', 34, 1, '2015-03-17 11:37:34', NULL, 'Assets'),
(25, 'Suspense Account(Liability)', '3', 23, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(26, 'Unsecured Loans', '20', 13, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(27, 'Proprietor Current Account', '7', 47, 1, '2015-03-17 11:37:34', NULL, 'Assets'),
(28, 'Chits Or Funds', '18', 10, 1, '2015-03-17 11:37:34', NULL, 'Liabilities'),
(29, 'Accounts Payables', '10', 2, 1, '2015-03-17 11:37:34', NULL, 'Expenses'),
(30, 'Accounts Receivables', '9', 10, 1, '2015-03-17 11:37:34', NULL, 'Income'),
(31, 'Accrued Income', '9', 10, 1, '2015-03-17 11:37:35', NULL, 'Income'),
(32, 'Long Term Deposits(Asset)', '3', 4, 1, '2015-03-17 11:37:35', NULL, 'Assets'),
(33, 'Short Term Loans(Liability)', '10', 2, 1, '2015-03-17 11:37:35', NULL, 'Liabilities'),
(34, 'Assets', '0', 0, 1, '2015-03-17 11:37:35', NULL, 'Assets'),
(35, 'Short Term Loans & Advances(Asset)', '9', 49, 1, '2015-03-17 11:37:35', NULL, 'Liabilities'),
(36, 'P D C Payable', '10', 2, 1, '2015-03-17 11:37:35', NULL, 'Expenses'),
(37, 'P D C Receivable', '9', 49, 1, '2015-03-17 11:37:35', NULL, 'Income'),
(38, 'Vendor', '28', 21, 1, '2015-03-17 11:37:35', NULL, 'Liabilities'),
(39, 'Customer', '29', 22, 1, '2015-03-17 11:37:35', NULL, 'Assets'),
(40, 'Operating Expense', '16', 8, 1, '2015-03-17 11:37:35', NULL, 'Expenses'),
(41, 'Non Operating Expense', '16', 8, 1, '2015-03-17 11:37:35', NULL, 'Expenses'),
(42, 'Additional Cost', '12', 4, 1, '2015-03-17 11:37:35', NULL, 'Expenses'),
(43, 'Production cost', '12', 4, 1, '2015-03-17 11:37:35', NULL, 'Expenses'),
(44, 'Bank Account', '9', 49, 1, '2015-03-17 11:37:35', NULL, 'Assets'),
(45, 'Staff Expenses', '12', 4, 1, '2015-03-17 11:37:36', NULL, 'Expenses'),
(46, 'Bank OD Account', '20', 13, 1, '2015-03-17 11:37:36', NULL, 'Liabilities'),
(47, 'Capital Account', '3', 34, 1, '2015-03-17 11:37:36', NULL, 'Liabilities'),
(48, 'Cash In Hand', '9', 49, 1, '2015-03-17 11:37:36', NULL, 'Assets'),
(49, 'Current Assets', '4', 34, 1, '2015-03-17 11:37:36', NULL, 'Assets'),
(51, 'Partner', '1', 1, 1, '2015-03-17 11:37:32', NULL, 'Expenses');

-- --------------------------------------------------------

--
-- Table structure for table `company_tbl`
--

CREATE TABLE `company_tbl` (
  `companyId` int(11) NOT NULL,
  `companyName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `tinNumber` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `cstNumber` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `address` text COLLATE utf8_bin,
  `phone` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `place` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `pincode` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `printType` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxBill` tinyint(1) DEFAULT NULL,
  `isPreviousBalance` tinyint(1) DEFAULT NULL,
  `gstIn` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `stateCode` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `bankName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `accountNo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `branchAndIfsCode` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `printMethod` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_tbl`
--

INSERT INTO `company_tbl` (`companyId`, `companyName`, `tinNumber`, `cstNumber`, `city`, `address`, `phone`, `email`, `place`, `state`, `pincode`, `entryDate`, `printType`, `taxBill`, `isPreviousBalance`, `gstIn`, `stateCode`, `bankName`, `accountNo`, `branchAndIfsCode`, `printMethod`) VALUES
(1, 'PULARI AGENCIES', '', '', 'MANJERI', 'dfgdf', '+91 95 3939 6121', '', '', '', '', '2018-01-11 16:16:02', 'Dot', 1, 1, '', '', 'dfgdf', '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `contacts_tbl`
--

CREATE TABLE `contacts_tbl` (
  `contactId` int(11) NOT NULL,
  `ledgerId` int(11) DEFAULT NULL,
  `contactName` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `address` text COLLATE utf8_bin,
  `city` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `phoneNumber` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `emailId` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `tinNumber` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `dlNumber` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `creditPeriod` int(11) DEFAULT NULL,
  `state` text COLLATE utf8_bin,
  `stateCode` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `gstIn` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contacts_tbl`
--

INSERT INTO `contacts_tbl` (`contactId`, `ledgerId`, `contactName`, `address`, `city`, `phoneNumber`, `mobile`, `emailId`, `type`, `description`, `tinNumber`, `dlNumber`, `creditPeriod`, `state`, `stateCode`, `gstIn`, `image`) VALUES
(2, 21, 'Saiju', '', '', '', '', '', 'Customer', NULL, NULL, '', 0, '', '', '', '778-3f8f44fb838e5e631ecd30c2147680fe.jpg'),
(3, 22, 'DAVOOD IBRAHIM', '', '', '', '', '', 'Supplier', NULL, NULL, '', 0, '', '', '', '1486-passport_size_400x400.jpg'),
(4, 23, 'Dharmendar Mehta', '', 'Deradoon', '', '', '', 'Customer', NULL, NULL, '', 0, 'UP', '', '', NULL),
(5, 24, 'Dudu', '', '', '', '', '', 'Customer', NULL, NULL, '', 0, '', '', '', NULL),
(6, 25, 'Tovino', '', '', '', '', '', 'Customer', NULL, NULL, '', 0, '', '', '', NULL),
(7, 26, 'BILLY', '', 'GOA', '', '', '', 'Customer', NULL, NULL, '', 0, '', '', '', NULL),
(8, 27, 'Javed', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 28, 'Rafid', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 29, 'Jobh', '', '', '', '', '', 'Customer', NULL, NULL, '', 0, '', '', '', NULL),
(15, 34, 'Pathrose', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(16, 35, 'Rajiv', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(17, 36, 'Moron', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(18, 37, 'Floyd', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(19, 38, 'Johnny', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(20, 39, 'Gaithonde', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(21, 40, 'Sartaj', '', '', '', '', '', NULL, '', NULL, '', 0, '', '', '', NULL),
(22, 41, 'Parulkar', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(23, 42, 'Merry', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(24, 43, 'Joyce', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(25, 44, 'James', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 45, 'Lijith', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(27, 46, 'Mubeen', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 47, 'Avon', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 48, 'Django', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 49, 'Arnol', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 50, 'Pappa Clarence', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 51, 'Clara', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 52, 'Dolores', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 53, 'Lee Sizemore', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 54, 'Hector', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 55, 'Maeve', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 57, 'Stubbs', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '', NULL),
(39, 58, 'Lores', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '594541521Z', NULL),
(40, 59, 'Robert', '', '', '', '', '', 'Customer', '', NULL, '', 0, '', '', '95215848421Z', NULL),
(41, 60, 'Rajkumar', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 61, 'Manoj', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 62, 'Pramod', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 63, 'Anu', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 64, 'Pillayachan', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 65, 'Sreeja', NULL, NULL, NULL, NULL, NULL, 'Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contradetails_tbl`
--

CREATE TABLE `contradetails_tbl` (
  `contraDetailsId` int(11) NOT NULL,
  `contraMasterId` int(11) DEFAULT NULL,
  `ledgerId` int(11) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `chequeOrDD` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `narration` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `contramaster_tbl`
--

CREATE TABLE `contramaster_tbl` (
  `contraMasterId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `ledgerId` int(11) DEFAULT NULL,
  `DrOrCr` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `designation_tbl`
--

CREATE TABLE `designation_tbl` (
  `designationId` int(11) NOT NULL,
  `designationName` text COLLATE utf8_bin,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `estimatedetails_tbl`
--

CREATE TABLE `estimatedetails_tbl` (
  `estimateDetailsId` int(11) NOT NULL,
  `estimateMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,3) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `estimatemaster_tbl`
--

CREATE TABLE `estimatemaster_tbl` (
  `estimateMasterId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `salesHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `tinNo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxForm` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `salesType` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `financialyear_tbl`
--

CREATE TABLE `financialyear_tbl` (
  `financialYearId` int(11) NOT NULL,
  `fromDate` datetime DEFAULT NULL,
  `toDate` datetime DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `closedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `finPrefix` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `financialyear_tbl`
--

INSERT INTO `financialyear_tbl` (`financialYearId`, `fromDate`, `toDate`, `closed`, `entryDate`, `closedDate`, `userId`, `finPrefix`) VALUES
(1, '2017-04-01 00:00:00', '2018-03-31 00:00:00', 0, '2018-01-11 16:16:03', '2018-01-11 16:16:03', 1, '2018');

-- --------------------------------------------------------

--
-- Table structure for table `journaldetails_tbl`
--

CREATE TABLE `journaldetails_tbl` (
  `journalDetailsId` int(11) NOT NULL,
  `journalMasterId` int(11) DEFAULT NULL,
  `ledgerId` int(11) DEFAULT NULL,
  `Dr` decimal(19,4) DEFAULT NULL,
  `Cr` decimal(19,4) DEFAULT NULL,
  `chequeOrDD` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `narration` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `journalmaster_tbl`
--

CREATE TABLE `journalmaster_tbl` (
  `journalMasterId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ledger_tbl`
--

CREATE TABLE `ledger_tbl` (
  `ledgerId` int(11) NOT NULL,
  `accountId` int(11) DEFAULT NULL,
  `ledgerName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ledgerDate` datetime DEFAULT NULL,
  `openingBalance` decimal(19,4) DEFAULT NULL,
  `CrOrDr` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `ledgerDetails` text COLLATE utf8_bin,
  `defaultLedger` tinyint(1) DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `tableName` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ledger_tbl`
--

INSERT INTO `ledger_tbl` (`ledgerId`, `accountId`, `ledgerName`, `ledgerDate`, `openingBalance`, `CrOrDr`, `ledgerDetails`, `defaultLedger`, `entryDate`, `tableName`, `description`) VALUES
(1, 44, 'Bank', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(2, 8, 'Bank Charges', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(3, 46, 'Bank Overdraft', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(4, 48, 'Cash', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(5, 8, 'Salary & Wages', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(6, 8, 'Office Expenses', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(7, 8, 'Office Rent', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(8, 8, 'Stationery', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(9, 4, 'Telephone Expenses', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:05', 'Default', NULL),
(10, 8, 'Travelling Expenses', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:06', 'Default', NULL),
(11, 8, 'Petrol Expenses', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:06', 'Default', NULL),
(12, 16, 'Purchase A/c', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:06', 'Default', NULL),
(13, 48, 'Petty Cash', NULL, '0.0000', 'Dr', NULL, 1, '2015-03-17 12:14:06', 'Default', NULL),
(14, 21, 'SAI PAPER CUPS', '2018-01-11 16:46:27', '0.0000', 'Dr', '', 0, '2018-01-11 16:46:27', 'Contact', ''),
(15, 18, 'Sales', '2018-01-11 17:04:32', '0.0000', 'Dr', '', 0, '2018-01-11 17:04:32', 'Ledger', ''),
(21, 22, 'Saiju', NULL, '0.0000', 'Dr', NULL, NULL, '2018-06-07 12:05:28', 'contacts_tbl', NULL),
(22, 21, 'DAVOOD IBRAHIM', NULL, '0.0000', 'Dr', NULL, NULL, '2018-06-07 12:16:02', 'contacts_tbl', NULL),
(23, 22, 'Dharmendar Mehta', NULL, '0.0000', NULL, NULL, NULL, '2018-07-13 11:48:23', 'contacts_tbl', NULL),
(24, 22, 'Dudu', NULL, '0.0000', NULL, NULL, NULL, '2018-07-13 15:04:14', 'contacts_tbl', NULL),
(25, 22, 'Tovino', NULL, '0.0000', NULL, NULL, NULL, '2018-07-13 15:04:28', 'contacts_tbl', NULL),
(26, 22, 'BILLY', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-18 18:00:09', 'contacts_tbl', NULL),
(27, 21, 'Javed', NULL, NULL, NULL, NULL, NULL, '2018-07-18 18:05:58', 'contacts_tbl', NULL),
(28, 22, 'Rafid', NULL, NULL, NULL, NULL, NULL, '2018-07-18 18:07:13', 'contacts_tbl', NULL),
(29, 22, 'Jobh', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-27 17:00:09', 'contacts_tbl', NULL),
(34, 22, 'Pathrose', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:34:34', 'contacts_tbl', NULL),
(35, 22, 'Rajiv', NULL, '0.0000', 'Cr', NULL, NULL, '2018-07-28 11:44:39', 'contacts_tbl', NULL),
(36, 22, 'Moron', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:46:06', 'contacts_tbl', NULL),
(37, 22, 'Floyd', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:47:06', 'contacts_tbl', NULL),
(38, 22, 'Johnny', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:47:36', 'contacts_tbl', NULL),
(39, 22, 'Gaithonde', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:49:47', 'contacts_tbl', NULL),
(40, 22, 'Sartaj', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:50:28', 'contacts_tbl', NULL),
(41, 22, 'Parulkar', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:51:34', 'contacts_tbl', NULL),
(42, 22, 'Merry', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 11:56:02', 'contacts_tbl', NULL),
(43, 22, 'Joyce', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 12:57:59', 'contacts_tbl', NULL),
(44, 22, 'James', NULL, NULL, NULL, NULL, NULL, '2018-07-28 13:00:25', 'contacts_tbl', NULL),
(45, 22, 'Lijith', NULL, '0.0000', 'Dr', NULL, NULL, '2018-07-28 13:06:32', 'contacts_tbl', NULL),
(46, 22, 'Mubeen', NULL, NULL, NULL, NULL, NULL, '2018-08-14 14:10:37', 'contacts_tbl', NULL),
(47, 22, 'Avon', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:01:23', 'contacts_tbl', NULL),
(48, 22, 'Django', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:05:09', 'contacts_tbl', NULL),
(49, 22, 'Arnol', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:08:10', 'contacts_tbl', NULL),
(50, 22, 'Pappa Clarence', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:10:25', 'contacts_tbl', NULL),
(51, 22, 'Clara', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:11:07', 'contacts_tbl', NULL),
(52, 22, 'Dolores', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:12:52', 'contacts_tbl', NULL),
(53, 22, 'Lee Sizemore', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:13:48', 'contacts_tbl', NULL),
(54, 21, 'Hector', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:15:03', 'contacts_tbl', NULL),
(55, 21, 'Maeve', NULL, NULL, NULL, NULL, NULL, '2018-08-14 16:16:24', 'contacts_tbl', NULL),
(57, 22, 'Stubbs', NULL, '0.0000', 'Dr', NULL, NULL, '2018-08-14 16:26:02', 'contacts_tbl', NULL),
(58, 22, 'Lores', NULL, '0.0000', 'Dr', NULL, NULL, '2018-08-14 16:28:12', 'contacts_tbl', NULL),
(59, 22, 'Robert', NULL, '0.0000', 'Dr', NULL, NULL, '2018-08-15 00:34:54', 'contacts_tbl', NULL),
(60, 22, 'Rajkumar', NULL, NULL, NULL, NULL, NULL, '2018-08-16 22:00:57', 'contacts_tbl', NULL),
(61, 22, 'Manoj', NULL, NULL, NULL, NULL, NULL, '2018-08-19 12:55:08', 'contacts_tbl', NULL),
(62, 22, 'Pramod', NULL, NULL, NULL, NULL, NULL, '2018-08-19 12:57:45', 'contacts_tbl', NULL),
(63, 22, 'Anu', NULL, NULL, NULL, NULL, NULL, '2018-08-19 13:03:01', 'contacts_tbl', NULL),
(64, 21, 'Pillayachan', NULL, NULL, NULL, NULL, NULL, '2018-08-19 14:12:22', 'contacts_tbl', NULL),
(65, 21, 'Sreeja', NULL, NULL, NULL, NULL, NULL, '2018-08-21 13:00:21', 'contacts_tbl', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mainmodule_tbl`
--

CREATE TABLE `mainmodule_tbl` (
  `moduleId` int(11) NOT NULL,
  `moduleName` varchar(100) NOT NULL,
  `subModule` int(11) NOT NULL COMMENT '0=false 1=true ',
  `status` int(11) NOT NULL COMMENT '0=false 1=true ',
  `userId` int(11) DEFAULT NULL,
  `icon` varchar(200) NOT NULL,
  `orderId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainmodule_tbl`
--

INSERT INTO `mainmodule_tbl` (`moduleId`, `moduleName`, `subModule`, `status`, `userId`, `icon`, `orderId`) VALUES
(1, 'General', 1, 1, 1, 'mdi mdi-settings', 1),
(2, 'Masters', 1, 1, 1, 'mdi mdi-account-multiple-plus', 2),
(3, 'Transactions', 1, 1, 1, 'mdi mdi-cash-multiple', 3),
(4, 'Reports', 1, 1, 1, 'mdi mdi-box-cutter', 4),
(5, 'Financial Statements', 0, 1, 1, 'mdi mdi-animation', 5);

-- --------------------------------------------------------

--
-- Table structure for table `paymentvoucher_tbl`
--

CREATE TABLE `paymentvoucher_tbl` (
  `paymentID` int(11) NOT NULL,
  `prefix` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `VoucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `ledgerFirst` int(11) DEFAULT NULL,
  `ledgerSecond` int(11) DEFAULT NULL,
  `total` decimal(19,4) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `paymentvoucher_tbl`
--

INSERT INTO `paymentvoucher_tbl` (`paymentID`, `prefix`, `VoucherNo`, `voucherDate`, `ledgerFirst`, `ledgerSecond`, `total`, `description`, `entryDate`, `userId`, `financialYearId`) VALUES
(6, 'PV', 1, '2018-07-18 00:00:00', 4, 22, '125000.0000', 'HUGE AMOUNT', '2018-07-18 01:20:49', 1, 1),
(7, 'PV', 2, '2018-07-18 00:00:00', 4, 22, '155000.0000', 'Davood\'s due is completed', '2018-07-18 16:24:55', 1, 1),
(8, 'PV', 3, '2018-08-15 00:00:00', 4, 59, '67158.0000', '', '2018-08-15 16:43:30', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pdcpayable_tbl`
--

CREATE TABLE `pdcpayable_tbl` (
  `pdcPayableId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `ledgerCustId` int(11) DEFAULT NULL,
  `ledgerBankId` int(11) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `chequeNo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `chequeDate` date DEFAULT NULL,
  `ClearStatus` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ClearDate` datetime DEFAULT NULL,
  `ClearUserId` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `pdcreceivable_tbl`
--

CREATE TABLE `pdcreceivable_tbl` (
  `pdcReceivableId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `ledgerCustId` int(11) DEFAULT NULL,
  `ledgerBankId` int(11) DEFAULT NULL,
  `amount` decimal(19,4) DEFAULT NULL,
  `chequeNo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `chequeDate` date DEFAULT NULL,
  `ClearStatus` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ClearDate` datetime DEFAULT NULL,
  `ClearUserId` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `productgroup_tbl`
--

CREATE TABLE `productgroup_tbl` (
  `productGroupId` int(11) NOT NULL,
  `productGroupName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `productgroup_tbl`
--

INSERT INTO `productgroup_tbl` (`productGroupId`, `productGroupName`, `Date`, `description`) VALUES
(24, 'Gold', '2018-06-02 18:41:50', 'Gold'),
(25, 'diamond', '2018-06-03 12:58:06', 'diamond');

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `productId` int(11) NOT NULL,
  `productGroupId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `productName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `productCode` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `purchaseRate` decimal(18,2) DEFAULT NULL,
  `wholeSalesRate` decimal(18,2) DEFAULT NULL,
  `retailRate` decimal(18,2) DEFAULT NULL,
  `consumerRate` decimal(18,2) DEFAULT NULL,
  `mrp` decimal(18,2) DEFAULT NULL,
  `openingStock` decimal(18,2) DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8_bin,
  `purTaxPercent` decimal(18,2) DEFAULT NULL,
  `reorderLevel` int(11) DEFAULT NULL,
  `hsnOrSacCode` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`productId`, `productGroupId`, `unitId`, `productName`, `productCode`, `taxType`, `taxPercent`, `purchaseRate`, `wholeSalesRate`, `retailRate`, `consumerRate`, `mrp`, `openingStock`, `entryDate`, `description`, `purTaxPercent`, `reorderLevel`, `hsnOrSacCode`, `image`) VALUES
(292, 24, 9, 'Gold chain', '22k', NULL, '0.00', '18695.00', '12312.00', '22000.00', '123.00', '131313.00', '5.00', '2018-06-03 12:51:15', 'gold chian ', NULL, 1, '123456', '1958-81XoN8DdizL._UL1500_.jpg'),
(293, 24, 9, 'NECKLACE', '18k', NULL, '3.00', '42230.00', '0.00', '22000.00', '0.00', '85000.00', '0.00', '2018-07-11 22:52:36', '', NULL, 0, '', NULL),
(294, 24, 9, 'Yellow Drop Earrings', '22KT', NULL, '0.00', '2750.00', '0.00', '22000.00', '0.00', '40000.00', '0.00', '2018-07-12 12:00:24', '', NULL, 0, '', NULL),
(295, 24, 9, 'ZING JEWEL SET', '18k', NULL, '3.00', '18500.00', '0.00', '22000.00', '0.00', '35000.00', '0.00', '2018-07-13 11:47:35', '', NULL, 0, '6585', NULL),
(296, 24, 7, 'GOLD WATCH', '19K', NULL, '0.00', '16400.00', '0.00', '3060.00', '0.00', '15000.00', '5.00', '2018-08-12 19:48:02', '', NULL, 0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchaseldetails_tbl`
--

CREATE TABLE `purchaseldetails_tbl` (
  `purchaseDetailsId` int(11) NOT NULL,
  `purchaseMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,5) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `hsnOrSacCode` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `grossWeight` decimal(18,2) NOT NULL,
  `stoneWeight` decimal(18,2) NOT NULL,
  `stoneRate` decimal(18,2) NOT NULL,
  `nettWeight` decimal(18,2) NOT NULL,
  `wastage` decimal(18,2) NOT NULL,
  `makingCharge` decimal(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `purchaseldetails_tbl`
--

INSERT INTO `purchaseldetails_tbl` (`purchaseDetailsId`, `purchaseMasterId`, `productId`, `unitId`, `unitConversionId`, `unitConversionRate`, `taxType`, `taxPercent`, `qty`, `rate`, `netAmount`, `taxAmount`, `totalAmount`, `hsnOrSacCode`, `grossWeight`, `stoneWeight`, `stoneRate`, `nettWeight`, `wastage`, `makingCharge`) VALUES
(5, 5, 294, 9, NULL, NULL, NULL, '3.00', '5.00000', '31829.00', '159145.00', '4774.35', '163919.35', '6565', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 5, 295, 9, NULL, NULL, NULL, '3.00', '3.00000', '27500.00', '82500.00', '2475.00', '84975.00', '6585', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(24, 20, 296, 7, 8, NULL, NULL, '0.00', '1.00000', '115384.62', '115384.62', '0.00', '115384.62', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(27, 23, 295, 9, NULL, NULL, NULL, '0.00', '13.56300', '27500.00', '372982.50', '0.00', '372982.50', '6585', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(28, 23, 294, 9, 3, NULL, NULL, '0.00', '24.00000', '3978.63', '95487.12', '0.00', '95487.12', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(29, 24, 293, 9, NULL, NULL, NULL, '3.00', '13.76200', '66745.00', '918544.69', '27556.34', '946101.03', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(30, 25, 292, 9, 3, NULL, NULL, '0.00', '18.00000', '3557.38', '64032.84', '0.00', '64032.84', '123456', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(31, 26, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '66745.00', '2002.35', '68747.35', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(32, 27, 294, 9, 3, NULL, NULL, '3.00', '19.00000', '3978.63', '75593.97', '2267.82', '77861.79', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(33, 28, 294, 9, NULL, NULL, NULL, '3.00', '1.00000', '31829.00', '31829.00', '954.87', '32783.87', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(34, 28, 293, 9, 3, NULL, NULL, '3.00', '4.00000', '8343.13', '33372.52', '1001.18', '34373.70', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(35, 29, 295, 9, 3, NULL, NULL, '3.00', '8.00000', '3437.50', '27500.00', '825.00', '28325.00', '6585', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(36, 30, 295, 9, 3, NULL, NULL, '3.00', '35.76400', '3437.50', '122938.75', '3688.16', '126626.91', '6585', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(37, 31, 296, 7, 8, NULL, NULL, '3.00', '1.00000', '115384.62', '115384.62', '3461.54', '118846.16', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(38, 32, 296, 7, NULL, NULL, NULL, '3.00', '13.29800', '15000.00', '199470.00', '5984.10', '205454.10', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(39, 33, 296, 7, 8, NULL, NULL, '0.00', '1.50000', '115384.62', '173076.93', '0.00', '173076.93', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(40, 34, 293, 9, NULL, NULL, NULL, '3.00', '2.30000', '66745.00', '153513.50', '4605.40', '158118.90', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(43, 37, 294, 9, NULL, NULL, NULL, '3.00', '2.46000', '16500.00', '40590.00', '1217.70', '41807.70', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(44, 38, 294, 9, NULL, NULL, NULL, '3.00', '1.00000', '22000.00', '22000.00', '660.00', '22660.00', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `purchasemaster_tbl`
--

CREATE TABLE `purchasemaster_tbl` (
  `purchaseMasterId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `purchaseHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `gstIn` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxForm` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `previousBalance` decimal(18,2) DEFAULT NULL,
  `purInvoiceNo` text COLLATE utf8_bin,
  `purInvoiceDate` datetime DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL,
  `isInterstatePurchase` tinyint(1) DEFAULT '0',
  `vendorType` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `purchasemaster_tbl`
--

INSERT INTO `purchasemaster_tbl` (`purchaseMasterId`, `prefix`, `voucherNo`, `entryDate`, `ledgerHead`, `purchaseHead`, `amount`, `discount`, `taxAmount`, `totalAmount`, `paidAmount`, `description`, `gstIn`, `taxForm`, `previousBalance`, `purInvoiceNo`, `purInvoiceDate`, `financialYearId`, `isInterstatePurchase`, `vendorType`) VALUES
(5, 'PUR', 1, '2018-07-17 00:00:00', 22, 12, '241645.00', '0.00', '7249.35', '248894.35', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(20, 'PUR', 1, '2018-08-13 00:00:00', 37, 12, '115384.62', '-0.38', '0.00', '115385.00', NULL, NULL, '', 'true', NULL, '', NULL, 1, 0, NULL),
(23, 'PUR', 2, '2018-08-14 00:00:00', 39, 12, '468469.62', '-0.38', '0.00', '468470.00', NULL, NULL, NULL, 'true', NULL, '', NULL, 1, 0, NULL),
(24, 'PUR', 2, '2018-08-14 00:00:00', 37, 12, '918544.69', '0.03', '27556.34', '946101.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(25, 'PUR', 3, '2018-08-14 00:00:00', 55, 12, '64032.84', '-0.16', '0.00', '64033.00', NULL, NULL, NULL, 'true', NULL, '', NULL, 1, 0, NULL),
(26, 'PUR', 125, '2018-08-14 00:00:00', 42, 12, '66745.00', '0.35', '2002.35', '68747.00', NULL, NULL, NULL, 'false', NULL, '', NULL, 1, 0, NULL),
(27, 'PUR', 3, '2018-08-15 00:00:00', 58, 12, '75593.97', '-0.21', '2267.82', '77862.00', NULL, NULL, '594541521Zs', 'false', NULL, '', NULL, 1, 0, NULL),
(28, 'PUR', 14, '2018-08-15 00:00:00', 59, 12, '65201.52', '-0.43', '1956.05', '67158.00', NULL, NULL, '95215848421Z', 'false', NULL, '', NULL, 1, 0, NULL),
(29, 'PUR', 15, '2018-08-15 00:00:00', 26, 12, '27500.00', '0.00', '825.00', '28325.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(30, 'PUR', 18, '2018-08-15 00:00:00', 27, 12, '122938.75', '-0.09', '3688.16', '126627.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(31, 'PUR', 18, '2018-08-15 00:00:00', 25, 12, '115384.62', '0.16', '3461.54', '118846.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(32, 'PUR', 19, '2018-08-15 00:00:00', 41, 12, '199470.00', '0.10', '5984.10', '205454.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(33, 'PUR', 4, '2018-08-15 00:00:00', 39, 12, '173076.93', '-0.07', '0.00', '173077.00', NULL, NULL, '', 'true', NULL, '', NULL, 1, 0, ''),
(34, 'PUR', 20, '2018-08-15 00:00:00', 29, 12, '153513.50', '-0.10', '4605.40', '158119.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(37, 'PUR', 21, '2018-08-16 00:00:00', 34, 12, '40590.00', '-0.30', '1217.70', '41808.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, NULL),
(38, 'PUR', 23, '2018-08-16 00:00:00', 48, 12, '22000.00', '0.00', '660.00', '22660.00', NULL, NULL, '', 'false', NULL, '', NULL, 1, 0, 'Vendor');

-- --------------------------------------------------------

--
-- Table structure for table `purchasereturndetails_tbl`
--

CREATE TABLE `purchasereturndetails_tbl` (
  `purchaseReturnDetailsId` int(11) NOT NULL,
  `purchaseReturnMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,3) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `purchaseDetailsId` int(11) DEFAULT NULL,
  `hsnOrSacCode` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `purchasereturnmaster_tbl`
--

CREATE TABLE `purchasereturnmaster_tbl` (
  `purchaseReturnMasterId` int(11) NOT NULL,
  `purchaseMasterId` int(11) DEFAULT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `purchaseHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `tinNo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxForm` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `previousBalance` decimal(18,2) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `receiptvoucher_tbl`
--

CREATE TABLE `receiptvoucher_tbl` (
  `receiptID` int(11) NOT NULL,
  `prefix` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `VoucherNo` int(11) DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `ledgerFirst` int(11) DEFAULT NULL,
  `ledgerSecond` int(11) DEFAULT NULL,
  `total` decimal(19,4) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `receiptvoucher_tbl`
--

INSERT INTO `receiptvoucher_tbl` (`receiptID`, `prefix`, `VoucherNo`, `voucherDate`, `ledgerFirst`, `ledgerSecond`, `total`, `description`, `entryDate`, `userId`, `financialYearId`) VALUES
(3, 'RV', 2, '2018-08-15 00:00:00', 4, 45, '145478.0000', 'fdg', '2018-08-15 15:34:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `roleId` int(11) NOT NULL,
  `roleName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`roleId`, `roleName`, `description`, `entryDate`) VALUES
(1, 'Administration', NULL, '2018-05-23 16:55:56');

-- --------------------------------------------------------

--
-- Table structure for table `salesdetails_tbl`
--

CREATE TABLE `salesdetails_tbl` (
  `salesDetailsId` int(11) NOT NULL,
  `salesMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,5) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `hsnOrSacCode` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `purchaseRate` decimal(18,2) DEFAULT NULL,
  `vaPercent` decimal(18,2) DEFAULT NULL,
  `vaAmount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `salesdetails_tbl`
--

INSERT INTO `salesdetails_tbl` (`salesDetailsId`, `salesMasterId`, `productId`, `unitId`, `unitConversionId`, `unitConversionRate`, `taxType`, `taxPercent`, `qty`, `rate`, `netAmount`, `taxAmount`, `totalAmount`, `hsnOrSacCode`, `purchaseRate`, `vaPercent`, `vaAmount`, `discount`) VALUES
(1, 1, 295, 9, NULL, NULL, NULL, '3.00', '1.00000', '27500.00', '28833.98', '865.02', '29699.00', '6585', '18500.00', '5.00', '1375.00', '41.02'),
(2, 2, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '33329.00', '0.00', '33329.00', '', '26433.00', '5.00', '1591.45', '91.45'),
(3, 3, 292, 7, NULL, NULL, NULL, '3.00', '1.00000', '28459.00', '30159.84', '904.80', '31064.64', '123456', '18695.00', '6.50', '1849.84', '149.00'),
(4, 4, 295, 9, NULL, NULL, NULL, '3.00', '1.00000', '27500.00', '27500.00', '825.00', '28325.00', '6585', '18500.00', '0.00', '0.00', '0.00'),
(5, 5, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '66745.00', '2002.35', '68747.35', '', '42230.00', '0.00', '0.00', '0.00'),
(6, 6, 295, 9, NULL, NULL, NULL, '3.00', '1.00000', '27500.00', '28800.00', '864.00', '29664.00', '6585', '18500.00', '5.00', '1375.00', '75.00'),
(7, 5, 294, 9, NULL, NULL, NULL, '3.00', '1.00000', '31829.00', '33420.45', '1002.61', '34423.06', '', '26433.00', '5.00', '1591.45', '0.00'),
(9, 8, 295, 9, NULL, NULL, NULL, '0.00', '1.00000', '27500.00', '29030.00', '0.00', '29030.00', '6585', '18500.00', '6.00', '1650.00', '120.00'),
(10, 9, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(11, 10, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(12, 11, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(13, 12, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(14, 13, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(15, 14, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(16, 15, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(17, 16, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(18, 17, 294, 9, NULL, NULL, NULL, '0.00', '1.00000', '31829.00', '31829.00', '0.00', '31829.00', '', '26433.00', '0.00', '0.00', '0.00'),
(21, 20, 295, 9, NULL, NULL, NULL, '0.00', '1.00000', '27500.00', '28800.00', '0.00', '28800.00', '6585', '18500.00', '5.00', '1375.00', '75.00'),
(26, 25, 295, 9, NULL, NULL, NULL, '3.00', '1.00000', '27500.00', '27500.00', '825.00', '28325.00', '6585', '18500.00', '0.00', '0.00', '0.00'),
(27, 26, 296, 7, 8, NULL, NULL, '3.00', '0.70000', '115384.62', '84784.91', '2543.55', '87328.46', '5851156', '16400.00', '5.00', '4038.46', '22.78'),
(28, 20, 293, 9, 3, NULL, NULL, '0.00', '18.64000', '8343.13', '160100.42', '0.00', '160100.42', '', '42230.00', '3.00', '4665.48', '81.00'),
(29, 26, 293, 9, 3, NULL, NULL, '3.00', '6.57000', '8343.13', '56455.79', '1693.67', '58149.46', '848515', '42230.00', '3.00', '1644.43', '3.00'),
(32, 30, 296, 7, 8, NULL, NULL, '3.00', '1.00000', '115384.62', '115384.62', '3461.54', '118846.16', '', '16400.00', '0.00', '0.00', '0.00'),
(33, 31, 293, 9, NULL, NULL, NULL, '0.00', '1.00000', '66745.00', '69832.25', '0.00', '69832.25', '', '42230.00', '5.00', '3337.25', '250.00'),
(34, 32, 293, 9, 3, NULL, NULL, '3.00', '19.64700', '8343.13', '163917.48', '4917.52', '168835.00', '', '42230.00', '0.00', '0.00', '0.00'),
(36, 34, 292, 9, 3, NULL, NULL, '3.00', '4.50000', '3557.38', '16008.21', '480.25', '16488.46', '123456', '18695.00', '0.00', '0.00', '0.00'),
(37, 35, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '69345.00', '2080.35', '71425.35', '', '42230.00', '4.00', '2669.80', '69.80'),
(38, 36, 295, 9, NULL, NULL, NULL, '3.00', '1.00000', '27500.00', '27500.00', '825.00', '28325.00', '6585', '18500.00', '0.00', '0.00', '0.00'),
(39, 36, 296, 7, NULL, NULL, NULL, '3.00', '1.00000', '15000.00', '15000.00', '450.00', '15450.00', '', '16400.00', '0.00', '0.00', '0.00'),
(40, 37, 295, 9, 3, NULL, NULL, '3.00', '15.63000', '3437.50', '56414.54', '1692.44', '58106.98', '6585', '18500.00', '5.00', '2686.41', '0.00'),
(41, 38, 294, 9, NULL, NULL, NULL, '3.00', '2.70000', '31829.00', '85938.30', '2578.15', '88516.45', '', '26433.00', '0.00', '0.00', '0.00'),
(42, 39, 294, 9, NULL, NULL, NULL, '0.00', '2.00000', '31829.00', '66840.90', '0.00', '66840.90', '', '26433.00', '5.00', '3182.90', '0.00'),
(43, 40, 295, 9, 3, NULL, NULL, '3.00', '24.64700', '2750.00', '69007.11', '2070.21', '71077.32', '6585', '18500.00', '5.50', '3727.86', '2500.00'),
(44, 41, 293, 9, 3, NULL, NULL, '3.00', '12.00000', '2750.00', '34550.00', '1036.50', '35586.50', '', '42230.00', '5.00', '1650.00', '100.00'),
(45, 42, 296, 7, 8, NULL, NULL, '3.00', '1.50000', '23538.46', '36873.07', '1106.19', '37979.26', '', '16400.00', '5.00', '1765.38', '200.00'),
(46, 43, 295, 9, NULL, NULL, NULL, '3.00', '3.00000', '22000.00', '69300.00', '2079.00', '71379.00', '6585', '18500.00', '5.00', '3300.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `salesmaster_tbl`
--

CREATE TABLE `salesmaster_tbl` (
  `salesMasterId` int(11) NOT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `salesHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `gstIn` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxForm` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `salesType` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `previousBalance` decimal(18,2) DEFAULT NULL,
  `poNumber` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `poDate` datetime DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL,
  `partyRef` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `salesOrderMasterId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `salesmaster_tbl`
--

INSERT INTO `salesmaster_tbl` (`salesMasterId`, `prefix`, `voucherNo`, `entryDate`, `ledgerHead`, `salesHead`, `amount`, `discount`, `taxAmount`, `totalAmount`, `paidAmount`, `description`, `gstIn`, `taxForm`, `salesType`, `previousBalance`, `poNumber`, `poDate`, `financialYearId`, `partyRef`, `salesOrderMasterId`) VALUES
(1, 'INV', 1, '2018-07-18 00:00:00', 28, 15, '28833.98', '0.00', '865.02', '29699.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, 'NO ONE', NULL),
(2, 'INV', 1, '2018-07-27 00:00:00', 4, 15, '33329.00', '0.00', '0.00', '33329.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, 'Vasim', NULL),
(3, 'INV', 2, '2018-07-30 00:00:00', 27, 15, '30159.84', '0.00', '904.80', '31064.64', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', 1),
(4, 'INV', 3, '2018-07-30 00:00:00', 21, 15, '27500.00', '0.00', '825.00', '28325.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', 2),
(5, 'INV', 4, '2018-07-30 00:00:00', 29, 15, '100165.45', '0.00', '3004.96', '103170.41', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(6, 'INV', 5, '2018-08-01 00:00:00', 43, 15, '28800.00', '0.00', '864.00', '29664.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(8, 'INV', 2, '2018-08-01 00:00:00', 44, 15, '29030.00', '0.00', '0.00', '29030.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(9, 'INV', 3, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(10, 'INV', 4, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(11, 'INV', 5, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(12, 'INV', 6, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(13, 'INV', 7, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(14, 'INV', 8, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(15, 'INV', 9, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(16, 'INV', 10, '2018-08-05 00:00:00', 25, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(17, 'INV', 11, '2018-08-05 00:00:00', 26, 15, '31829.00', '0.00', '0.00', '31829.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(20, 'INV', 12, '2018-08-13 00:00:00', 21, 15, '188900.42', '0.42', '0.00', '188900.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(25, 'INV', 6, '2018-08-13 00:00:00', 43, 15, '27500.00', '0.00', '825.00', '28325.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(26, 'INV', 7, '2018-08-13 00:00:00', 45, 15, '141240.70', '-0.08', '4237.22', '145478.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(30, 'INV', 8, '2018-08-14 00:00:00', 52, 15, '115384.62', '0.16', '3461.54', '118846.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(31, 'INV', 13, '2018-08-14 00:00:00', 53, 15, '69832.25', '0.25', '0.00', '69832.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(32, 'INV', 9, '2018-08-14 00:00:00', 57, 15, '163917.48', '0.00', '4917.52', '168835.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(34, 'INV', 10, '2018-08-14 00:00:00', 58, 15, '16008.21', '0.46', '480.25', '16488.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(35, 'INV', 11, '2018-08-15 00:00:00', 59, 15, '69345.00', '0.35', '2080.35', '71425.00', NULL, '', '95215848421Z', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(36, 'INV', 12, '2018-08-15 00:00:00', 38, 15, '42500.00', '0.00', '1275.00', '43775.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', 5),
(37, 'INV', 13, '2018-08-15 00:00:00', 35, 15, '56414.54', '-0.02', '1692.44', '58107.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(38, 'INV', 14, '2018-08-15 00:00:00', 24, 15, '85938.30', '0.45', '2578.15', '88516.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(39, 'INV', 14, '2018-08-15 00:00:00', 38, 15, '66840.90', '-0.10', '0.00', '66841.00', NULL, '', NULL, NULL, 'Estimate', NULL, NULL, NULL, 1, '', NULL),
(40, 'INV', 15, '2018-08-15 00:00:00', 60, 15, '69007.11', '0.32', '2070.21', '71077.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, 'Rao', 5),
(41, 'INV', 16, '2018-08-19 00:00:00', 61, 15, '34550.00', '-0.50', '1036.50', '35587.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(42, 'INV', 17, '2018-08-19 00:00:00', 62, 15, '36873.07', '0.26', '1106.19', '37979.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL),
(43, 'INV', 18, '2018-08-19 00:00:00', 63, 15, '69300.00', '0.00', '2079.00', '71379.00', NULL, '', '', NULL, 'Sales', NULL, NULL, NULL, 1, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `salesorderdetails_tbl`
--

CREATE TABLE `salesorderdetails_tbl` (
  `salesOrderDetailsId` int(11) NOT NULL,
  `salesOrderMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,5) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `hsnOrSacCode` varchar(50) DEFAULT NULL,
  `purchaseRate` decimal(18,2) DEFAULT NULL,
  `vaPercent` decimal(18,2) DEFAULT NULL,
  `vaAmount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salesorderdetails_tbl`
--

INSERT INTO `salesorderdetails_tbl` (`salesOrderDetailsId`, `salesOrderMasterId`, `productId`, `unitId`, `unitConversionId`, `unitConversionRate`, `taxType`, `taxPercent`, `qty`, `rate`, `netAmount`, `taxAmount`, `totalAmount`, `hsnOrSacCode`, `purchaseRate`, `vaPercent`, `vaAmount`, `discount`) VALUES
(1, 1, 292, 7, NULL, NULL, NULL, '3.00', '1.00000', '28459.00', '29681.95', '890.46', '30572.41', '123456', '18695.00', '5.00', '1422.95', '200.00'),
(2, 2, 292, 7, NULL, NULL, NULL, '3.00', '1.00000', '28459.00', '30154.54', '904.64', '31059.18', '123456', '18695.00', '6.00', '1707.54', '12.00'),
(3, 3, 295, 9, 3, NULL, NULL, '3.00', '1.00000', '3437.50', '3437.50', '103.13', '3540.63', '6585', '18500.00', '0.00', '0.00', '0.00'),
(4, 3, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '66745.00', '2002.35', '68747.35', '', '42230.00', '0.00', '0.00', '0.00'),
(5, 4, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '66745.00', '2002.35', '68747.35', '', '42230.00', '0.00', '0.00', '0.00'),
(6, 5, 293, 9, NULL, NULL, NULL, '3.00', '1.00000', '66745.00', '66745.00', '2002.35', '68747.35', '', '42230.00', '0.00', '0.00', '0.00'),
(7, 5, 296, 7, 8, NULL, NULL, '3.00', '1.00000', '115384.62', '115384.62', '3461.54', '118846.16', '', '16400.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `salesordermaster_tbl`
--

CREATE TABLE `salesordermaster_tbl` (
  `salesOrderMasterId` int(11) NOT NULL,
  `prefix` varchar(20) DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `salesHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text,
  `tinNo` varchar(100) DEFAULT NULL,
  `taxForm` varchar(100) DEFAULT NULL,
  `salesType` varchar(200) DEFAULT NULL,
  `previousBalance` decimal(18,2) DEFAULT NULL,
  `poNumber` varchar(100) DEFAULT NULL,
  `poDate` datetime DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL,
  `partyRef` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salesordermaster_tbl`
--

INSERT INTO `salesordermaster_tbl` (`salesOrderMasterId`, `prefix`, `voucherNo`, `entryDate`, `ledgerHead`, `salesHead`, `amount`, `discount`, `taxAmount`, `totalAmount`, `paidAmount`, `description`, `tinNo`, `taxForm`, `salesType`, `previousBalance`, `poNumber`, `poDate`, `financialYearId`, `partyRef`) VALUES
(1, 'SO', 1, '2018-07-28 00:00:00', 40, 15, '29681.95', '0.00', '890.46', '30572.41', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, ''),
(2, 'SO', 2, '2018-07-30 00:00:00', 35, 15, '30154.54', '0.00', '904.64', '31059.18', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, ''),
(3, 'SO', 3, '2018-08-13 00:00:00', 44, 15, '70182.50', '0.00', '2105.48', '72287.98', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, ''),
(4, 'SO', 4, '2018-08-13 00:00:00', 38, 15, '66745.00', '0.35', '2002.35', '68747.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, ''),
(5, 'SO', 5, '2018-08-15 00:00:00', 24, 15, '182129.62', '-0.49', '5463.89', '187594.00', NULL, '', NULL, NULL, 'Sales', NULL, NULL, NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `salesreturndetails_tbl`
--

CREATE TABLE `salesreturndetails_tbl` (
  `salesReturnDetailsId` int(11) NOT NULL,
  `salesReturnMasterId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `unitId` int(11) DEFAULT NULL,
  `unitConversionId` int(11) DEFAULT NULL,
  `unitConversionRate` decimal(18,2) DEFAULT NULL,
  `taxType` text COLLATE utf8_bin,
  `taxPercent` decimal(18,2) DEFAULT NULL,
  `qty` decimal(18,3) DEFAULT NULL,
  `rate` decimal(18,2) DEFAULT NULL,
  `netAmount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `salesDetailsId` int(11) DEFAULT NULL,
  `hsnOrSacCode` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `salesreturnmaster_tbl`
--

CREATE TABLE `salesreturnmaster_tbl` (
  `salesReturnMasterId` int(11) NOT NULL,
  `salesMasterId` int(11) DEFAULT NULL,
  `prefix` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `voucherNo` int(11) DEFAULT NULL,
  `entryDate` datetime DEFAULT NULL,
  `ledgerHead` int(11) DEFAULT NULL,
  `salesHead` int(11) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  `taxAmount` decimal(18,2) DEFAULT NULL,
  `totalAmount` decimal(18,2) DEFAULT NULL,
  `paidAmount` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `tinNo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `taxForm` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `salesType` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `previousBalance` decimal(18,2) DEFAULT NULL,
  `financialYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servicedepartment_tbl`
--

CREATE TABLE `servicedepartment_tbl` (
  `serviceDepartmentId` int(11) NOT NULL,
  `serviceDepartmentName` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `submodule_tbl`
--

CREATE TABLE `submodule_tbl` (
  `subModuleId` int(11) NOT NULL,
  `mainModuleId` int(11) DEFAULT NULL,
  `moduleName` varchar(100) DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submodule_tbl`
--

INSERT INTO `submodule_tbl` (`subModuleId`, `mainModuleId`, `moduleName`, `isActive`) VALUES
(1, 1, 'Company', b'1'),
(2, 1, 'Role', b'0'),
(3, 1, 'Users', b'1'),
(4, 1, 'Settings', b'0'),
(5, 2, 'Units', b'1'),
(6, 2, 'Unit Conversion', b'1'),
(7, 2, 'Product Group', b'1'),
(8, 2, 'Products', b'1'),
(9, 2, 'Contacts', b'1'),
(10, 2, 'Account Groups', b'0'),
(11, 2, 'Account Ledger', b'0'),
(12, 1, 'Financial Year', b'0'),
(13, 3, 'Payment Voucher', b'1'),
(14, 3, 'Receipt Voucher', b'1'),
(15, 3, 'Contra Voucher', b'0'),
(16, 3, 'Journal Voucher', b'0'),
(17, 3, 'PDC Payble', b'0'),
(18, 3, 'PDC Receivable', b'0'),
(19, 3, 'PDC Clearance ', b'0'),
(20, 3, 'Purchase Invoice', b'1'),
(21, 3, 'Sales Order', b'1'),
(22, 3, 'Sales Invoice', b'1'),
(23, 3, 'POS', b'0'),
(24, 3, 'Sales Return', b'0'),
(25, 3, 'Estimate Bill', b'0'),
(26, 4, 'Day Book', b'0'),
(27, 4, 'Ledger Book', b'1'),
(28, 4, 'Stock Report', b'1'),
(29, 4, 'Closing Stock Report', b'0'),
(30, 4, 'Tax Report', b'0'),
(32, 5, 'Profit And Loss', b'1'),
(33, 5, 'Balance Sheet', b'1'),
(34, 5, 'Trial Balance', b'1'),
(35, 4, 'Stock Report - Estimate', b'0'),
(36, 4, 'Purchase Report', b'1'),
(37, 4, 'Sales Report', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_tbl`
--

CREATE TABLE `transaction_tbl` (
  `transId` int(11) NOT NULL,
  `voucherId` int(11) DEFAULT NULL,
  `voucherType` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `voucherDate` datetime DEFAULT NULL,
  `voucherNo` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ledgerId` int(11) DEFAULT NULL,
  `Dr` decimal(19,4) DEFAULT NULL,
  `Cr` decimal(19,4) DEFAULT NULL,
  `transDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transaction_tbl`
--

INSERT INTO `transaction_tbl` (`transId`, `voucherId`, `voucherType`, `voucherDate`, `voucherNo`, `ledgerId`, `Dr`, `Cr`, `transDate`) VALUES
(463, 7, 'Payment Voucher', '2018-07-18 00:00:00', 'PV/2', 22, '155000.0000', '0.0000', '2018-08-15 12:17:50'),
(464, 7, 'Payment Voucher', '2018-07-18 00:00:00', 'PV/2', 4, '0.0000', '155000.0000', '2018-08-15 12:17:50'),
(465, 6, 'Payment Voucher', '2018-07-18 00:00:00', 'PV/1', 22, '125000.0000', '0.0000', '2018-08-15 12:18:30'),
(466, 6, 'Payment Voucher', '2018-07-18 00:00:00', 'PV/1', 4, '0.0000', '125000.0000', '2018-08-15 12:18:30'),
(467, 5, 'Purchase Invoice', '2018-07-17 00:00:00', 'PUR/1', 22, '0.0000', '248894.3500', '2018-08-15 12:27:40'),
(468, 5, 'Purchase Invoice', '2018-07-17 00:00:00', 'PUR/1', 12, '248894.3500', '0.0000', '2018-08-15 12:27:40'),
(469, 24, 'Purchase Invoice', '2018-08-14 00:00:00', 'PUR/2', 37, '0.0000', '946101.0000', '2018-08-15 12:33:57'),
(470, 24, 'Purchase Invoice', '2018-08-14 00:00:00', 'PUR/2', 12, '946101.0000', '0.0000', '2018-08-15 12:33:57'),
(471, 1, 'Sales Invoice', '2018-07-18 00:00:00', 'INV/1', 28, '29699.0000', '0.0000', '2018-08-15 12:36:36'),
(472, 1, 'Sales Invoice', '2018-07-18 00:00:00', 'INV/1', 15, '0.0000', '29699.0000', '2018-08-15 12:36:36'),
(473, 10, 'Estimate Bill', '2018-08-05 00:00:00', 'INV/4', 25, '31829.0000', '0.0000', '2018-08-15 12:53:59'),
(474, 10, 'Estimate Bill', '2018-08-05 00:00:00', 'INV/4', 15, '0.0000', '31829.0000', '2018-08-15 12:54:00'),
(475, 20, 'Purchase Without', '2018-08-13 00:00:00', 'PUR/1', 37, '0.0000', '115385.0000', '2018-08-15 12:54:55'),
(476, 20, 'Purchase Without', '2018-08-13 00:00:00', 'PUR/1', 12, '115385.0000', '0.0000', '2018-08-15 12:54:55'),
(491, 26, 'Sales Invoice', '2018-08-13 00:00:00', 'INV/7', 45, '145478.0000', '0.0000', '2018-08-15 15:55:05'),
(492, 26, 'Sales Invoice', '2018-08-13 00:00:00', 'INV/7', 15, '0.0000', '145478.0000', '2018-08-15 15:55:05'),
(503, 8, 'Payment Voucher', '2018-08-15 00:00:00', 'PV/3', 59, '67158.0000', '0.0000', '2018-08-15 16:43:30'),
(504, 8, 'Payment Voucher', '2018-08-15 00:00:00', 'PV/3', 4, '0.0000', '67158.0000', '2018-08-15 16:43:30'),
(505, 3, 'Receipt Voucher', '2018-08-15 00:00:00', 'RV/2', 45, '0.0000', '145478.0000', '2018-08-15 16:45:41'),
(506, 3, 'Receipt Voucher', '2018-08-15 00:00:00', 'RV/2', 4, '145478.0000', '0.0000', '2018-08-15 16:45:41'),
(507, 29, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/15', 26, '0.0000', '28325.0000', '2018-08-15 18:57:55'),
(508, 29, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/15', 12, '28325.0000', '0.0000', '2018-08-15 18:57:55'),
(509, 30, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/18', 27, '0.0000', '126627.0000', '2018-08-15 18:59:18'),
(510, 30, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/18', 12, '126627.0000', '0.0000', '2018-08-15 18:59:18'),
(511, 31, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/18', 25, '0.0000', '118846.0000', '2018-08-15 19:00:21'),
(512, 31, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/18', 12, '118846.0000', '0.0000', '2018-08-15 19:00:21'),
(513, 32, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/19', 41, '0.0000', '205454.0000', '2018-08-15 19:02:02'),
(514, 32, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/19', 12, '205454.0000', '0.0000', '2018-08-15 19:02:02'),
(515, 33, 'Purchase Without', '2018-08-15 00:00:00', 'PUR/4', 39, '0.0000', '173077.0000', '2018-08-15 20:15:06'),
(516, 33, 'Purchase Without', '2018-08-15 00:00:00', 'PUR/4', 12, '173077.0000', '0.0000', '2018-08-15 20:15:06'),
(517, 34, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/20', 29, '0.0000', '158119.0000', '2018-08-15 20:17:58'),
(518, 34, 'Purchase Invoice', '2018-08-15 00:00:00', 'PUR/20', 12, '158119.0000', '0.0000', '2018-08-15 20:17:58'),
(523, 35, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/22', 34, '0.0000', '42488.0000', '2018-08-16 01:31:59'),
(524, 35, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/22', 12, '42488.0000', '0.0000', '2018-08-16 01:31:59'),
(525, 36, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/22', 34, '0.0000', '42488.0000', '2018-08-16 01:33:09'),
(526, 36, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/22', 12, '42488.0000', '0.0000', '2018-08-16 01:33:09'),
(527, 37, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/21', 34, '0.0000', '41808.0000', '2018-08-16 01:35:39'),
(528, 37, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/21', 12, '41808.0000', '0.0000', '2018-08-16 01:35:39'),
(531, 40, 'Sales Invoice', '2018-08-15 00:00:00', 'INV/15', 60, '71077.0000', '0.0000', '2018-08-16 22:00:58'),
(532, 40, 'Sales Invoice', '2018-08-15 00:00:00', 'INV/15', 15, '0.0000', '71077.0000', '2018-08-16 22:00:58'),
(537, 41, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/16', 61, '35587.0000', '0.0000', '2018-08-19 12:56:44'),
(538, 41, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/16', 15, '0.0000', '35587.0000', '2018-08-19 12:56:44'),
(571, 33, 'Purchase Without', '2018-08-15 00:00:00', 'PUR/4', 39, '0.0000', '173077.0000', '2018-08-19 14:58:04'),
(572, 33, 'Purchase Without', '2018-08-15 00:00:00', 'PUR/4', 12, '173077.0000', '0.0000', '2018-08-19 14:58:04'),
(577, 39, 'Purchase Invoice', '2018-08-19 00:00:00', 'PUR/5', 64, '0.0000', '37400.0000', '2018-08-19 15:19:35'),
(578, 39, 'Purchase Invoice', '2018-08-19 00:00:00', 'PUR/5', 12, '37400.0000', '0.0000', '2018-08-19 15:19:35'),
(579, 38, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/23', 48, '0.0000', '22660.0000', '2018-08-19 15:22:19'),
(580, 38, 'Purchase Invoice', '2018-08-16 00:00:00', 'PUR/23', 12, '22660.0000', '0.0000', '2018-08-19 15:22:19'),
(597, 38, 'Sales Invoice', '2018-08-15 00:00:00', 'INV/14', 24, '88516.0000', '0.0000', '2018-08-21 13:12:12'),
(598, 38, 'Sales Invoice', '2018-08-15 00:00:00', 'INV/14', 15, '0.0000', '88516.0000', '2018-08-21 13:12:12'),
(603, 42, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/17', 62, '37979.0000', '0.0000', '2018-08-21 13:25:44'),
(604, 42, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/17', 15, '0.0000', '37979.0000', '2018-08-21 13:25:44'),
(605, 40, 'Purchase Invoice', '2018-08-21 00:00:00', 'PUR/5', 65, '0.0000', '46402.0000', '2018-08-21 13:47:10'),
(606, 40, 'Purchase Invoice', '2018-08-21 00:00:00', 'PUR/5', 12, '46402.0000', '0.0000', '2018-08-21 13:47:10'),
(609, 43, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/18', 63, '71379.0000', '0.0000', '2018-08-21 14:05:29'),
(610, 43, 'Sales Invoice', '2018-08-19 00:00:00', 'INV/18', 15, '0.0000', '71379.0000', '2018-08-21 14:05:29'),
(611, 39, 'Estimate Bill', '2018-08-15 00:00:00', 'INV/14', 38, '66841.0000', '0.0000', '2018-08-21 14:05:50'),
(612, 39, 'Estimate Bill', '2018-08-15 00:00:00', 'INV/14', 15, '0.0000', '66841.0000', '2018-08-21 14:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `unitconversion_tbl`
--

CREATE TABLE `unitconversion_tbl` (
  `unitConversionId` int(11) NOT NULL,
  `unitId` int(11) DEFAULT NULL,
  `conversionUnitId` int(11) DEFAULT NULL,
  `conversionRate` decimal(18,2) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `downCode` text COLLATE utf8_bin,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `unitconversion_tbl`
--

INSERT INTO `unitconversion_tbl` (`unitConversionId`, `unitId`, `conversionUnitId`, `conversionRate`, `description`, `downCode`, `entryDate`) VALUES
(3, 9, 7, '8.00', NULL, NULL, '2018-07-04 13:57:28'),
(4, 10, 7, '1000.00', NULL, NULL, '2018-07-04 13:58:58'),
(6, 9, 8, '23.00', NULL, NULL, '2018-07-04 14:05:44'),
(8, 7, 9, '0.13', NULL, NULL, '2018-08-12 22:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `unit_tbl`
--

CREATE TABLE `unit_tbl` (
  `unitId` int(11) NOT NULL,
  `unitName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `unit_tbl`
--

INSERT INTO `unit_tbl` (`unitId`, `unitName`, `description`) VALUES
(7, 'Gram', 'Gram '),
(8, 'diamond', 'diamond'),
(9, 'Pavan', ''),
(10, 'KILO', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) DEFAULT NULL,
  `userName` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `userType` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `secondPassword` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `entryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `companyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`userId`, `roleId`, `userName`, `email`, `password`, `userType`, `secondPassword`, `entryDate`, `companyId`) VALUES
(1, NULL, 'admin', 'admin.baits@gmail.com', 'admin', NULL, 'admin', '2018-01-11 16:16:03', 1),
(3, NULL, 'ali', 'ali@gmail.com', 'ali123', 'Admin', NULL, '2018-06-07 17:17:50', 1),
(8, NULL, 'Ben', 'ben@eklavya.com', 'ben123', 'User', NULL, '2018-07-04 12:22:35', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_tbl`
--
ALTER TABLE `account_tbl`
  ADD PRIMARY KEY (`accountId`);

--
-- Indexes for table `company_tbl`
--
ALTER TABLE `company_tbl`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `contacts_tbl`
--
ALTER TABLE `contacts_tbl`
  ADD PRIMARY KEY (`contactId`),
  ADD KEY `FK__contacts___ledge__719CDDE7` (`ledgerId`);

--
-- Indexes for table `contradetails_tbl`
--
ALTER TABLE `contradetails_tbl`
  ADD PRIMARY KEY (`contraDetailsId`),
  ADD KEY `FK__contraDet__contr__72910220` (`contraMasterId`),
  ADD KEY `FK__contraDet__ledge__73852659` (`ledgerId`);

--
-- Indexes for table `contramaster_tbl`
--
ALTER TABLE `contramaster_tbl`
  ADD PRIMARY KEY (`contraMasterId`),
  ADD KEY `FK__contraMas__finan__2E86BBED` (`financialYearId`),
  ADD KEY `FK__contraMas__ledge__6FB49575` (`ledgerId`),
  ADD KEY `FK__contraMas__userI__70A8B9AE` (`userId`);

--
-- Indexes for table `designation_tbl`
--
ALTER TABLE `designation_tbl`
  ADD PRIMARY KEY (`designationId`);

--
-- Indexes for table `estimatedetails_tbl`
--
ALTER TABLE `estimatedetails_tbl`
  ADD PRIMARY KEY (`estimateDetailsId`),
  ADD KEY `FK__estimateD__estim__558AAF1E` (`estimateMasterId`),
  ADD KEY `FK__estimateD__produ__567ED357` (`productId`),
  ADD KEY `FK__estimateD__unitC__58671BC9` (`unitConversionId`),
  ADD KEY `FK__estimateD__unitI__5772F790` (`unitId`);

--
-- Indexes for table `estimatemaster_tbl`
--
ALTER TABLE `estimatemaster_tbl`
  ADD PRIMARY KEY (`estimateMasterId`),
  ADD KEY `FK__estimateM__ledge__4FD1D5C8` (`ledgerHead`),
  ADD KEY `FK__estimateM__sales__50C5FA01` (`salesHead`);

--
-- Indexes for table `financialyear_tbl`
--
ALTER TABLE `financialyear_tbl`
  ADD PRIMARY KEY (`financialYearId`),
  ADD KEY `FK__financial__userI__2BAA4F42` (`userId`);

--
-- Indexes for table `journaldetails_tbl`
--
ALTER TABLE `journaldetails_tbl`
  ADD PRIMARY KEY (`journalDetailsId`),
  ADD KEY `FK__journalDe__journ__74794A92` (`journalMasterId`),
  ADD KEY `FK__journalDe__ledge__756D6ECB` (`ledgerId`);

--
-- Indexes for table `journalmaster_tbl`
--
ALTER TABLE `journalmaster_tbl`
  ADD PRIMARY KEY (`journalMasterId`),
  ADD KEY `FK__journalMa__finan__2F7AE026` (`financialYearId`),
  ADD KEY `FK__journalMa__userI__6EC0713C` (`userId`);

--
-- Indexes for table `ledger_tbl`
--
ALTER TABLE `ledger_tbl`
  ADD PRIMARY KEY (`ledgerId`),
  ADD KEY `FK__ledger_tb__accou__5E8A0973` (`accountId`);

--
-- Indexes for table `mainmodule_tbl`
--
ALTER TABLE `mainmodule_tbl`
  ADD PRIMARY KEY (`moduleId`);

--
-- Indexes for table `paymentvoucher_tbl`
--
ALTER TABLE `paymentvoucher_tbl`
  ADD PRIMARY KEY (`paymentID`),
  ADD KEY `FK__paymentVo__finan__2C9E737B` (`financialYearId`),
  ADD KEY `FK__paymentVo__ledge__6442E2C9` (`ledgerFirst`),
  ADD KEY `FK__paymentVo__ledge__65370702` (`ledgerSecond`),
  ADD KEY `FK__paymentVo__userI__662B2B3B` (`userId`);

--
-- Indexes for table `pdcpayable_tbl`
--
ALTER TABLE `pdcpayable_tbl`
  ADD PRIMARY KEY (`pdcPayableId`),
  ADD KEY `FK__pdcPayabl__Clear__671F4F74` (`ClearUserId`),
  ADD KEY `FK__pdcPayabl__finan__306F045F` (`financialYearId`),
  ADD KEY `FK__pdcPayabl__ledge__681373AD` (`ledgerCustId`),
  ADD KEY `FK__pdcPayabl__ledge__690797E6` (`ledgerBankId`),
  ADD KEY `FK__pdcPayabl__userI__69FBBC1F` (`userId`);

--
-- Indexes for table `pdcreceivable_tbl`
--
ALTER TABLE `pdcreceivable_tbl`
  ADD PRIMARY KEY (`pdcReceivableId`),
  ADD KEY `FK__pdcReceiv__Clear__6AEFE058` (`ClearUserId`),
  ADD KEY `FK__pdcReceiv__finan__31632898` (`financialYearId`),
  ADD KEY `FK__pdcReceiv__ledge__6BE40491` (`ledgerCustId`),
  ADD KEY `FK__pdcReceiv__ledge__6CD828CA` (`ledgerBankId`),
  ADD KEY `FK__pdcReceiv__userI__6DCC4D03` (`userId`);

--
-- Indexes for table `productgroup_tbl`
--
ALTER TABLE `productgroup_tbl`
  ADD PRIMARY KEY (`productGroupId`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`productId`),
  ADD KEY `FK__product_t__produ__7167D3BD` (`productGroupId`),
  ADD KEY `FK__product_t__unitI__725BF7F6` (`unitId`);

--
-- Indexes for table `purchaseldetails_tbl`
--
ALTER TABLE `purchaseldetails_tbl`
  ADD PRIMARY KEY (`purchaseDetailsId`),
  ADD KEY `FK__purchasel__produ__7EC1CEDB` (`productId`),
  ADD KEY `FK__purchasel__purch__7DCDAAA2` (`purchaseMasterId`),
  ADD KEY `FK__purchasel__unitC__00AA174D` (`unitConversionId`),
  ADD KEY `FK__purchasel__unitI__7FB5F314` (`unitId`);

--
-- Indexes for table `purchasemaster_tbl`
--
ALTER TABLE `purchasemaster_tbl`
  ADD PRIMARY KEY (`purchaseMasterId`),
  ADD KEY `FK__purchaseM__finan__32574CD1` (`financialYearId`),
  ADD KEY `FK__purchaseM__ledge__7814D14C` (`ledgerHead`),
  ADD KEY `FK__purchaseM__purch__7908F585` (`purchaseHead`);

--
-- Indexes for table `purchasereturndetails_tbl`
--
ALTER TABLE `purchasereturndetails_tbl`
  ADD PRIMARY KEY (`purchaseReturnDetailsId`),
  ADD KEY `FK__purchaseR__produ__0D0FEE32` (`productId`),
  ADD KEY `FK__purchaseR__purch__0C1BC9F9` (`purchaseReturnMasterId`),
  ADD KEY `FK__purchaseR__purch__3118447E` (`purchaseDetailsId`),
  ADD KEY `FK__purchaseR__unitC__0EF836A4` (`unitConversionId`),
  ADD KEY `FK__purchaseR__unitI__0E04126B` (`unitId`);

--
-- Indexes for table `purchasereturnmaster_tbl`
--
ALTER TABLE `purchasereturnmaster_tbl`
  ADD PRIMARY KEY (`purchaseReturnMasterId`),
  ADD KEY `FK__purchaseR__finan__334B710A` (`financialYearId`),
  ADD KEY `FK__purchaseR__ledge__0662F0A3` (`ledgerHead`),
  ADD KEY `FK__purchaseR__purch__056ECC6A` (`purchaseMasterId`),
  ADD KEY `FK__purchaseR__purch__075714DC` (`purchaseHead`);

--
-- Indexes for table `receiptvoucher_tbl`
--
ALTER TABLE `receiptvoucher_tbl`
  ADD PRIMARY KEY (`receiptID`),
  ADD KEY `FK__receiptVo__finan__2D9297B4` (`financialYearId`),
  ADD KEY `FK__receiptVo__ledge__607251E5` (`ledgerFirst`),
  ADD KEY `FK__receiptVo__ledge__6166761E` (`ledgerSecond`),
  ADD KEY `FK__receiptVo__userI__625A9A57` (`userId`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `salesdetails_tbl`
--
ALTER TABLE `salesdetails_tbl`
  ADD PRIMARY KEY (`salesDetailsId`),
  ADD KEY `FK__salesDeta__produ__1A69E950` (`productId`),
  ADD KEY `FK__salesDeta__sales__1975C517` (`salesMasterId`),
  ADD KEY `FK__salesDeta__unitC__1C5231C2` (`unitConversionId`),
  ADD KEY `FK__salesDeta__unitI__1B5E0D89` (`unitId`);

--
-- Indexes for table `salesmaster_tbl`
--
ALTER TABLE `salesmaster_tbl`
  ADD PRIMARY KEY (`salesMasterId`),
  ADD KEY `FK__salesMast__finan__343F9543` (`financialYearId`),
  ADD KEY `FK__salesMast__ledge__13BCEBC1` (`ledgerHead`),
  ADD KEY `FK__salesMast__sales__14B10FFA` (`salesHead`);

--
-- Indexes for table `salesorderdetails_tbl`
--
ALTER TABLE `salesorderdetails_tbl`
  ADD PRIMARY KEY (`salesOrderDetailsId`);

--
-- Indexes for table `salesordermaster_tbl`
--
ALTER TABLE `salesordermaster_tbl`
  ADD PRIMARY KEY (`salesOrderMasterId`);

--
-- Indexes for table `salesreturndetails_tbl`
--
ALTER TABLE `salesreturndetails_tbl`
  ADD PRIMARY KEY (`salesReturnDetailsId`),
  ADD KEY `FK__salesRetu__produ__28B808A7` (`productId`),
  ADD KEY `FK__salesRetu__sales__27C3E46E` (`salesReturnMasterId`),
  ADD KEY `FK__salesRetu__sales__320C68B7` (`salesDetailsId`),
  ADD KEY `FK__salesRetu__unitC__2AA05119` (`unitConversionId`),
  ADD KEY `FK__salesRetu__unitI__29AC2CE0` (`unitId`);

--
-- Indexes for table `salesreturnmaster_tbl`
--
ALTER TABLE `salesreturnmaster_tbl`
  ADD PRIMARY KEY (`salesReturnMasterId`),
  ADD KEY `FK__salesRetu__finan__3533B97C` (`financialYearId`),
  ADD KEY `FK__salesRetu__ledge__220B0B18` (`ledgerHead`),
  ADD KEY `FK__salesRetu__sales__2116E6DF` (`salesMasterId`),
  ADD KEY `FK__salesRetu__sales__22FF2F51` (`salesHead`);

--
-- Indexes for table `servicedepartment_tbl`
--
ALTER TABLE `servicedepartment_tbl`
  ADD PRIMARY KEY (`serviceDepartmentId`);

--
-- Indexes for table `submodule_tbl`
--
ALTER TABLE `submodule_tbl`
  ADD PRIMARY KEY (`subModuleId`);

--
-- Indexes for table `transaction_tbl`
--
ALTER TABLE `transaction_tbl`
  ADD PRIMARY KEY (`transId`),
  ADD KEY `FK__transacti__ledge__634EBE90` (`ledgerId`);

--
-- Indexes for table `unitconversion_tbl`
--
ALTER TABLE `unitconversion_tbl`
  ADD PRIMARY KEY (`unitConversionId`),
  ADD KEY `FK__unitConve__conve__7EF6D905` (`conversionUnitId`),
  ADD KEY `FK__unitConve__unitI__7E02B4CC` (`unitId`);

--
-- Indexes for table `unit_tbl`
--
ALTER TABLE `unit_tbl`
  ADD PRIMARY KEY (`unitId`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `FK__user_tbl__roleId__5F7E2DAC` (`roleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_tbl`
--
ALTER TABLE `account_tbl`
  MODIFY `accountId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `company_tbl`
--
ALTER TABLE `company_tbl`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts_tbl`
--
ALTER TABLE `contacts_tbl`
  MODIFY `contactId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `contradetails_tbl`
--
ALTER TABLE `contradetails_tbl`
  MODIFY `contraDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contramaster_tbl`
--
ALTER TABLE `contramaster_tbl`
  MODIFY `contraMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `designation_tbl`
--
ALTER TABLE `designation_tbl`
  MODIFY `designationId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `estimatedetails_tbl`
--
ALTER TABLE `estimatedetails_tbl`
  MODIFY `estimateDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `estimatemaster_tbl`
--
ALTER TABLE `estimatemaster_tbl`
  MODIFY `estimateMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `financialyear_tbl`
--
ALTER TABLE `financialyear_tbl`
  MODIFY `financialYearId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `journaldetails_tbl`
--
ALTER TABLE `journaldetails_tbl`
  MODIFY `journalDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `journalmaster_tbl`
--
ALTER TABLE `journalmaster_tbl`
  MODIFY `journalMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ledger_tbl`
--
ALTER TABLE `ledger_tbl`
  MODIFY `ledgerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `mainmodule_tbl`
--
ALTER TABLE `mainmodule_tbl`
  MODIFY `moduleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `paymentvoucher_tbl`
--
ALTER TABLE `paymentvoucher_tbl`
  MODIFY `paymentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pdcpayable_tbl`
--
ALTER TABLE `pdcpayable_tbl`
  MODIFY `pdcPayableId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pdcreceivable_tbl`
--
ALTER TABLE `pdcreceivable_tbl`
  MODIFY `pdcReceivableId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productgroup_tbl`
--
ALTER TABLE `productgroup_tbl`
  MODIFY `productGroupId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT for table `purchaseldetails_tbl`
--
ALTER TABLE `purchaseldetails_tbl`
  MODIFY `purchaseDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `purchasemaster_tbl`
--
ALTER TABLE `purchasemaster_tbl`
  MODIFY `purchaseMasterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `purchasereturndetails_tbl`
--
ALTER TABLE `purchasereturndetails_tbl`
  MODIFY `purchaseReturnDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchasereturnmaster_tbl`
--
ALTER TABLE `purchasereturnmaster_tbl`
  MODIFY `purchaseReturnMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receiptvoucher_tbl`
--
ALTER TABLE `receiptvoucher_tbl`
  MODIFY `receiptID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `roleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `salesdetails_tbl`
--
ALTER TABLE `salesdetails_tbl`
  MODIFY `salesDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `salesmaster_tbl`
--
ALTER TABLE `salesmaster_tbl`
  MODIFY `salesMasterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `salesorderdetails_tbl`
--
ALTER TABLE `salesorderdetails_tbl`
  MODIFY `salesOrderDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `salesordermaster_tbl`
--
ALTER TABLE `salesordermaster_tbl`
  MODIFY `salesOrderMasterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `salesreturndetails_tbl`
--
ALTER TABLE `salesreturndetails_tbl`
  MODIFY `salesReturnDetailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salesreturnmaster_tbl`
--
ALTER TABLE `salesreturnmaster_tbl`
  MODIFY `salesReturnMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `servicedepartment_tbl`
--
ALTER TABLE `servicedepartment_tbl`
  MODIFY `serviceDepartmentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `submodule_tbl`
--
ALTER TABLE `submodule_tbl`
  MODIFY `subModuleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `transaction_tbl`
--
ALTER TABLE `transaction_tbl`
  MODIFY `transId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=613;
--
-- AUTO_INCREMENT for table `unitconversion_tbl`
--
ALTER TABLE `unitconversion_tbl`
  MODIFY `unitConversionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `unit_tbl`
--
ALTER TABLE `unit_tbl`
  MODIFY `unitId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts_tbl`
--
ALTER TABLE `contacts_tbl`
  ADD CONSTRAINT `FK__contacts___ledge__719CDDE7` FOREIGN KEY (`ledgerId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contradetails_tbl`
--
ALTER TABLE `contradetails_tbl`
  ADD CONSTRAINT `FK__contraDet__contr__72910220` FOREIGN KEY (`contraMasterId`) REFERENCES `contramaster_tbl` (`contraMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__contraDet__ledge__73852659` FOREIGN KEY (`ledgerId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contramaster_tbl`
--
ALTER TABLE `contramaster_tbl`
  ADD CONSTRAINT `FK__contraMas__finan__2E86BBED` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__contraMas__ledge__6FB49575` FOREIGN KEY (`ledgerId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__contraMas__userI__70A8B9AE` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `estimatedetails_tbl`
--
ALTER TABLE `estimatedetails_tbl`
  ADD CONSTRAINT `FK__estimateD__estim__558AAF1E` FOREIGN KEY (`estimateMasterId`) REFERENCES `estimatemaster_tbl` (`estimateMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__estimateD__produ__567ED357` FOREIGN KEY (`productId`) REFERENCES `product_tbl` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__estimateD__unitC__58671BC9` FOREIGN KEY (`unitConversionId`) REFERENCES `unitconversion_tbl` (`unitConversionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__estimateD__unitI__5772F790` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `estimatemaster_tbl`
--
ALTER TABLE `estimatemaster_tbl`
  ADD CONSTRAINT `FK__estimateM__ledge__4FD1D5C8` FOREIGN KEY (`ledgerHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__estimateM__sales__50C5FA01` FOREIGN KEY (`salesHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `financialyear_tbl`
--
ALTER TABLE `financialyear_tbl`
  ADD CONSTRAINT `FK__financial__userI__2BAA4F42` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `journaldetails_tbl`
--
ALTER TABLE `journaldetails_tbl`
  ADD CONSTRAINT `FK__journalDe__journ__74794A92` FOREIGN KEY (`journalMasterId`) REFERENCES `journalmaster_tbl` (`journalMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__journalDe__ledge__756D6ECB` FOREIGN KEY (`ledgerId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `journalmaster_tbl`
--
ALTER TABLE `journalmaster_tbl`
  ADD CONSTRAINT `FK__journalMa__finan__2F7AE026` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__journalMa__userI__6EC0713C` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ledger_tbl`
--
ALTER TABLE `ledger_tbl`
  ADD CONSTRAINT `FK__ledger_tb__accou__5E8A0973` FOREIGN KEY (`accountId`) REFERENCES `account_tbl` (`accountId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `paymentvoucher_tbl`
--
ALTER TABLE `paymentvoucher_tbl`
  ADD CONSTRAINT `FK__paymentVo__finan__2C9E737B` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__paymentVo__ledge__6442E2C9` FOREIGN KEY (`ledgerFirst`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__paymentVo__ledge__65370702` FOREIGN KEY (`ledgerSecond`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__paymentVo__userI__662B2B3B` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pdcpayable_tbl`
--
ALTER TABLE `pdcpayable_tbl`
  ADD CONSTRAINT `FK__pdcPayabl__Clear__671F4F74` FOREIGN KEY (`ClearUserId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcPayabl__finan__306F045F` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcPayabl__ledge__681373AD` FOREIGN KEY (`ledgerCustId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcPayabl__ledge__690797E6` FOREIGN KEY (`ledgerBankId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcPayabl__userI__69FBBC1F` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pdcreceivable_tbl`
--
ALTER TABLE `pdcreceivable_tbl`
  ADD CONSTRAINT `FK__pdcReceiv__Clear__6AEFE058` FOREIGN KEY (`ClearUserId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcReceiv__finan__31632898` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcReceiv__ledge__6BE40491` FOREIGN KEY (`ledgerCustId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcReceiv__ledge__6CD828CA` FOREIGN KEY (`ledgerBankId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__pdcReceiv__userI__6DCC4D03` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD CONSTRAINT `FK__product_t__produ__7167D3BD` FOREIGN KEY (`productGroupId`) REFERENCES `productgroup_tbl` (`productGroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__product_t__unitI__725BF7F6` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchaseldetails_tbl`
--
ALTER TABLE `purchaseldetails_tbl`
  ADD CONSTRAINT `FK__purchasel__produ__7EC1CEDB` FOREIGN KEY (`productId`) REFERENCES `product_tbl` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchasel__purch__7DCDAAA2` FOREIGN KEY (`purchaseMasterId`) REFERENCES `purchasemaster_tbl` (`purchaseMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchasel__unitC__00AA174D` FOREIGN KEY (`unitConversionId`) REFERENCES `unitconversion_tbl` (`unitConversionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchasel__unitI__7FB5F314` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchasemaster_tbl`
--
ALTER TABLE `purchasemaster_tbl`
  ADD CONSTRAINT `FK__purchaseM__finan__32574CD1` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseM__ledge__7814D14C` FOREIGN KEY (`ledgerHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseM__purch__7908F585` FOREIGN KEY (`purchaseHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchasereturndetails_tbl`
--
ALTER TABLE `purchasereturndetails_tbl`
  ADD CONSTRAINT `FK__purchaseR__produ__0D0FEE32` FOREIGN KEY (`productId`) REFERENCES `product_tbl` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__purch__0C1BC9F9` FOREIGN KEY (`purchaseReturnMasterId`) REFERENCES `purchasereturnmaster_tbl` (`purchaseReturnMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__purch__3118447E` FOREIGN KEY (`purchaseDetailsId`) REFERENCES `purchaseldetails_tbl` (`purchaseDetailsId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__unitC__0EF836A4` FOREIGN KEY (`unitConversionId`) REFERENCES `unitconversion_tbl` (`unitConversionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__unitI__0E04126B` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchasereturnmaster_tbl`
--
ALTER TABLE `purchasereturnmaster_tbl`
  ADD CONSTRAINT `FK__purchaseR__finan__334B710A` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__ledge__0662F0A3` FOREIGN KEY (`ledgerHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__purch__056ECC6A` FOREIGN KEY (`purchaseMasterId`) REFERENCES `purchasemaster_tbl` (`purchaseMasterId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__purchaseR__purch__075714DC` FOREIGN KEY (`purchaseHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `receiptvoucher_tbl`
--
ALTER TABLE `receiptvoucher_tbl`
  ADD CONSTRAINT `FK__receiptVo__finan__2D9297B4` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__receiptVo__ledge__607251E5` FOREIGN KEY (`ledgerFirst`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__receiptVo__ledge__6166761E` FOREIGN KEY (`ledgerSecond`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__receiptVo__userI__625A9A57` FOREIGN KEY (`userId`) REFERENCES `user_tbl` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `salesdetails_tbl`
--
ALTER TABLE `salesdetails_tbl`
  ADD CONSTRAINT `FK__salesDeta__produ__1A69E950` FOREIGN KEY (`productId`) REFERENCES `product_tbl` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesDeta__sales__1975C517` FOREIGN KEY (`salesMasterId`) REFERENCES `salesmaster_tbl` (`salesMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesDeta__unitC__1C5231C2` FOREIGN KEY (`unitConversionId`) REFERENCES `unitconversion_tbl` (`unitConversionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesDeta__unitI__1B5E0D89` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `salesmaster_tbl`
--
ALTER TABLE `salesmaster_tbl`
  ADD CONSTRAINT `FK__salesMast__finan__343F9543` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesMast__ledge__13BCEBC1` FOREIGN KEY (`ledgerHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesMast__sales__14B10FFA` FOREIGN KEY (`salesHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `salesreturndetails_tbl`
--
ALTER TABLE `salesreturndetails_tbl`
  ADD CONSTRAINT `FK__salesRetu__produ__28B808A7` FOREIGN KEY (`productId`) REFERENCES `product_tbl` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__sales__27C3E46E` FOREIGN KEY (`salesReturnMasterId`) REFERENCES `salesreturnmaster_tbl` (`salesReturnMasterId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__sales__320C68B7` FOREIGN KEY (`salesDetailsId`) REFERENCES `salesdetails_tbl` (`salesDetailsId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__unitC__2AA05119` FOREIGN KEY (`unitConversionId`) REFERENCES `unitconversion_tbl` (`unitConversionId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__unitI__29AC2CE0` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `salesreturnmaster_tbl`
--
ALTER TABLE `salesreturnmaster_tbl`
  ADD CONSTRAINT `FK__salesRetu__finan__3533B97C` FOREIGN KEY (`financialYearId`) REFERENCES `financialyear_tbl` (`financialYearId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__ledge__220B0B18` FOREIGN KEY (`ledgerHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__sales__2116E6DF` FOREIGN KEY (`salesMasterId`) REFERENCES `salesmaster_tbl` (`salesMasterId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__salesRetu__sales__22FF2F51` FOREIGN KEY (`salesHead`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_tbl`
--
ALTER TABLE `transaction_tbl`
  ADD CONSTRAINT `FK__transacti__ledge__634EBE90` FOREIGN KEY (`ledgerId`) REFERENCES `ledger_tbl` (`ledgerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `unitconversion_tbl`
--
ALTER TABLE `unitconversion_tbl`
  ADD CONSTRAINT `FK__unitConve__conve__7EF6D905` FOREIGN KEY (`conversionUnitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK__unitConve__unitI__7E02B4CC` FOREIGN KEY (`unitId`) REFERENCES `unit_tbl` (`unitId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD CONSTRAINT `FK__user_tbl__roleId__5F7E2DAC` FOREIGN KEY (`roleId`) REFERENCES `role_tbl` (`roleId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
