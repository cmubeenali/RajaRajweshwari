create table salesordermaster_tbl
(
  salesOrderMasterId int auto_increment primary key,
  prefix varchar(20) null,
  voucherNo int null,
  entryDate datetime null,
  ledgerHead int null REFERENCES ledger_tbl(ledgerId),
  salesHead int null REFERENCES ledger_tbl(ledgerId),
  amount decimal(18,2) null,
  discount decimal(18,2) null,
  taxAmount decimal(18,2) null,
  totalAmount decimal(18,2) null,
  paidAmount decimal(18,2) null,
  description text null,
  tinNo varchar(100) null,
  taxForm varchar(100) null,
  salesType varchar(200) null,
  previousBalance decimal(18,2) null,
  poNumber varchar(100) null,
  poDate datetime null,
  financialYearId int null REFERENCES financialyear_tbl(financialYearId)
);

create table salesorderdetails_tbl
(
  salesOrderDetailsId int auto_increment primary key,
  salesOrderMasterId int null REFERENCES salesordermaster_tbl(salesOrderMasterId),
  productId int null REFERENCES product_tbl(productId),
  unitId int null REFERENCES unit_tbl(unitId),
  unitConversionId int null REFERENCES unitconversion_tbl(unitConversionId),
  unitConversionRate decimal(18,2) null,
  taxType text null,
  taxPercent decimal(18,2) null,
  qty decimal(18,3) null,
  rate decimal(18,2) null,
  netAmount decimal(18,2) null,
  taxAmount decimal(18,2) null,
  totalAmount decimal(18,2) null,
  hsnOrSacCode varchar(50) null,
  purchaseRate decimal(18,2) null,
  vaPercent decimal(18,2) null,
  vaAmount decimal(18,2) null,
  discount decimal(18,2) null
);

alter table salesordermaster_tbl add partyRef varchar(100);