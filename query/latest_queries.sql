ALTER TABLE purchaseldetails_tbl MODIFY qty decimal(18,5);

ALTER TABLE salesdetails_tbl MODIFY qty decimal(18,5);

ALTER TABLE salesorderdetails_tbl MODIFY qty decimal(18,5);

update submodule_tbl set isActive=0 where moduleName='Tax Report';

update ledger_tbl set ledgerName='Sales A/c' where ledgerName='Service Sales';

update ledger_tbl set ledgerName='Purchase A/c' where ledgerName='Service Purchase';

ALTER TABLE purchasemaster_tbl CHANGE tinNo gstIn varchar(100);

insert into submodule_tbl(mainModuleId,moduleName,isActive)values (4,'Purchase Report',1);

insert into submodule_tbl(mainModuleId,moduleName,isActive)values (4,'Sales Report',1);

create function `unitConversion`(`mainUnit` int, `conversionId` int,`qty` decimal(18,5)) returns decimal(18,5)
  begin
    if (conversionId>0)then
      begin
        set @conversionRate:=(select conversionRate from unitconversion_tbl t1 where t1.unitConversionId=conversionId);
        return qty/@conversionRate;
      end;
    else
      return qty;
    end if;
  end;


 alter table company_tbl add printMethod varchar(50);

 update company_tbl set printMethod='1';

alter table purchasemaster_tbl add vendorType varchar(50)

create function `ReturnUnitName`(`mainUnitId` int,`conversionId` int)returns varchar(50)
  begin
    if(conversionId>0)then
      begin
        set @unitName := (select u.unitName from unitconversion_tbl t1 inner join unit_tbl u on t1.conversionUnitId = u.unitId where t1.unitConversionId=conversionId);
        return @unitName;
      end;
    else
      return (select t1.unitName from unit_tbl t1 where t1.unitId=mainUnitId);
    end if;
  end;